// 兼容问题  用来获取当前属性
function getStyle(element, attr) {
    return window.getComputedStyle ? window.getComputedStyle(element, null)[attr] : element.currentStyle[attr] || 0;
}
// 调用变速动画函数 第一个是 元素 第二个是 对象 第三个是 回调函数
function animate(element, json, fn) {
    // 在声明定时器之前 先清理定时器
    clearInterval(element.timeId);
    // 声明定时器
    element.timeId = setInterval(function () {
        //默认,假设,全部到达目标
        var flag = true;

        //遍历json对象中的每个属性还有属性对应的目标值
        for (var attr in json) {
            // 判断是不是 opacity 透明度
            if (attr == "opacity") {
                // 获取当前属性的位置 放大100
                var current = getStyle(element, attr) * 100;
                // 获取目标位置 也就是当前属性的键值  放大100
                var target = json[attr] * 100;
                // 进步值 因为是 变速 先快后慢 而下面的 公式 就是让 进步值 越来越小 
                var step = (target - current) / 10;
                // 判断 进步值 是否大于0  如果大于0 向上取整 如果小于0 那就向下取整 目的 就是 目标值 比 现在获取的小 那就需要 减 
                step = step > 0 ? Math.ceil(step) : Math.floor(step);
                // 让 当前值 自增
                current += step;
                // 让该 属性 进行操作 比如 宽度  让宽度 的 宽度 变大 一次比一次小  但是 透明度没有 px 原本放大100 现在 缩小100
                element.style[attr] = current / 100;

            } else if (attr == "zIndex") {
                // 判断是不是 zIndex 层级关系
                // 如果是 让该元素的属性值 赋给 该元素的属性
                element.style[attr] = json[attr];

            } else {
                // 如果都不是 那就是 普通的属性

                //获取元素这个属性的当前的值
                var current = parseInt(getStyle(element, attr));
                // 获取目标位置 也就是当前属性的键值
                var target = json[attr];
                // 进步值 因为是 变速 先快后慢 而下面的 公式 就是让 进步值 越来越小 
                var step = (target - current) / 10;
                // 判断 进步值 是否大于0  如果大于0 向上取整 如果小于0 那就向下取整 目的 就是 目标值 比 现在获取的小 那就需要 减 
                step = step > 0 ? Math.ceil(step) : Math.floor(step);
                // 让 当前值 自增
                current += step;
                // 让该 属性 进行操作 比如 宽度  让宽度 的 宽度 变大 一次比一次小
                element.style[attr] = current + 'px';
            }
            //是否到达目标 没有到达的话  就让它 默认到达目标的 让它 为false
            if (target != current) {
                flag = false;
            }

        }

        // 在for in 循环完毕后 判断所有的目标 都已经完成 如果到达 就 清理定时器 然后 判断 fn是不是 有 如果存在 就调用 ()
        if (flag) {
            clearInterval(element.timeId);
            if (fn) {
                fn();
            }
        }

    }, 20)
}


