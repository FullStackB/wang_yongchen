/**
 * let的基本用法
 * ES6新增了let命令，用来声明变量。
 * 它的用法类似于var，但是所声明的变量，只在let命令所在的代码块内有效
 */

// 报错
/**
 * let不允许在相同作用域内重复声明同一个变量
 * Identifier(标识符) 'a' has already(已经) been declared(声明)
 */
{
    let ser = 898;
    console.log(ser);
    let ser = 777;
    console.log(ser);
}