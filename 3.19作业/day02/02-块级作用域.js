/**
 * 为什么需要块级作用域
 * ES5只有全局作用域和函数作用域，没有块级作用域，这会带来很多不合理的场景
 * 1.内层变量可能会覆盖外层变量
 * 2.用来计数的循环变量泄露为全局变量
 */


// 1.内层变量可能会覆盖外层变量

// var age = 10;
// function fn() {
//   console.log(age);
//   if (false) {
//     var age = "hello";
//   }
// }
// fn();

// 解释: 这个代码本来的意图是为了打印age = 10; 如果他是true 打印里面的 如果是false 打印10
// 但是无论代码怎么执行 都是undefined 因为var声明的变量会发生变量提升,如果使用let 就没有这样的问题
// var age = 10;
// function fn() {
//   console.log(age);
//   if (false) {
//     let age = 'hello';
//   }
// }
// fn();


// var age = 10;
// function fn() {
//   var age;
//   console.log(age);
//   if (false) {
//     age = "hello";
//   }
// }

// fn();


// 2.用来计数的循环变量泄露为全局变量
// for (var i = 0; i < 10; i++) {

// }
// var i = 100;
// console.log(i);

// for (let i = 0; i < 10; i++) {

// }

// console.log(i);// i is not defined