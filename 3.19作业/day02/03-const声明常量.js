/**
 * const声明常量(不变的量)
 * 1.const声明的常量不能改变
 * 2.const声明的常量必须立即赋值
 * 3.const不能重复声明常量
 * 4.const的常量值真的不能变吗? 对于基本数据类型 绝对是不能改变的 但是对于对象 如果只是改变对象的属性 对象在内存中的地址没有改变 那const是不会报错的
 *
 */
// 1.const声明的常量不能改变
// 2.const声明的常量必须立即赋值
// 3.const不能重复声明常量


// const声明的常量不能改变

// const ser = 88;
// console.log(ser);
// ser = 999;
// console.log(ser); // 报错

// const声明的常量必须立即赋值

// const num;
// num = 99;
// console.log(num);  // 报错


// const不能重复声明常量

// const s = 888;
// const s = 222;  // 报错