const person = {
    name: '没时间',
    age: 18,
    sex: '160mol'
}
//  一 默认写法
// var {
//     name: a,
//     age: b,
//     sex: c
// } = person;
// console.log(a, b, c);

// 二  更换
// var {
//     name: name,
//     age: age,
//     sex: sex
// } = person;
// console.log(name, age, sex);


//  三  最简单之法
// var {
//     name,
//     age,
//     sex
// } = person;
// console.log(name, age, sex);

// 第二遍

// const person = {
//     name: '你好啊',
//     age: 199,
//     sex: '160mol'
// }
//  一 默认写法
// var {
//     name: a,
//     age: b,
//     sex: c
// } = person;
// console.log(a, b, c);

// 二  更换
// var {
//     name: name,
//     age: age,
//     sex: sex
// } = person;
// console.log(name, age, sex);


//  三  最简单之法
// var {
//     name,
//     age,
//     sex
// } = person;
// console.log(name, age, sex);

// 第三

// const person = {
//     name: '哈达给',
//     age: 8,
//     sex: '10mol'
// }
//  一 默认写法
// var {
//     name: a,
//     age: b,
//     sex: c
// } = person;
// console.log(a, b, c);

// 二  更换
// var {
//     name: name,
//     age: age,
//     sex: sex
// } = person;
// console.log(name, age, sex);


//  三  最简单之法
// var {
//     name,
//     age,
//     sex
// } = person;
// console.log(name, age, sex);