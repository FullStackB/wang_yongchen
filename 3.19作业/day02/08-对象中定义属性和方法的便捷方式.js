//ES6 允许在对象之中，直接写变量。这时，属性名为变量名, 属性值为变量的值。


// 第一
// // 默认写法
// let name = "没时间";
// let age = 19;

// // var obj = {
// //     name: name,
// //     age: age,
// //     sayHi: function () {
// //         console.log(没时间喽);
// //     }
// // }

// //  vue中简写比较多 的有属性和方法

// // let obj = {
// //     name,
// //     age,
// //     sayHi() {
// //         console.log('没时间喽');
// //     }
// // }

// // console.log(obj);
// // obj.sayHi();

// 第二

// let name = '没时间';
// let age = 19;
// var obj = {
//     name: name,
//     age: age,
//     sayHi: function () {
//         console.log('嗯嗯');
//     }
// }
// console.log(obj);
// obj.sayHi();

// let obj = {
//     name,
//     age,
//     sayHi() {
//         console.log('嗯嗯');
//     }
// }

// console.log(obj);
// obj.sayHi();




// 第三

// let name = '没时间';
// let age = 19;
// var obj = {
//     name: name,
//     age: age,
//     sayHi: function () {
//         console.log('嗯嗯');
//     }
// }
// console.log(obj);
// obj.sayHi();

// let obj = {
//     name,
//     age,
//     sayHi() {
//         console.log('嗯嗯');
//     }
// }

// console.log(obj);
// obj.sayHi();