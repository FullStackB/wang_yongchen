// //  1、引包
// const fs = require('fs');
// // 用包
// fs.readFile('./file/1.txt', (err, data) => {
//     if (err) {
//         return console.log('读取文件失败！');
//     }

//     console.log(data.toString());
// })

// const fs = require('fs');
// fs.readFile('./file/1.txt', (err, data) => {
//     if (err) {
//         return console.log('读取文件失败');
//     }
//     console.log(data.toString());
// })

// 第二
// const fs = require('fs');
// fs.readFile('./file/1.txt', (err, data) => {
//     if (err) {
//         return console.log("读取文件失败");
//     }
//     console.log(data);

// })

// 第三   读取失败
// const fs = require('fs');
// fs.readFile('./file/1.tx', (err, data) => {
//     if (err) {
//         return console.log('读取失败');
//     }
//     console.log(data);
// })