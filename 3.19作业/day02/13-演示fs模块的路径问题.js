// 在node中 最好不要用相对路径 因为相对路径都是以当前路径为开始的 
// 要用绝对路径

// let fs = require('fs');

// fs.readFile(__dirname + '/file/1.txt', 'utf8', (err, data) => {
//     if (err) {
//         return console.log('读取文件失败');
//     }
//     console.log(data);
// })

// 第二 错误   __dirname + './file/1.txt'  ./file/1.txt  前面不能加点
// let fs = require('fs');
// fs.readFile(__dirname + './file/1.txt', 'utf8', (err, data) => {
//     if (err) {
//         return console.log("读取文件失败");
//     }
//     console.log(data);
// })

// 第三
// let fs = require('fs');
// fs.readFile(__dirname + '/file/1.txt', 'utf8', (err, data) => {
//     if (err) {
//         return console.log('读取文件失败');
//     }
//     console.log(data);
// })