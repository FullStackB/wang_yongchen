// fs.stat(文件地址,回调函数)

// let fs = require('fs');
// fs.stat(__dirname + '/file/msj.txt', (err, stats) => {
//     if (err) {
//         return console.log(err);
//     }
//     // 文件的大小  单位是字节 byte   stats.size
//     console.log(stats.size);
//     // 文件的创建时间
//     console.log(stats.birthtime.toString());
//     // 判断该文件 是不是 文件
//     console.log(stats.isFile());
//     // 判断该文件是不是文件夹
//     console.log(stats.isDirectory());
// })

// 第二
// let fs = require('fs');
// fs.stat(__dirname + '/file/1.txt', (err, stats) => {
//     if (err) {
//         return console.log('读取失败!');
//     }
//     // 大小
//     console.log(stats.size);
//     // 创建时间
//     console.log(stats.birthtime.toString());
//     // 文件
//     console.log(stats.isFile());
//     // 文件夹
//     console.log(stats.isDirectory());
// })

// 第三
let fs = require('fs');
fs.stat(__dirname + '/file/2.txt', (err, stats) => {
    if (err) {
        return console.log('读取失败');
    }
    console.log(stats.size);
    console.log(stats.birthtime.toString());
    console.log(stats.isFile());
    console.log(stats.isDirectory());
})