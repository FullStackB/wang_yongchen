// fs.readdir(要读取的文件的目录, 编码 ,回调(错误对象, 读出来的文件的名数组))
// const fs = require('fs');
// fs.readdir(__dirname, 'utf8', (err, files) => {
//     if (err) {
//         console.log(err);
//     }
//     console.log(files);
// })

// 第二

let fs = require('fs');
fs.readdir(__dirname, 'utf8', (err, data) => {
    if (err) {
        return console.log('读取失败!');
    }
    console.log(data);
})


// 第三

let fs = require('fs');
fs.readdir(__dirname, 'utf8', (err, data) => {
    if (err) {
        return console.log('读取失败!');
    }
    console.log(data);
})