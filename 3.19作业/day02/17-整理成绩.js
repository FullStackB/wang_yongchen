// 分析：
// 1. 读取内容  fs.readFile
// 2. 使用字符串的 split 方法，分割成绩，然后使用字符串的 replace 方法，把 = 替换为 :
// 3. 把成绩，拼接成一个新的字符串，然后写入到 成绩 - ok.txt 中
const fs = require('fs')

fs.readFile(__dirname + '/file/成绩.txt', 'utf8', (err, dataStr) => {
    if (err) return console.log('读取成绩失败！')
    // filter 过滤的意思，对数组进行过滤
    const scoreArr = dataStr.split(' ').filter(item => item.length > 0)
    // 调用数组的 map 方法，对原数组中的每一项，进行一个操作，并把操作的结果，当作新数组返回
    const newScoreArr = scoreArr.map(item => item.replace('=', '：'))
    fs.writeFile(__dirname + '/file/成绩-okok.txt', newScoreArr.join('\n'), err => {
        if (err) return console.log('写入成绩失败！')
        console.log('搞定！')
    })
})  