// 2、使用node.js对文件‘02-write.txt’写入内容“我是传智专修学院的大学生，我要好好学习”，并且读取刚才写入的内容显示到控制台，请在02-writeFile.js中完成

const fs = require('fs');
fs.writeFile('02-write.txt', '我是传智专修学院的大学生，我要好好学习', 'utf8', (err) => {
    if (err) {
        return console.log('写入失败');
    }
})