// 3、使用node.js完成如下功能
// 3.1、首先对文件‘03-write.txt’写入内容“我是传智专修学院的大学生，我要好好学习”；
// 3.2、其次对文件‘03-write.txt’追加内容“我是最棒的”；
// 3.3、最后读取“03-write.txt”整个文件内容显示到控制台；
// 3.4、请在03-appendFile.js中完成

let fs = require('fs');
fs.writeFile('03-write.txt', '我是传智专修学院的大学生，我要好好学习', 'utf8', (err) => {
    if (err) {
        return console.log('写入失败');
    }
})

fs.appendFile('03-write.txt', '我是最棒的', 'utf8', (err) => {
    if (err) {
        return console.log('追加失败');
    }
})

fs.readFile('03-write.txt', 'utf8', (err, data) => {
    if (err) {
        return console.log('读取失败');
    }
    console.log(data);
})