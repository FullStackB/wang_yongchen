// 4、使用node.js读取文件信息
// 4.1、首先对文件‘04-stat.txt’写入内容“全栈谁最牛”；
// 4.2、其次对文件‘04-stat.txt’追加内容“我最牛”；
// 4.3、获取文件‘04-stat.txt’大小 单位是字节；
// 4.4、获取文件‘04-stat.txt’的创建时间
// 4.5、判断‘04-stat.txt’是否是一个文件
// 4.6、判断‘04-stat.txt’是不是目录
// 4.7、请在04-stat.js中完成
let fs = require('fs');
fs.writeFile('04-stat.txt', '全栈谁最牛', 'utf8', (err) => {
    if (err) {
        return console.log('写入失败');
    }
})
fs.appendFile('04-stat.txt', '我最牛', 'utf8', (err) => {
    if (err) {
        return console.log(err);
    }
})
fs.stat('04-stat.txt', (err, stats) => {
    if (err) {
        return console.log('失败');
    }
    console.log(stats.size);
    console.log(stats.birthtime.toString());
    console.log(stats.isFile());
    console.log(stats.isDirectory());

})