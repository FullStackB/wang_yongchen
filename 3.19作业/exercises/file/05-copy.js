// 5、使用node.js拷贝文件
// 5.1、首先对文件‘05-copy.txt’写入内容“我准备拷贝文件”；
// 5.2、其次对文件‘05-copy.txt’追加内容“我继续追加信息到文件中”；
// 5.3、获取文件‘05-copy.txt’大小 单位是字节；
// 5.4、获取文件‘05-copy.txt’的创建时间
// 5.5、判断‘05-copy.txt’是否是一个文件
// 5.6、判断‘05-copy.txt’是不是目录
// 5.7、复制文件“05-copy.txt”为一个新的文件“05-copyFile.txt”
// 5.7、请在05-copy.js中完成

// 引包
const fs = require('fs');
// 写入
fs.writeFile('05-copy.txt', '我准备拷贝文件', (err) => {
    if (err) {
        return console.log('失败');
    }
})
// 拷贝
fs.appendFile('05-copy.txt', '我继续追加信息到文件中', 'utf8', (err) => {
    if (err) {
        return console.log('失败');
    }
})


// 由于 异步 所以先 来个延迟定时器
setTimeout(() => {
    // 获取文件的属性
    fs.stat('05-copy.txt', (err, stats) => {
        if (err) {
            return console.log('失败');
        }
        // 大小
        console.log(stats.size);
        // 创建时间
        console.log(stats.birthtime.toString());
        // 是否是一个文件
        console.log(stats.isFile());
        // 是不是目录
        console.log(stats.isDirectory());
    })

    // 复制
    fs.copyFile('05-copy.txt', '05-copyFile.txt', 'utf8', (err) => {
        if (err) {
            return console.log('失败');
        }
    })
}, 1000)