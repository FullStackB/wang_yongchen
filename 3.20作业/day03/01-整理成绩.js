// 需求: 用Node读取 成绩.txt中的数据 然后把数据写入到 result.txt中
// 格式是这样的：
//  小红：99
//  小白：100
//  小黄：70
//  小黑：66
//  小绿：88


// 思路: 使用fs模块，通过readFile方法把成绩.txt中的数据读取出来 数据是个字符串
// 使用split方法把字符串分割成字符串数组 把‘=’号替换成‘:’  
// 把数组变成字符串 就使用数组的join方法在每个元素后面添加一个换行符 让其换行

const fs = require('fs');
// 读文件
fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (err, data) => {
    if (err) {
        return console.log(err);
    }
    // console.log(data);

    var sroceArr = data.split(' ');
    console.log(sroceArr);

    var serArr = [];
    sroceArr.forEach((item) => {
        if (item.length > 0) {
            var nameArr = item.replace('=', ':');
            serArr.push(nameArr);
        }
    })
    console.log(serArr);
    var result = serArr.join('\n');
    fs.writeFile(__dirname + '/files/result.txt', result, 'utf8', (err) => {
        if (err) {
            return console.log(err);
        }
    })
})


// var arr = [' ', '22'];
// arr.forEach((ietm, i) => {
//   console.log(ietm.length, i);
// })