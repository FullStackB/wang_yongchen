// 前置知识 filter

let arr = [13, 0, 123, 56];
var result = arr.filter(function (item, index) {
  // console.log(item);

  return item >= 13;
})
var result = arr.filter((item) => { return item > 13 })
console.log(result);


// 前置知识 map
// let array1 = [1, 4, 9, 16];
// var newArr1 = array1.map(function (item) {
//   return item + '*';
// })
// console.log(newArr1);

// 需求: 用Node读取 成绩.txt中的数据 然后把数据写入到 result.txt中
// 格式是这样的：
//  小红：99
//  小白：100
//  小黄：70
//  小黑：66
//  小绿：88


// 思路: 使用fs模块，通过readFile方法把成绩.txt中的数据读取出来 数据是个字符串
// 使用split方法把字符串分割成字符串数组 把‘=’号替换成‘:’  
// 把数组变成字符串 就使用数组的join方法在每个元素后面添加一个换行符 让其换行
// const fs = require('fs');
// fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (err, data) => {
//     if (err) {
//         return console.log("读取失败！");
//     }
//     var sroceArr = data.split(' ').filter((item) => {
//         return item.length > 0
//     })
//     var nameArr = sroceArr.map((item) => {
//         return item.replace('=', ':');
//     })
//     fs.writeFile(__dirname + '/files/msj.txt', nameArr.join('\n'), 'utf8', (err) => {
//         if (err) {
//             return console.log(err);
//         }
//     })
// })

// 第二
// const fs = require('fs');
// fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (err, data) => {
//     if (err) {
//         return console.log(err);
//     }
//     var sroceArr = data.split(' ').filter((item) => {
//         return item.length > 0;
//     })
//     var nameArr = sroceArr.map((item) => {
//         return item.replace('=', ':');
//     })
//     fs.writeFile(__dirname + '/files/result2.txt', nameArr.join('\n'), (err) => {
//         if (err) {
//             return console.log(err);
//         }
//     })
// })

// 第三
// const fs = require('fs');
// fs.readFile(__dirname + '/files/成绩.txt', 'utf8', (err, data) => {
//     if (err) {
//         return console.log(err);
//     }
//     var sroceArr = data.split(' ').filter((ietm) => {
//         return ietm.length > 0;
//     })
//     var nameArr = sroceArr.map((ietm) => {
//         return ietm.replace('=', ':');
//     })
//     fs.writeFile(__dirname + '/files/result3.txt', nameArr.join('\n'), 'utf8', (err) => {
//         if (err) {
//             return console.log(err);
//         }
//     })
// })