// fs模块 是用来操作文件的
// node自带path

const path = require('path');
// 1.path.sep 返回当前系统的片段符
// console.log(path.sep); // \

// 2.path.dirname 返回某个文件所在的目录

// console.log(path.dirname);
// let str = "G:/vs code/Node/day03/03-path路径操作模块.js"
// console.log(__dirname);
// console.log(path.dirname(str));


// 3.path.extname  extname扩展名后缀
// console.log(path.extname(str));/.js


// 4.path.join(重要的) 拼接路径 相当于  conent() 好像写错了
// './files/成绩.txt' 加点的话会报错 很难找的 所以用path.join()  目的为了 拼接

// console.log(__dirname + './files/成绩.txt'); //G:\vs code\Node\day03/files/成绩.txt

console.log(path.join(__dirname + './files/msj.txt')); //G:\vs code\Node\day03.\files\msj.txt

console.log(path.join('f:', "/a", "/b", "./c", "../d/aa.txt"));