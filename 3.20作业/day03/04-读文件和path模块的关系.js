// 记住：今后只要涉及到路径片段的拼接，一定要使用 path.join() 方法 

// const fs = require('fs');
// const path = require('path');

// fs.readFile(path.join(__dirname, './files/msj.txt'), 'utf-8', (err, data) => {
//     if (err) {
//         return console.log(err.message);
//     }
//     console.log(data);
// })

// 记住：今后只要涉及到路径片段的拼接，一定要使用 path.join() 方法