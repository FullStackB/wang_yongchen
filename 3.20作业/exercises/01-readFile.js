// 1、使用node.js对文件‘01-read.txt’中的内容“我是传智专修学院，全栈应用方向专业的一名大大一学生，我非常自豪”进行读取，并且在控制台输出，请在01-readFile.js中完成

// 加入模块
const fs = require('fs');
const path = require('path');
// 进行读取
fs.readFile(path.join(__dirname, './files/01-read.txt'), 'utf-8', (err, data) => {
    // 判断是否有没有这个文件
    if (err) {
        return console.log(err.message);
    }
    // 输出
    console.log(data);
})