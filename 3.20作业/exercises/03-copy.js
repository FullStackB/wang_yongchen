// 3、使用node.js拷贝文件   使用sync方法 同步进程
// 3.1、首先对文件‘03-copy.txt’写入内容“我准备拷贝文件”；
// 3.2、其次对文件‘03-copy.txt’追加内容“我继续追加信息到文件中”；
// 3.3、获取文件‘03-copy.txt’大小 单位是字节；
// 3.4、获取文件‘03-copy.txt’的创建时间
// 3.5、判断‘03-copy.txt’是否是一个文件
// 3.6、判断‘03-copy.txt’是不是目录
// 3.7、复制文件“03-copy.txt”为一个新的文件“03-copyFile.txt”
// 3.7、请在03-copy.js中完成

const fs = require('fs');
const path = require('path');
// 写入
fs.writeFileSync(path.join(__dirname, './files/03-copy.txt'), '我准备拷贝文件');
// 追加  为了解决异步 都放在追加后
fs.appendFileSync(path.join(__dirname, './files/03-copy.txt'), '我继续追加信息到文件中');
// 查看元素
var stat = fs.statSync(path.join(__dirname, './files/03-copy.txt'))
// 大小
console.log(stat.size);
// 创建时间
console.log(stat.birthtime.toString());
// 是否是一个文件
console.log(stat.isFile());
// 是不是目录
console.log(stat.isDirectory());
// 复制
fs.copyFileSync(path.join(__dirname, './files/03-copy.txt'), path.join(__dirname, './files/03-copyFile.txt'))