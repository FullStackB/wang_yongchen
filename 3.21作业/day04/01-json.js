// 1.为什么需要json
// 答: json存在的意义在于后端给前端返回的数据大部分(90%)都是json

// 2.json是什么？
//  是一种轻量级的数据交换格式。 易于人阅读和编写。 同时也易于机器解析和生成
// json是符合javascript对象或数组格式的字符串

// var jsonArr = '[1,2,3,4,5]';
// var jsonObj = '{ name: "zhangsan", age: "18" }';

// 3.怎么证明这个数据是json？
// 网络中的数据都是字符串
// 但是前端需要把这些字符串转换成 数组或对象
// 因为 我要获取数组中的数据 那就要把这些复杂的字符串变成对象或数组


// <1> 把字符串转换成数组 或者 对象
// var jsonArr = '[1,2,3,4,5]';
// var jsonObj = '{ "name": "zhangsan", "age": "18" }';
// // JSON.parse 格式 必须这样写 把字符串转换成数组 或 对象       // JSON.parse 把字符串变成对象或数组
// // 数组
// var arr = JSON.parse(jsonArr);
// console.log(arr);
// // 对象
// var obj = JSON.parse(jsonObj);
// console.log(obj);


// <2> 把数组 或者 对象 转换成字符串
var arr = [1, 2, 3, 4, 5];
var obj = {
    name: 'zhangsan',
    age: '18'
};
// // JSON.stringify(数组或对象)   格式 必须这样写  把数组 或者 对象 转换成字符串
// //数组
// var jsonArr = JSON.stringify(arr);
// console.log(jsonArr);
// // 对象
// var jsonObj = JSON.stringify(obj);
// console.log(jsonObj);




// 总结:
// 1.为什么要用json
// 2.json是什么
// 3.json转换为对象或数组 JSON.parse
// 4.对象或数组转换为json字符串 JSON.stringify
// 5.JSON字符串的格式: 如果是对象 那么对象的属性和值都要用双引号
// 6.JSON是字符串 但是也可以写成json文件