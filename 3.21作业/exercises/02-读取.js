// 2.0-读取package-lock.json文件,并将内容打印在控制台上，使用readFileSync读取，要求：
// 2.1-将读取到的数据转换为对象打印在控制台
// 2.2-将对象再次转换为字符串再次打印在控制台
// 2.3-通过[ writeFileSync ]写入一个新的文件 newJson.txt

let fs = require('fs');
let ser = fs.readFileSync('./package-lock.json', 'utf8')
console.log(ser);
// 华丽的水平线
console.log('======================================================================');

// 读取到的数据转换为对象打印在控制台
var obj = JSON.parse(ser);
console.log(obj);
// 华丽的水平线
console.log('======================================================================');
// 对象再次转换为字符串再次打印在控制台
var str = JSON.stringify(obj);
console.log(str);

fs.writeFileSync('./newJson.txt', str, 'utf8');