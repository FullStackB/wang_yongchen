const add = require('./lib/add')
const minus = require('./lib/minus')
const ride = require('./lib/ride')
const except = require('./lib/except')

module.exports = {
    add,
    minus,
    ride,
    except
}