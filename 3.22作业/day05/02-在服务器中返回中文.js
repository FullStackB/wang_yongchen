// const http = require('http');
// const server = http.createServer();

// server.on('request', (req, res) => {
//     // 在服务器中 要给前端返回数据 有时候会成功 有时候会失败
//     // 为了让浏览器更清晰的知道 就规定了状态码
//     // 成功时的返回 200
//     // 找不到时404
//     // 服务器错误: 500 502 

//     // Content-Type 内容的类型  告诉浏览器你如何解析我的内容
//     // text/plain 请把我的内容解析为文本格式
//     // charset=UTF-8请使用UTF-8编码解析
//     res.writeHead(200, {
//         "Content-Type": "text/plain;charset=UTF-8"
//     })
//     res.end('没时间 你真好');
// })

// server.listen(3000, () => {
//     console.log("请访问：http://192.168.26.171:3000");
// })



// 第二

// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res) => {
//     res.writeHead(200, {
//         "Content-Type": "text/plain;charset=UTF-8"
//     })

//     res.end('天气很哈皮');
// })
// server.listen(3000, () => {
//     console.log("请访问：http://192.168.26.171:3000");
// })

// 第三
// const http = require('http');
// const server = http.createServer();
// server.on('request', (req, res) => {
//     res.writeHead(200, {
//         "Content-Type": "text/plain;charset=UTF-8"
//     })
//     res.end("如果心情很好，那么学习就很爽")
// })
// server.listen(3000, () => {
//     console.log("请访问：http://192.168.26.171:3000");
// })