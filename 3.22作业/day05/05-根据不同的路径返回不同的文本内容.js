const http = require('http')
const fs = require('fs')
const path = require('path')
const server = http.createServer()
server.on('request', (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html;charset=UTF-8"
    })
    if (req.url === '/' || req.url === '/index.html') {
        fs.readFile(path.join(__dirname, "./index.html"), 'utf-8', (err, data) => {
            if (err) {
                return console.log(err.message);
            }
            res.end(data);
        });
    } else if (req.url === '/about.html') {
        fs.readFile(path.join(__dirname, "./about.html"), 'utf-8', (err, data) => {
            if (err) {
                return console.log(err.message);
            }
            res.end(data);
        });
    } else {
        fs.readFile(path.join(__dirname, "./404.html"), 'utf-8', (err, data) => {
            if (err) {
                return console.log(err.message);
            }
            res.end(data);
        });
    }
})
server.listen(3000, () => {
    console.log('请访问：http://192.168.26.171:3000');

})

// 第二
// const http = require('http')
// const fs = require('fs')
// const path = require('path')
// const server = http.createServer()
// server.on('request', (req, res) => {
//     res.writeHead(200, {
//         "Content-Type": "text/html;charset=UTF-8"
//     })
//     if (req.url === '/' || req.url === '/index.html') {
//         fs.readFile(path.join(__dirname, "./index.html"), 'utf-8', (err, data) => {
//             if (err) {
//                 return console.log(err.message);
//             }
//             res.end(data);
//         });
//     } else if (req.url === '/about.html') {
//         fs.readFile(path.join(__dirname, "./about.html"), 'utf-8', (err, data) => {
//             if (err) {
//                 return console.log(err.message);
//             }
//             res.end(data);
//         });
//     } else {
//         fs.readFile(path.join(__dirname, "./404.html"), 'utf-8', (err, data) => {
//             if (err) {
//                 return console.log(err.message);
//             }
//             res.end(data);
//         });
//     }
// })
// server.listen(3000, () => {
//     console.log('请访问：http://192.168.26.171:3000');

// })

// 第三


// const http = require('http')
// const fs = require('fs')
// const path = require('path')
// const server = http.createServer()
// server.on('request', (req, res) => {
//     res.writeHead(200, {
//         "Content-Type": "text/html;charset=UTF-8"
//     })
//     if (req.url === '/' || req.url === '/index.html') {
//         fs.readFile(path.join(__dirname, "./index.html"), 'utf-8', (err, data) => {
//             if (err) {
//                 return console.log(err.message);
//             }
//             res.end(data);
//         });
//     } else if (req.url === '/about.html') {
//         fs.readFile(path.join(__dirname, "./about.html"), 'utf-8', (err, data) => {
//             if (err) {
//                 return console.log(err.message);
//             }
//             res.end(data);
//         });
//     } else {
//         fs.readFile(path.join(__dirname, "./404.html"), 'utf-8', (err, data) => {
//             if (err) {
//                 return console.log(err.message);
//             }
//             res.end(data);
//         });
//     }
// })
// server.listen(3000, () => {
//     console.log('请访问：http://192.168.26.171:3000');

// })