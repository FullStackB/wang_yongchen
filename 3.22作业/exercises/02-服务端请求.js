// 2、使用node.js开启服务器端口,要求
// 2.1、端口号：8899
// 2.2、并且在页面输出：Hello 传智学院

const http = require('http')
const fs = require('fs')
const path = require('path')

const server = http.createServer()
server.on('request', (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html;charset=UTF-8"
    })
    fs.readFile(path.join(__dirname, "./02-hello.html"), 'utf-8', (err, data) => {
        if (err) {
            return console.log(err.message);
        }
        res.end(data);
    });
});
server.listen(8899, () => {
    console.log("请访问：http://192.168.26.171:8899");

})