// 3、使用node.js完成根据不同url显示不同的内容，要求
// 3.1、开启服务，并且使用8899端口；
// 3.2、访问”http://127.0.0.1:8899/home.html”显示“首页”
// 3.3、访问”http://127.0.0.1:8899/study.html”显示“学习”；
// 3.4、访问”http://127.0.0.1:8899/us.html”显示“我们”；
// 3.5、访问其他url显示“页面找不到”；

const http = require('http');
const server = http.createServer();
server.on('request', (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html;charset=UTF-8"
    })
    if (req.url === '/' || req.url === '/home.html') {
        res.end(`<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            <style>
                h1 {
                    font-size: 270px;
                    color: blue;
                    text-align: center;
                    margin-top: 0;
                }
            </style>
        </head>
        
        <body>
            <h1>首页</h1>
        </body>
        
        </html>`)
    } else if (req.url === '/study.html') {
        res.end(`<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            <style>
                h1 {
                    font-size: 270px;
                    color: skyblue;
                    text-align: center;
                    margin-top: 0;
                }
            </style>
        </head>
        
        <body>
            <h1>学习</h1>
        </body>
        
        </html>`)
    } else if (req.url === '/us.html') {
        res.end(`<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            <style>
                h1 {
                    font-size: 270px;
                    color: yellow;
                    text-align: center;
                    margin-top: 0;
                }
            </style>
        </head>
        
        <body>
            <h1>我们</h1>
        </body>
        
        </html>`)
    } else {
        res.end(`<!DOCTYPE html>
        <html lang="en">
        
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta http-equiv="X-UA-Compatible" content="ie=edge">
            <title>Document</title>
            <style>
                h1 {
                    font-size: 270px;
                    color: green;
                    text-align: center;
                    margin-top: 0;
                }
            </style>
        </head>
        
        <body>
            <h1>页面找不到</h1>
        </body>
        
        </html>`)
    }
})
server.listen(8899, () => {
    console.log("请访问：http://127.0.0.1:8899");
})