// 7、计算两个数的最大公约数和最小公倍数


var num1 = 10;
var num2 = 6;
//判断两个数的大小s
if (num1 > num2) {
    max = num1;
    min = num2;
} else {
    max = num2;
    min = num1;
}
//进行计算最大公约数 
//基本思想就是辗转相除法（大数除以小数，小数除以上一步的余数，知道余数为0） 
while (max % min != 0) {
    remainder = max % min;
    max = min;
    min = remainder;
}
//变量：gcd最大公约数;lcm最大公倍数； 
//求出了最大公约数，那么最大公约数基本思想就是：两数的成绩除以最大公约数。 
var gcd = min;
var lcm = num1 * num2 / gcd;
//输出页面 
console.log("您输入的两数为：" + num1 + " " + num2)
console.log("两数最大公约数为：" + gcd)
console.log("两数最小公倍数为：" + lcm)