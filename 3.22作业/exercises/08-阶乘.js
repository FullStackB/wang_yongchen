// 8、求1!+2!+3!+...+10!的和

// 阶乘 到多少的值
var num = 10;
// 求和
var sum = 0;
// 遍历
for (let i = 1; i <= num; i++) {
    // 相乘 每次重置
    var ser = 1;
    // 每一个的 阶乘
    for (let j = 1; j <= i; j++) {
        ser *= j;
    }
    // 累加
    sum += ser;
}
// 输出
console.log(sum);