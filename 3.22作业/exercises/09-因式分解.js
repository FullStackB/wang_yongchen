// 9、对一个整数分解质因数。例如： 90 = 2 * 3 * 3 * 5

// 初始化
var num = 90;
// 拼接 连接
var sum = num + '=';
// 遍历
for (let i = 2; i <= num; i++) {
    // 判断是否 方能整除
    if (num % i == 0) {
        // 如果相等的话  就累加 就不用加个乘号了
        if (i == num) {
            sum += i;
        } else {
            sum += i + '*';
        }
        // 抹除 整数 一半
        num = num / i;
        // 然后累减
        i--;
    }
}
// 输出
console.log(sum);