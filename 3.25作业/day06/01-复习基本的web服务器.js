// 引入包
// const http = require('http');
// 创建服务器
// const server = http.createServer();
// 监听客户端的request请求事件 并处理响应
// server.on('request', (req, res) => {
//     res.end('hello')
// })
// 指定端口 启动服务
// server.listen(3333, () => {
//     console.log("开始: http://127.0.0.1:3333");

// })

// 第二遍
// const http = require('http');
// 创建服务器
// const server = http.createServer();

// 监听客户端的request请求事件 并处理响应
// server.on('request', (req, res) => {
//     res.writeHead(200, {
//         "Content-Type": "text/html;charset=utf-8"
//     })
//     res.end('没时间')
// })
// 指定端口 启动服务
// server.listen(3333, () => {
//     console.log("开始: http://127.0.0.1:3333");

// })
// 第三遍
const http = require('http');
const server = http.createServer();
// 监听客户端的request请求事件 并处理响应
server.on('request', (req, res) => {
    res.writeHead(200, {
        "Content-Type": "text/html;charset=utf-8"
    })
    res.end('世间如此美好之事，你偏偏在意那一件难过之事，只能说你真逗')
})

// 指定端口 启动服务
server.listen(3333, () => {
    console.log("开始: http://127.0.0.1:3333");

})