//引入包
const http = require('http');
// 创建服务器
const server = http.createServer();
// 监听客户端的request请求事件 并处理响应
server.on('request', (req, res) => {
    if (req.url === '/') {
        res.end("首页");
    }
})
// 指定端口 启动服务
server.listen(80, () => {
    console.log('服务器运行中...');
})