// // 引入模块
// const express = require('express');
// const qs = require('querystring');

// // 创建服务器
// const app = express();
// // 设置静态资源目录
// app.use(express.static('public'));
// // 监听路径 返回响应
// app.get('/', (req, res) => {
//     res.send("首页 ");
// })

// // 根据前端的请求地址 接受前端的数据
// // 什么方式提交 就用什么方式接受

// // 如果你想接受前端用户输入的用户名和密码 
// // 需要使用一个原生Node的模块 queryString

// app.get('/login', (req, res) => {
//     res.send(req.query.username + ', 您好,您猜一猜我知道你的密码不？ 哈哈哈,我知道您的密码是: ' + req.query.password);
// })
// app.listen(3434, () => {
//     console.log("开始：http://192.168.26.171:3434");
// })




// 第二遍

// const express = require('express');
// const qs = require('querystring');

// const app = express();
// app.use(express.static('public'));
// app.get('/', (req, res) => {
//     res.send('首页');
// });
// app.get('/login', (req, res) => {
//     res.send(req.query.username + '========' + req.query.password);
// });
// app.listen(3434, () => {
//     console.log("开始：http://192.168.26.171:3434");
// })

// 第三遍
const express = require('express');
const qs = require('querystring');
const app = express();
app.use(express.static('public'));
app.get('/', (req, res) => {
    res.send('首页');
});
app.get('/login', (req, res) => {
    res.send(req.query.username + '===============' + req.query.password);
});
app.listen(3434, () => {
    console.log("开始：http://192.168.26.171:3434");
})