// 引入body-parser
// 设置包app.use(bodyParser.urlencoded({ extended: false }))
// 用包: req.body

// const express = require('express');
// const bodyParser = require('body-parser');
// const app = express();

// app.use(bodyParser.urlencoded({
//     extended: false
// }));
// // 设置静态资源目录
// app.use(express.static("public"));

// app.get('/', (req, res) => {
//     res.send("首页")
// });
// // 如果使用POST提交表单 如果想要获取用户传来的数据
// // 需要一个包 body-parser
// app.post('/login', (req, res) => {
//     res.send(req.body.username + ', 您好,您猜一猜我知道你的密码不？ 哈哈哈,我知道您的密码是: ' + req.body.password);
// });
// app.listen(3434, () => {
//     console.log("开始：http://192.168.26.171:3434");
// })

// 第二遍
// const express = require('express');
// const qs = require('querystring');
// const bodyParser = require('body-parser');
// const app = express();
// app.use(express.static('public'));
// app.use(bodyParser.urlencoded({
//     extended: false
// }))
// app.get('/', (req, res) => {
//     res.send('首页');
// });
// app.post('/login', (req, res) => {
//     res.send(req.body.username + "==========" + req.body.password);
// })
// app.listen(3434, () => {
//     console.log("开始：http://192.168.26.171:3434");
// })

// 第三遍
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(express.static('public'));
app.get('/', (req, res) => {
    res.send('首页');
});
app.post('/login', (req, res) => {
    res.send(req.body.username + "===============" + req.body.password);
});
app.listen(3434, () => {
    console.log("开始：http://192.168.26.171:3434");
})