// 一、 使用node.js完成文件操作和http模块操作， 要求
// 1.1、 创建文件‘ exam.html’ 并且使用node.js增加内容“ 我叫王大锤， 今年18岁， 我的特点是能吃能喝能睡”；
// 1.2、 使用http模块， 开启服务， 并且使用9999端口；
// 1.3、 访问” http: //127.0.0.1:7878”,显示“欢迎光临”，当访问”http://127.0.0.1:7878/exam.html”，把文件”exam.html”的内容显示在页面上;
//     1.4、 访问其他url显示“ 404, 页面找不到”；

// 引入模块
// 创建fs
const fs = require('fs');
// 创建path
const path = require('path');
// 创建http
const http = require('http');
// 服务器
const server = http.createServer();
// 声明变量
let html = `<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<body>
    <h1>我叫王大锤,今年18岁,的特点是能吃能喝能睡</h1>
</body>

</html>`;
// 创建文件
fs.writeFile(path.join(__dirname, './views/exam.html'), html, (err) => {
    if (err) return console.log(err.message);
})
// 监听服务器
server.on('request', (req, res) => {
    // 编码格式
    res.writeHead(200, {
        "Content-Type": "text/html;charset=UTF-8"
    })
    // 判断是否为 /
    if (req.url === '/') {
        res.end('欢迎光临');

    } else if (req.url === '/exam.html') { //判断是否为/exam.html
        // 读取文件
        fs.readFile(path.join(__dirname, './public/exam.html'), "utf8", (err, data) => {
            if (err) return console.log(err.message);
            res.end(data);
        })
    } else {
        res.end('404, 页面找不到');
    }


})
// 开始服务器
server.listen(7878, () => {
    console.log('服务器启动： http://127.0.0.1:7878');
})