const express = require('express');
const fs = require('fs');
const path = require('path');
const app = express();
app.use(express.static('public'));
app.get('/login', (req, res) => {
    res.send(`<!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <style>
        input {
            margin-bottom: 5px;
        }
        </style>
    </head>

    <body>
        <form action="/lotin" method="GET">
            <h1>登录</h1>
            用户名: <input type="text" name="username"></br>
            密码: <input type="password" name="password">
        </form>
    </body>

    
    </html>`);
    //获取到每次注册的用户信息
    var str2 = req.query;
    //空数组
    var arr2 = [];
    //读取json文件
    fs.readFile(path.join(__dirname, './login.json'), 'utf8', (err, data) => {
        //判断json有没有内容
        if (data === '') {
            //把用户信息添加至数组里
            arr2.push(str2);
            //把数组用户信息数组变成json字符串
            var obj = JSON.stringify(arr2);
            //把用户信息写入login文件里
            fs.writeFile(path.join(__dirname, './login.json'), obj, 'utf8', err => {
                console.log(err);
            })
        } else {
            //获取json文件里的数据并转为对象或数组
            var str = JSON.parse(data);
            //json数据的数组录入用户信息
            str.push(str2);
            // for (var i = 0; i < str.length; i++) {
            //     arr2.push(str[i]);
            // }
            // arr2.push(str2);
            //把数组变为json字符串
            var obj2 = JSON.stringify(str);
            //写入json文件
            fs.writeFile(path.join(__dirname, './login.json'), obj2, 'utf8', err => {
                console.log(err);
            })
        }
    })
})
app.get('/lotin', (req, res) => {
    fs.readFile(path.join(__dirname, './login.json'), 'utf8', (err, data) => {
        var obj = JSON.parse(data);
        var flag = false;
        for (let i = 0; i < obj.length; i++) {
            if (req.query.username === obj[i].username && req.query.password === obj[i].password) {
                flag = true;
                break;
            } else {
                flag = false;
            }
        }
        if (flag) {
            res.send('欢迎来到我的页面');
        } else {
            res.send('验证失败');
        }
    })
})
app.listen(3434, () => {
    console.log('http://127.0.0.1:3434')
})