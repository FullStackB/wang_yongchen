// 原生Node根据不同路径返回不同内容
// const http = require('http');
// const server = http.createServer();

// server.on('request', (req, res) => {
//     res.writeHead(200, {
//         'Content-Type': 'text/html;charset=utf8'
//     })
//     if (req.url === '/') {
//         res.end("首页")
//     } else if (req.url === '/about.html') {
//         res.end("关于")
//     }
// })

// server.listen(80, () => {
//     console.log("服务器已开始。。。");
// })


// express
// const express = require('express');
// const app = express();
// app.get('/', (req, res) => {
//     res.send('首页')
// })
// app.get('/about.html', (req, res) => {
//     res.send('关于')
// })
// app.listen(80, () => {
//     console.log("开始。。。。");

// })