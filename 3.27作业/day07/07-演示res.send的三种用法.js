// send的三种用法
// 1.发送字符串给浏览器
// res.send("我是send发送的字符串");
// 2.发送对象或数组给浏览器
// res.send(['a', 'b', 'c']);
// res.send({ name: "jerry", age: 18 })
// 3.发送buffer 这种二进制数据给浏览器
const express = require('express');
const fs = require('fs');
const path = require('path');
const app = express();
// app.use(express.static('public'));
app.get('/', (req, res) => {
    // 01
    // res.send("没时间 你好")

    // 02
    // res.send([1, 2, 3, 4, 5, 6, 7, 8, 9]);
    // res.send({name:"msj", age:19});

    // 03   加个 utf-8 会打印出来  但是 是以 utf-8的格式打印出来  不是我们想要的结果
    // fs.readFile(path.join(__dirname, "./music/毛不易 - 像我这样的人 (Live).flac"), 'utf8', (err, data) => {
    //     if (err) return console.log(err.message);
    //     res.send(data);
    // })
    fs.readFile(path.join(__dirname, "./music/毛不易 - 像我这样的人 (Live).flac"), (err, data) => {
        if (err) return console.log(err.message);
        res.send(data);
    })
});
app.listen(80, () => {
    console.log("服务器开始。。。");
})