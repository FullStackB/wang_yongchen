// 1.引入express 包
const express = require('express');

const path = require('path');
// 2.使用expressc创建服务器
const app = express();

// // 3.设置静态资源目录
app.use(express.static('public'));
app.get('/', (req, res) => {
  res.sendFile(path.join(__dirname, './public/index.html'));
})

// 5.启动服务 指定端口
app.listen(80, () => {
  console.log('服务器运行中...1');
})

// 第二
// const express = require('express');
// const path = require('path');
// const app = express();
// app.use(express.static('public'));
// app.get('/', (req, res) => {
//   res.sendFile(path.join(__dirname, './public/index.html'));
// })
// app.listen(80, () => {
//   console.log('服务器运行中...1');
// })


// 第三
// const express = require('express');
// const path = require('path');
// const app = express();
// app.use(express.static('public'));
// app.get('/', (req, res) => {
//   res.sendFile(path.join(__dirname, './public/index.html'));
// })
// app.listen(80, () => {
//   console.log('服务器运行中...1');
// })

