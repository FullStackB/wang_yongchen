// 使用模板引擎更好 
// 模板引擎是什么 是相当于 冰棍和 模具的关系

// 常见的模板引擎有: 1.art-template   2.ejs  3.swig  4.pug   5.handlebars  6. baidutemplate

// arttempalte的使用
// 1.下包
// 2.引入包
// 3.使用  结果是一个拼接好的html页面 = template(模板的地址, 必须是一个对象)
// const http = require('http');
// const template = require('art-template');
// const path = require('path');
// const server = http.createServer();

// let arr = [
//     "[上海] \"Higher Brothers 2019恭喜发财 WISH YOU RICH 世界巡演\"",
//     "2019\"绝色\"莫文蔚巡回演唱会—天津站",
//     "[廊坊] 2019中国平安中超联赛第三轮 河北华夏幸福vs上海上港",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月"
// ]
// server.on('request', (req, res) => {
//     let result = template(path.join(__dirname, './public/yongle.html'), {
//         arr: arr
//     });
//     res.end(result);
// })
// server.listen(80, () => {
//     console.log("服务器开始。。。");

// })

// 第二\

// let arr = [
//     "[上海] \"Higher Brothers 2019恭喜发财 WISH YOU RICH 世界巡演\"",
//     "2019\"绝色\"莫文蔚巡回演唱会—天津站",
//     "[廊坊] 2019中国平安中超联赛第三轮 河北华夏幸福vs上海上港",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
//     "[武汉] 2019年中国·武汉速度赛马公开赛（3月"
// ]
// const http = require('http');
// const server = http.createServer();
// const path = require('path');
// const template = require('art-template');
// server.on('request', (req, res) => {
//     let ser = template(path.join(__dirname, "./public/yongle.html"), {
//         arr: arr
//     });
//     res.end(ser)
// });
// server.listen(80, () => {
//     console.log("服务器开始...");
// })


// 第三
let arr = [
    "[上海] \"Higher Brothers 2019恭喜发财 WISH YOU RICH 世界巡演\"",
    "2019\"绝色\"莫文蔚巡回演唱会—天津站",
    "[廊坊] 2019中国平安中超联赛第三轮 河北华夏幸福vs上海上港",
    "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
    "[武汉] 2019年中国·武汉速度赛马公开赛（3月",
    "[武汉] 2019年中国·武汉速度赛马公开赛（3月"
]
const http = require('http');
const server = http.createServer();
const path = require('path');
const template = require('art-template');
server.on('request', (req, res) => {
    let ser = template(path.join(__dirname, "./public/yongle.html"), {
        arr: arr
    });
    res.end(ser)
});
server.listen(80, () => {
    console.log("服务器开始...");
})