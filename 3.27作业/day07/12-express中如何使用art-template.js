// express中配置模板引擎
// 1.下载
// npm install --save art-template
//  

// 2.配置模板的后缀
// app.engine('html', require('express-art-template'));
// 3.配置模板所在的路径
// app.set('views', './views')    第一个views代表的是模板(不能改)  第二个view代表的是模板的路径
// 4.把模板和数据拼接起来(也叫绑定起来)
// res.render('students.html', { students: students });
// res.render('模板名称', 数据对象(必须是对象));

const express = require('express');
const app = express();
app.engine('html', require('express-art-template'));
app.set('views', './views');

let students = [{
    name: '张三',
    age: '18',
    sex: "男",
    hobby: '篮球'
}, {
    name: '张三1',
    age: '18',
    sex: "男",
    hobby: '养马'
}, {
    name: '张三2',
    age: '18',
    sex: "男",
    hobby: '汤头'
}, {
    name: '张三3',
    age: '18',
    sex: "男",
    hobby: '抽烟'
}, {
    name: '张三4',
    age: '19',
    sex: "女",
    hobby: '篮球'
}]

app.get('/', (req, res) => {
    res.render('students.html', {
        students: students
    })
});
app.listen(80, () => {
    console.log("服务器开始。。。");
});

// 第二
// let students = [{
//     name: '张三',
//     age: '18',
//     sex: "男",
//     hobby: '篮球'
// }, {
//     name: '张三1',
//     age: '18',
//     sex: "男",
//     hobby: '养马'
// }, {
//     name: '张三2',
//     age: '18',
//     sex: "男",
//     hobby: '汤头'
// }, {
//     name: '张三3',
//     age: '18',
//     sex: "男",
//     hobby: '抽烟'
// }, {
//     name: '张三4',
//     age: '19',
//     sex: "女",
//     hobby: '篮球'
// }]
// const express = require('express');
// const app = express();
// app.engine('html', require('express-art-template'));
// app.set('views', './views');

// app.get('/', (req, res) => {
//     res.render('students.html', {
//         students: students
//     })
// });
// app.listen(80, () => {
//     console.log("服务器开始。。。");
// });

// 第三
// let students = [{
//     name: '张三',
//     age: '18',
//     sex: "男",
//     hobby: '篮球'
// }, {
//     name: '张三1',
//     age: '18',
//     sex: "男",
//     hobby: '养马'
// }, {
//     name: '张三2',
//     age: '18',
//     sex: "男",
//     hobby: '汤头'
// }, {
//     name: '张三3',
//     age: '18',
//     sex: "男",
//     hobby: '抽烟'
// }, {
//     name: '张三4',
//     age: '19',
//     sex: "女",
//     hobby: '篮球'
// }]
// const express = require('express');
// const app = express();
// app.engine('html', require('express-art-template'));
// app.set('views', './views');
// app.get('/', (req, res) => {
//     res.render('students.html', {
//         students: students
//     })
// });
// app.listen(80, () => {
//     console.log("服务器开始。。。");
// });