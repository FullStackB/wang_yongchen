// 引入模块
const express = require('express');
const fs = require('fs');
const path = require('path');
const bodyParser = require('body-parser');
// 服务器
const app = express();
// express-art-template 模块引入
app.engine('html', require('express-art-template'));
// 配置模板所在的路径
app.set('views', './public');
// 编码格式
app.use(bodyParser.urlencoded({
    extended: false
}));
// 静态资源
app.use(express.static('public'));
// 刚开始
app.get('/', (req, res) => {
    // 读取刚开始有咩有
    fs.readFile(path.join(__dirname, "./login.json"), 'utf8', (err, data) => {
        // 如果没有的话 添加个数组   有的话获取 里面的内容 转换成数组形式 输出到页面中
        if (data == '') {
            // 写入空数组
            fs.writeFile(path.join(__dirname, "./login.json"), '[]', (err) => {
                if (err) return console.log(err);
            })
        } else {
            // 写入到html中 由html打印到页面中
            res.render('index1.html', {
                students: JSON.parse(data)
            });
        }
    })
})
// 添加用户
app.get('/login', (req, res) => {
    // 页面
    res.sendFile(path.join(__dirname, './public/get.html'));
    // 我要读取 logon的信息 只能 进入这个页面后也才能 得到想要的信息 有bug 只能用这个终止
    app.post('/logon', (req, res) => {
        // 获取输入的信息
        var str = req.body;
        // 空数组
        var arr = [];
        // 读取文件
        fs.readFile(path.join(__dirname, "./login.json"), 'utf8', (err, data) => {
            // 判断是否读取失败
            if (err) {
                return console.log(err);
                // 是否有数据 防止第一次
            } else if (data == "") {
                // 添加到数组
                arr.push(str);
                // 变成字符串 
                var obj = JSON.stringify(arr);
                // 写入json中
                fs.writeFile(path.join(__dirname, "./login.json"), obj, 'utf8', (err) => {
                    if (err) return console.log(err);
                })
                // 一写入完之后 立刻添加到页面中
                res.render('index1.html', {
                    students: JSON.parse(obj)
                });
            } else {
                // 获取json 中的数据
                var str1 = JSON.parse(data);
                // 添加到数组
                str1.push(str);
                // 变成字符串 
                var obj1 = JSON.stringify(str1);
                // 写入json中
                fs.writeFile(path.join(__dirname, "./login.json"), obj1, 'utf8', (err) => {
                    if (err) return console.log(err);
                })
                // 一写完之后 立刻添加到页面中
                res.render('index1.html', {
                    students: JSON.parse(obj1)
                });
            }

        });
    })
});
// 删除
// +
// 端口
app.listen(80, () => {
    console.log("至高无上的服务器开启了。。。");
})