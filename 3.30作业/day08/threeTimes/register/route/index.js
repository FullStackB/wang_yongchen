const express = require('express');

// 调用express的Router()方法 来创建路由
const router = express.Router();

// 调用controller
const controller = require('../controller');

// 注册
router.get('/', controller.index);

// 展示
router.get('/show', controller.show);
// 添加
router.get('/adduser',controller.adduser);
// 删除
module.exports = router;