const express = require('express');
// 服务器
const app = express();
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}))
// 静态
app.use('/public', express.static('public'));
// app.use(express.static('public'))
app.engine('html', require('express-art-template'));
app.set('views', 'public');
// 路由
const router = require('./route');
app.use(router);

app.listen(80, () => {
    console.log("至高无上的服务器开始运行了。。。");
})