const express = require('express');

const router = express.Router();

const controller = require('../controller');

router.get('/', controller.index);
// 查询
router.get('/admin.html', controller.admin);
// 添加
router.get('/add.html', controller.add);
router.get('/login', controller.login);
// 删除
router.get('/del', controller.del);


// 暴漏
module.exports = router;