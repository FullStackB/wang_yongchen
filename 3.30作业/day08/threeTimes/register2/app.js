// 引入模块
const express = require('express');
const app = express();
// 引入bodyParser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));
// 配置静态资源目录
app.use('/public', express.static('public'));
// 配置模板
app.engine('html', (require('express-art-template')));
app.set('views', 'public');

// 配置路由
const router = require('./route');
app.use(router);

// 服务器
app.listen(80, () => {
    console.log("至高无上的服务器开始运行了...");
})