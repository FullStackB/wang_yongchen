const mysql = require('mysql');
const conn = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: '123456',
    database: 'msj'
})

module.exports.index = (req, res) => {
    res.render('index.html')
};
module.exports.add = (req, res) => {
    res.render('add.html')
};
// 查询
module.exports.admin = (req, res) => {
    conn.query('select * from users', (err, result) => {
        if (err) return console.log(err);
        res.render('admin.html', ({
            teachers: result
        }))
    })
};

// 添加
module.exports.login = (req, res) => {
    conn.query('insert into users(username,age,sex,password) values ("' + req.query.username + '","' + req.query.age + '","' + req.query.sex + '","' + req.query.password + '")', (err, result) => {
        if (err) return console.log(err);
        res.redirect('admin.html')
    })
};
// 删除
module.exports.del = (req, res) => {
    conn.query('delete from users where id=' + req.query.id, (err, result) => {
        if (err) return console.log(err);
      
    })
    res.redirect('admin.html')
};