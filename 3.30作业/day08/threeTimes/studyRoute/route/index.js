const express = require('express');
const router = express.Router();
let index = (req, res) => {
  res.render('index.html', {
    title: "首页"
  })
};
let about = (req, res) => {
  res.render('about.html', {
    title: "关于我们"
  })
};
let music = (req, res) => {
  res.render('about.html', {
    title: "音乐欣赏"
  })
};
router.get('/', index);
router.get('/about', about);
router.get('/music', music);
module.exports = router;