// const express = require('express');
// const app = express();
// // 配置模板引擎
// app.engine('html', require('express-art-template'));
// app.set('views', 'views');
// // 配置路由
// const route = require('./route');
// app.use(route);


// // 服务器开始
// app.listen(80, () => {
//   console.log("至高无上的服务器开始。。。。");
// })


const express = require('express');
const app = express();
// 配置模板引擎
app.engine('html', require('express-art-template'));
app.set('views', 'views');
// 路由
const route = require('./route');
app.use(route);
// 服务器开始
app.listen(80, () => {
  console.log("至高无上的服务器开始。。。。");
})