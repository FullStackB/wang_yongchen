// // 引入模块
// const express = require('express');
// // 路由   express方法中的 Router()
// const router = express.Router();
// // 路由
// const controller = require('../controller');

// router.get('/', controller.index);
// router.get('/about', controller.about);
// router.get('/music', controller.music);
// // 暴露
// module.exports = router;

const express = require('express');
const router = express.Router();
// 路由
const controller = require('../controller');

router.get('/', controller.index);
router.get('/about', controller.about);
router.get('/music', controller.music);
// 暴露
module.exports = router;