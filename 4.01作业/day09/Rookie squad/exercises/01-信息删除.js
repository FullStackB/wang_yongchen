// 移除数组 arr 中的所有值与 item 相等的元素。不要直接修改数组 arr，结果返回新的数组


//arr传入的数组，item具体元素

// function remove(arr, item) {

// var newArr = []; //返回的数组

//完成代码部分


// }

function remove(arr, item) {
    // 定义数组变量
    var newArr = [];
    // 遍历
    arr.forEach(function (e) {
        // 判断
        if (e != item) {
            // newArr[newArr.length] = e;
            // 从后插入
            newArr.push(e);
        }
    })
    // console.log(newArr);
    // 返回值
    return newArr;

}

// remove([1, 2, 3, 4, 5, 6], 6);
// 打印
console.log(remove([1, 2, 3, 4, 5, 6], 6));;