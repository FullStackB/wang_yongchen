const mysql = require('mysql');
const conn = mysql.createConnection({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: '123456',
    database: 'msj'
})
// 展示页面
module.exports.index = (req, res) => {
    res.render('index.html')
}

// 查询用户
module.exports.show = (req, res) => {
    conn.query('select * from users', (err, result) => {
        if (err) return console.log(err);
        res.render('show.html', {
            list: result
        })
    })
}
// 添加用户
module.exports.adduser = (req, res) => {
    conn.query('INSERT INTO users (username,password) VALUES ("' + req.body.username + '","' + req.body.password + '")', (err, result) => {
        if (err) return console.log(err);
        res.redirect('/show');
    })
}

// 删除用户
module.exports.deluser = (req, res) => {
    conn.query('delete from users where id=' + req.query.id, (err, result) => {
        if (err) return console.log(err);
        res.redirect('/show');
    })
}

// 修改用户
module.exports.gai = (req, res) => {
    var obj = req.query.id;
    var arr = [];
    arr.push(obj);
    res.render('gai.html', {
        list: arr
    })
}
// 更新数据后 刷新页面
module.exports.update = (req, res) => {
    var od = parseInt(req.body.id);
    conn.query('update users set username=("' + req.body.username + '"),password=("' + req.body.password + '") where id =("' + od + '")', (err, result) => {
        if (err) return console.log(err);
    })
    res.redirect('/show');
}