const express = require('express');
const router = express.Router();

const controller = require('../controller');
// 展示页面
router.get('/', controller.index);
// 查询用户
router.get('/show', controller.show);
// 添加用户
router.post('/adduser', controller.adduser);
// 删除用户
router.get('/deluser', controller.deluser);
// 修改用户
router.get('/gai', controller.gai);
// 更新数据后 刷新页面
router.post('/update', controller.update);

// 暴露
module.exports = router;