const express = require('express');
const app = express();

// 配置body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));
// 静态资源 暂时没有

// 配置引擎
app.engine('html', require('express-art-template'));
app.set('views', 'views');

// 配置路由
const route = require('./route');
app.use(route);


// 端口
app.listen(80, () => {
    console.log('至高无上的服务器开始运行了...');
})