// 添加路由express框架
const express = require('express');
// 使用Router创建路由框架
const router = express.Router();
// 调用业务处理模块
const controller = require('../controller');
// 展现页面
router.get('/', controller.index);
// 添加用户
router.post('/adduser', controller.adduser);
// 查询用户
router.get('/show', controller.show);
// 删除用户
router.get('/del', controller.del);
// 修改用户
router.get('/gai',controller.gai);
// 更改数据并跳转到表格也页面
router.get('/update', controller.update);

// 暴漏
module.exports = router;