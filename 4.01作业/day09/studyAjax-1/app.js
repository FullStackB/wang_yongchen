const express = require('express');
const app = express();
const bodyParer = require('body-parser');
app.use(bodyParer.urlencoded({
    extended: false
}))
// 静态资源
app.use('/public', express.static('public'));
// 初始化页面
app.get('/', (req, res) => {
    res.sendFile('./views/get.html', {
        root: __dirname
    })
});
// get方式
app.get('/user', (req, res) => {
    res.send(req.query);
})
// post方式
app.post('/user', (req, res) => {
    res.send(req.body);
})

// 端口
app.listen(80, () => {
    console.log('至高无上的服务器开始运行了。。。');
})