// const express = require('express');
// const app = express();

// // 配置接受post请求的body-parser
// const bodyParser = require('body-parser');
// app.use(bodyParser.urlencoded({
//     extended: false
// }));

// // 配置静态目录
// app.use('/public', express.static('public'));

// // 配置模板

// app.get('/', (req, res) => {
//     res.sendFile('./views/get.html', {
//         root: __dirname
//     })
// });

// // get 请求
// // app.get('/user', (req, res) => {
// //     res.send(req.query);
// // })

// // post请求
// app.post('/user', (req, res) => {
//     res.send(req.body)
// })

// // 启动服务并指定端口
// app.listen(80, () => {
//     console.log("至高无上的服务器开始运行了...");
// })


// 第二遍

const express = require('express');
const app = express();

// 配置接受post请求的body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));

// 配置静态目录
app.use('/public', express.static('public'));

// 配置模板

app.get('/', (req, res) => {
    res.sendFile('./views/get.html', {
        root: __dirname
    })
});

// get 请求
app.get('/user', (req, res) => {
    res.send(req.query);
})

// post请求
app.post('/user', (req, res) => {
    res.json(req.body)
})

// 启动服务并指定端口
app.listen(80, () => {
    console.log("至高无上的服务器开始运行了...");
})