const express = require('express');
const app = express();
// body-parser
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}));
// 静态资源
app.use(express.static('./public'));

// 配置引擎 
app.engine('html', require('express-art-template'));
app.set('views', 'views');

// 配置路由
const router = require('./route');
app.use(router);

// 服务器
app.listen(80, () => {
    console.log('至高无上的服务器开始运行了...');
})