const express = require('express');
const router = express.Router();

const controller = require('../controller');
// 初始化
router.get('/', controller.index);
// 展示
router.get('/show', controller.show);
// 添加
router.post('/createUser', controller.createUser);
// 删除
router.get('/del', controller.del);

module.exports = router;