const express = require('express');
const app = express();

// body-parser 模板
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: false
}))
// 静态资源
app.use(express.static('./public'));

// 配置引擎模板
app.engine('html', require('express-art-template'));
app.set('views', 'views');

// 路由
const route = require('./route');
app.use(route);

app.listen(80, () => {
    console.log('至高无上的服务器开始运fsfsd行了...');
})