const express = require('express');
const router = express.Router();

const controller = require('../controller');

// 默认
router.get('/', controller.index);
// 展示
router.get('/show', controller.show);
// 添加
router.post('/adduser', controller.adduser);
// 删除
router.post('/del', controller.del);


// 暴露
module.exports = router;