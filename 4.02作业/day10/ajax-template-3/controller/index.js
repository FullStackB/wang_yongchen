const connection = require('../data');

// 默认显示
module.exports.index = (req, res) => {
    res.render('index.html')
}
// 展示
module.exports.show = (req, res) => {
    connection.query('select * from users', (err, result) => {
        if (err) return console.log(err);
        res.json(result)
    })
}
// 添加
module.exports.adduser = (req, res) => {
    connection.query('INSERT INTO users (username,password) VALUES ("' + req.body.username + '","' + req.body.password + '")', (err, result) => {
        if (err) return console.log(err);
        res.json({
            code: 100,
            msg: '添加成功'
        })
    })
}

// 删除
module.exports.del = (req, res) => {
    connection.query('delete from users where id=' + req.body.id, (err, result) => {
        if (err) return console.log(err);
        res.json({
            code: 101,
            msg: '删除成功'
        })
    })
}