const connection = require('../data');

module.exports.index = (req, res) => {
  res.render('index.html');
}
// 添加用户
module.exports.createUser = (req, res) => {
  connection.query('INSERT INTO users (username,password) VALUES ("' + req.body.username + '","' + req.body.password + '")', (err, result) => {
    if (err) return console.log(err);
    res.json({
      code: 10000,
      msg: "添加成功"
    })
  })
}
// 查询
module.exports.show = (req, res) => {
  connection.query('select * from users', (err, result) => {
    if (err) return console.log(err);
    res.json(result);
  })
}

// 删除
module.exports.del = (req, res) => {
  connection.query('delete from users where id=' + req.query.id, (err, result) => {
    if (err) return console.log(err);
    res.json({
      code: 10001,
      msg: "删除成功"
    });
  })
}