const express = require('express');
const router = express.Router();

const connection = require('../controller');

router.get('/', connection.index);
// 添加用户
router.post('/createUser', connection.createUser);
// 查询
router.get('/show', connection.show);
// 删除
router.get('/del', connection.del);


module.exports = router;