const express = require('express');
const router = express.Router();

const connection = require('../controller');

router.get('/', connection.index);
router.get('/show', connection.show);

module.exports = router;