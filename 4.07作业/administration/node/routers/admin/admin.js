let express = require("express");

let router = new express.Router();

const connection = require("../../config/db.js");

const crypto = require('crypto');
// 导入moment模块
const moment = require("moment");

// 管理员管理首页
router.get('/', function (req, res, next) {
	// 获取地址栏数据 req.query
	connection.query('SELECT * FROM user',(err,result)=>{
		// 获取表单数据 req.body
		// res.send("管理员管理首页");
		// 从数据库中查询数据
		// 判断是否执行成功
			if(err)return console.log(err);
		// 加载页面
			res.render('admin/admin/index.html',{
				data:result,search: ""
			})
	})
});
// 管理员管理添加页面
router.get('/add', function (req, res, next) {
	// res.send("管理员管理添加页面");
	// 加载页面
	res.render("admin/admin/add.html");

});
// 管理员的添加功能
router.post("/add", function (req, res, next) {
	// 判断用户名长度
	// 判断密码
	// 判断两次密码师傅一直
	// 判断该用户名是否已经注册
	// 判断是否有错误
	// 判断该用户名是否注册
	// 没有注册，我们需要插入数据
	// 当前时间戳
	// 密码加密
	// 判断
	// 判断是否执行成
	// 接受数据
	connection.query('INSERT INTO user (username,password,repassword) VALUES ("'+req.body.username+'","'+req.body.password+'","'+req.body.repassword+'")',(err,result)=>{
		// 判断用户名是否书写
		if(err)return console.log(err);
		// 获取地址栏数据 req.query
		connection.query('SELECT * FROM user',(err,result)=>{
			// 获取表单数据 req.body
			// res.send("管理员管理首页");
			// 从数据库中查询数据
			// 判断是否执行成功
				if(err)return console.log(err);
			// 加载页面
				res.render('admin/admin/index.html',{
				data:result,search: ""
				})
		})
	})
});

// 无刷新修改状态
router.get("/ajax_status", function (req, res, next) {
	// 接受对应的数据
	// 修改数据
	connection.query('select * from user',(error,result)=> {
		if(error){
			console.log(error);
		}else{
			let newArr = result.filter((ele, index) => {
				return ele.username.indexOf(req.query.search) != -1;
			});
		res.render('admin/admin/index',{data:newArr,search:req.query.search});
		}
		})
	
})

// 管理员管理修改页面
router.get('/edit', function (req, res, next) {
	// 加载修改页面
	// 接受数据的ID
	let myId = req.query.id;
	// 查询对应数据
	//数据读取
	connection.query('select * from user where id=' + myId,(error,result)=> {
		if(error){
			console.log(error);
		}else{
			res.render('admin/admin/edit',{data:{
				id:result[0].id,
				username:result[0].username
			}});
		}
	})
});

// 管理员数据修改功能
router.post("/edit", function (req, res, next) {
	// 接受表单提交的数据
	// 判断该用户是否修改密码
	// 密码加密
	// sql语句
	// sql语句
	// 执行sql语句
		// // 接受表单提交的数据
		let reqData = req.body;
		// // 更改数据
		connection.query('update user set username =("'+reqData.username+'"),password = ("'+reqData.password+'") where id =("'+reqData.id+'")',(error,result)=> {
			if(error){
				console.log(error);
			}else{
				res.redirect('/admin/');
			}
		})  // ----修改数据
});

// 无刷新删除数据
router.get("/ajax_del", function (req, res, next) {
	// res.send("管理员管理首页");
	// 接受地址栏数据
	// 删除数据
	// 判断是否执行成功
    connection.query('delete from user where id=' + req.query.id, (err, result) => {
		if (err) return console.log(err);
		res.json({
			code:'0'
		})
    })
})

module.exports = router;