-- 登陆mysql
PS G:\vs code\Mysql> mysql

-- 创建数据库
mysql> create database czxy;

-- 选择数据库
mysql> use czxy;

-- 查看数据库
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| czxy               |
| mei                |
| msj                |
| mysql              |
| performance_schema |
| sys                |
| wyc                |
| wyc1               |
| wyc2               |
| zhangsan           |
+--------------------+
11 rows in set (0.00 sec)

-- 创建数据表
mysql> create table czxy.user(id varchar(100),username varchar(10),sex varchar(2));

-- 显示所有数据表
mysql> show tables;
+----------------+
| Tables_in_czxy |
+----------------+
| user           |
+----------------+
1 row in set (0.00 sec)

-- 显示数据表的结构
mysql> desc user;
+----------+--------------+------+-----+---------+-------+
| Field    | Type         | Null | Key | Default | Extra |
+----------+--------------+------+-----+---------+-------+
| id       | varchar(100) | YES  |     | NULL    |       |
| username | varchar(10)  | YES  |     | NULL    |       |
| sex      | varchar(2)   | YES  |     | NULL    |       |
+----------+--------------+------+-----+---------+-------+
3 rows in set (0.00 sec)
-- 原本创建了 news  删除数据表
mysql> drop table news;

-- 创建 数据表字段 这样只能一个一个的创建     三个放在一块
mysql> alter table user add Elail varchar(255);
mysql> alter table user add phong int(12);
mysql> alter table user add resume varchar(255);

-- 显示数据表的结构
mysql> desc user;
+----------+--------------+------+-----+---------+-------+
| Field    | Type         | Null | Key | Default | Extra |
+----------+--------------+------+-----+---------+-------+
| id       | varchar(100) | YES  |     | NULL    |       |
| username | varchar(10)  | YES  |     | NULL    |       |
| sex      | varchar(2)   | YES  |     | NULL    |       |
| Elail    | varchar(255) | YES  |     | NULL    |       |
| phong    | int(12)      | YES  |     | NULL    |       |
| resume   | varchar(255) | YES  |     | NULL    |       |
+----------+--------------+------+-----+---------+-------+
6 rows in set (0.00 sec)

-- 创建数据表 和 数据表字段 
mysql> create table czxy.news(newsId varchar(100), newsTitle varchar(255), newsOutline varchar(255), newsContent varchar(255), newsEvent varchar(255), newsClickNumber int(255), newsCollectionNumber varchar(255));

-- 显示数据表的结构
mysql> desc news;
+----------------------+--------------+------+-----+---------+-------+
| Field                | Type         | Null | Key | Default | Extra |
+----------------------+--------------+------+-----+---------+-------+
| newsId               | varchar(100) | YES  |     | NULL    |       |
| newsTitle            | varchar(255) | YES  |     | NULL    |       |
| newsOutline          | varchar(255) | YES  |     | NULL    |       |
| newsContent          | varchar(255) | YES  |     | NULL    |       |
| newsEvent            | varchar(255) | YES  |     | NULL    |       |
| newsClickNumber      | int(255)     | YES  |     | NULL    |       |
| newsCollectionNumber | varchar(255) | YES  |     | NULL    |       |
+----------------------+--------------+------+-----+---------+-------+
7 rows in set (0.00 sec)

-- 创建数据表 和 数据表字段 
mysql> alter table news add msj varchar(2);

-- 显示数据表的结构
mysql> desc news;
+----------------------+--------------+------+-----+---------+-------+
| Field                | Type         | Null | Key | Default | Extra |
+----------------------+--------------+------+-----+---------+-------+
| newsId               | varchar(100) | YES  |     | NULL    |       |
| newsTitle            | varchar(255) | YES  |     | NULL    |       |
| newsOutline          | varchar(255) | YES  |     | NULL    |       |
| newsContent          | varchar(255) | YES  |     | NULL    |       |
| newsEvent            | varchar(255) | YES  |     | NULL    |       |
| newsClickNumber      | int(255)     | YES  |     | NULL    |       |
| newsCollectionNumber | varchar(255) | YES  |     | NULL    |       |
| msj                  | varchar(2)   | YES  |     | NULL    |       |
+----------------------+--------------+------+-----+---------+-------+
8 rows in set (0.00 sec)

-- 更改数据表的名字 
mysql> rename table news to uis;

-- 显示数据表
mysql> show tables;
+----------------+
| Tables_in_czxy |
+----------------+
| uis            |
| user           |
+----------------+
2 rows in set (0.00 sec)

-- 更改数据表的名字 
mysql> rename table uis to news;

-- 显示数据表
mysql> show tables;
+----------------+
| Tables_in_czxy |
+----------------+
| news           |
| user           |
+----------------+
2 rows in set (0.00 sec)

-- 修改字段名
mysql> alter table news change msj ser int(2);

-- 显示数据表的结构
mysql> desc news;
+----------------------+--------------+------+-----+---------+-------+
| Field                | Type         | Null | Key | Default | Extra |
+----------------------+--------------+------+-----+---------+-------+
| newsId               | varchar(100) | YES  |     | NULL    |       |
| newsTitle            | varchar(255) | YES  |     | NULL    |       |
| newsOutline          | varchar(255) | YES  |     | NULL    |       |
| newsContent          | varchar(255) | YES  |     | NULL    |       |
| newsEvent            | varchar(255) | YES  |     | NULL    |       |
| newsClickNumber      | int(255)     | YES  |     | NULL    |       |
| newsCollectionNumber | varchar(255) | YES  |     | NULL    |       |
| ser                  | int(2)       | YES  |     | NULL    |       |
+----------------------+--------------+------+-----+---------+-------+
8 rows in set (0.00 sec)

-- 删除数据表中的字段
mysql> alter table news drop ser;

-- 显示数据表的结构
mysql> desc news;
+----------------------+--------------+------+-----+---------+-------+
| Field                | Type         | Null | Key | Default | Extra |
+----------------------+--------------+------+-----+---------+-------+
| newsId               | varchar(100) | YES  |     | NULL    |       |
| newsTitle            | varchar(255) | YES  |     | NULL    |       |
| newsOutline          | varchar(255) | YES  |     | NULL    |       |
| newsContent          | varchar(255) | YES  |     | NULL    |       |
| newsEvent            | varchar(255) | YES  |     | NULL    |       |
| newsClickNumber      | int(255)     | YES  |     | NULL    |       |
| newsCollectionNumber | varchar(255) | YES  |     | NULL    |       |
+----------------------+--------------+------+-----+---------+-------+
7 rows in set (0.00 sec)

-- 创建数据表
mysql> create database czxy2;

-- 显示数据表
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| czxy               |
| czxy2              |
| mei                |
| msj                |
| mysql              |
| performance_schema |
| sys                |
| wyc                |
| wyc1               |
| wyc2               |
| zhangsan           |
+--------------------+
12 rows in set (0.00 sec)

-- 删除数据表
mysql> drop database czxy2;

-- 显示数据表
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| czxy               |
| mei                |
| msj                |
| mysql              |
| performance_schema |
| sys                |
| wyc                |
| wyc1               |
| wyc2               |
| zhangsan           |
+--------------------+
11 rows in set (0.00 sec)

-- 删除数据表
mysql> drop database wyc2;

-- 删除数据表
mysql> drop database wyc1;

-- 显示数据表
mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| czxy               |
| mei                |
| msj                |
| mysql              |
| performance_schema |
| sys                |
| wyc                |
| zhangsan           |
+--------------------+
9 rows in set (0.00 sec)

-- 选择数据表
mysql> use czxy;
Database changed

-- 添加字段  在user中添加字段
mysql> alter table user add ser int(2);

-- 显示数据表的结构
mysql> desc user;
+----------+--------------+------+-----+---------+-------+
| Field    | Type         | Null | Key | Default | Extra |
+----------+--------------+------+-----+---------+-------+
| id       | varchar(100) | YES  |     | NULL    |       |
| username | varchar(10)  | YES  |     | NULL    |       |
| sex      | varchar(2)   | YES  |     | NULL    |       |
| Elail    | varchar(255) | YES  |     | NULL    |       |
| phong    | int(12)      | YES  |     | NULL    |       |
| resume   | varchar(255) | YES  |     | NULL    |       |
| ser      | int(2)       | YES  |     | NULL    |       |
+----------+--------------+------+-----+---------+-------+
7 rows in set (0.00 sec)

-- 删除数据表中的字段
mysql> alter table user drop ser;

-- 显示数据表结构
mysql> desc user;
+----------+--------------+------+-----+---------+-------+
| Field    | Type         | Null | Key | Default | Extra |
+----------+--------------+------+-----+---------+-------+
| id       | varchar(100) | YES  |     | NULL    |       |
| username | varchar(10)  | YES  |     | NULL    |       |
| sex      | varchar(2)   | YES  |     | NULL    |       |
| Elail    | varchar(255) | YES  |     | NULL    |       |
| phong    | int(12)      | YES  |     | NULL    |       |
| resume   | varchar(255) | YES  |     | NULL    |       |
+----------+--------------+------+-----+---------+-------+
6 rows in set (0.00 sec)

-- 在数据表中 添加数据
mysql>  insert into user(id,username,sex,Elail,phong,resume) values (1,'msj','男','132311@qq.com',1,'你更好');

-- 查看数据表中的user
mysql> select * from user;
+------+----------+------+---------------+-------+-----------+
| id   | username | sex  | Elail         | phong | resume    |
+------+----------+------+---------------+-------+-----------+
| 1    | msj      | 男   | 132311@qq.com |     1 | 你更好    |
+------+----------+------+---------------+-------+-----------+
1 row in set (0.00 sec)

-- 在数据表中 添加数据
mysql> insert into user(id,username,sex,Elail,phong,resume) values (2,'wyc','男','132311@qq.com',123,'巴巴');

-- 查看数据表中的user
mysql> select * from user;
+------+----------+------+---------------+-------+-----------+
| id   | username | sex  | Elail         | phong | resume    |
+------+----------+------+---------------+-------+-----------+
| 1    | msj      | 男   | 132311@qq.com |     1 | 你更好    |
| 2    | wyc      | 男   | 132311@qq.com |   123 | 巴巴      |
+------+----------+------+---------------+-------+-----------+
2 rows in set (0.00 sec)

-- 显示数据表结构
mysql> desc news;
+----------------------+--------------+------+-----+---------+-------+
| Field                | Type         | Null | Key | Default | Extra |
+----------------------+--------------+------+-----+---------+-------+
| newsId               | varchar(100) | YES  |     | NULL    |       |
| newsTitle            | varchar(255) | YES  |     | NULL    |       |
| newsOutline          | varchar(255) | YES  |     | NULL    |       |
| newsContent          | varchar(255) | YES  |     | NULL    |       |
| newsEvent            | varchar(255) | YES  |     | NULL    |       |
| newsClickNumber      | int(255)     | YES  |     | NULL    |       |
| newsCollectionNumber | varchar(255) | YES  |     | NULL    |       |
+----------------------+--------------+------+-----+---------+-------+
7 rows in set (0.00 sec)

-- 在数据表中 添加数据
mysql> insert into news(newsId,newsTitle,newsOutline,newsContent,newsEvent,newsClickNumber,newsCollectionNumber) values (1,'时光','美','高兴','美好','100','233');

-- 查看数据表中的news
mysql> select * from news;
+--------+-----------+-------------+-------------+-----------+-----------------+----------------------+
| newsId | newsTitle | newsOutline | newsContent | newsEvent | newsClickNumber | newsCollectionNumber |
+--------+-----------+-------------+-------------+-----------+-----------------+----------------------+
| 1      | 时光      | 美          | 高兴        | 美好      |             100 | 233                  |
+--------+-----------+-------------+-------------+-----------+-----------------+----------------------+
1 row in set (0.00 sec)

-- 在数据表中 添加数据
mysql> insert into news(newsId,newsTitle,newsOutline,newsContent,newsEvent,newsClickNumber,newsCollectionNumber) values (2,'学习','学习','学习','先学习','232','255');

-- 查看数据表中的news
mysql> select * from news;
+--------+-----------+-------------+-------------+-----------+-----------------+----------------------+
| newsId | newsTitle | newsOutline | newsContent | newsEvent | newsClickNumber | newsCollectionNumber |
+--------+-----------+-------------+-------------+-----------+-----------------+----------------------+
| 1      | 时光      | 美          | 高兴        | 美好      |             100 | 233                  |
| 2      | 学习      | 学习        | 学习        | 先学习    |             232 | 255                  |
+--------+-----------+-------------+-------------+-----------+-----------------+----------------------+
2 rows in set (0.00 sec)