-- 浮点型又称之为精度类型：是一种有可能丢失精度的数据类型，数据有可能不那么准确（由其是在超出范围的时候）

-- 浮点数的取值范围 10^38次方

-- 浮点数在7位以下还是精确的 只要超过7位 那么就是没那么精确了

-- 浮点数的基本语法: float 
--   float(M,D) M代表整个数字有几位  D代表小数有几位 
-- float(6,2)  1234.23

-- 创建数据表
create table num_float(
    f1 float,
    f2 float(10,2)
);




create table str_key(
    id int not null primary key comment'用户ID',
    name varchar(10) not null
);

create table str_key1(
    id int not null comment'用户ID',
    name varchar(10) not null,
    primary key(id)
);
create table str_key2(
    id int not null comment'用户ID',
    name varchar(10) not null
);
alter table str_key2 add primary key(id);        


create database demo;




create table order(
 id int primary key auto_increment comment '商品id'
);