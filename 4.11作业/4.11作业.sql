-- 1、创建account_log 用户账目日志表
create table account_log (
    log_id mediumint comment '自增 ID 号',
    user_id mediumint comment '用户登录后保存在session中的id号,跟users表中user_id对应',
    user_money decimal(10,2) comment '用户该笔记录的余额',
    frozen_monet decimal(10,2) comment '被冻结的资金',
    rank_points mediumint comment '等级积分,跟消费积分是分开的',
    pay_points mediumint comment '消费积分,跟等级积分是分开的',
    change_time	 int(10) comment '该笔操作发生的时间',
    change_desc varchar(255) comment '该笔操作的备注',
    change_type tinyint comment '操作类型,0为充值,1,为提现,2为管理员调节,99为其它类型'
);
-- 2、创建 ad 广告表
create table ad(
    ad_id smallint comment '自增ID号',
    position_id smallint comment '站外广告;从1开始代表的是该广告所处的广告位,同表ad_postition中的字段position_id的值',
    media_type tinyint comment '广告类型,0图片;1flash;2代码3文字',
    ad_name varchar(60) comment '该条广告记录的广告名称',
    ad_link varchar(255) comment '广告链接地址',
    ad_code text comment '广告链接的表现,文字广告就是文字或图片和flash就是它们的地址',
    start_time int(11) comment '广告开始时间',
    end_time int(11) comment '广告结速时间',
    link_man varchar(60) comment '广告联系人',
    link_email varchar(60) comment '广告联系人的邮箱',
    link_phone varchar(60)	comment '广告联系人的电话',
    click_count mediumint comment '该广告点击数',
    enabled tinyint comment '该广告是否关闭;1开启; 0关闭; 关闭后广告将不再有效'
);

-- 3、admin_action 管理权限分配
create table admin_action (
    action_id tinyint comment '自增ID号',
    parent_id tinyint comment '该id 项的父id,对应本表的action_id 字段',
    action_code varchar(20) comment '代表权限的英文字符串,对应汉文在语言文件中,如果该字段有某个字符串,就表示有该权限',
    relevance varchar(20)
);
-- 4、admin_log  管理日志
create table admin_log(
    log_id int comment '自增id号',
    log_time int comment '写日志时间',
    user_id tinyint comment '该日志所记录的操作者id,同admin_user的user_id',
    log_info varchar(255) comment '管理操作内容',
    ip_address varchar(15) comment '登录者登录IP'
);
-- 5、admin_message  管理留言 
create table admin_message(
    message_id smallint comment '自增id号',
    sender_id tinyint comment '发送该留言的管理员id,同admin_user的user_id',
    receiver_id tinyint comment '接收消息管理员id,同admin_user的user_id,如果是给多个管理员发送,则同一个消息给每个管理员id发送一条',
    sent_time int comment '留言发送时间',
    read_time int comment '留言阅读时间',	 
    readed tinyint comment '留言是否阅读1已阅读;0未阅读',
    deleted tinyint comment '留言是否已经被删除 1已删除;0未删除',
    title varchar(150) comment '留言的主题',
    message text comment '留言的内容'
);
-- 6.admin_user  管理员管理 
create table admin_user(
    user_id smallint comment '自增id号,管理员代码',
    user_name varchar(60) comment '管理员登录名',
    email varchar(60) comment '管理员邮箱',
    password varchar(32) comment '管理员登录密码',
    add_time int comment '管理员添加时间',	 
    last_login int comment '管理员最后一次登录时间',
    last_ip varchar(15) comment '管理员最后一次登录IP',
    action_list text comment '管理员管理权限列表',
    nav_list text comment '管理员导航栏配置项',
    lang_type varchar(50),
    agency_id smallint comment '该管理员负责的办事处理的id,同agency的agency_id字段.如果管理员没有负责办事处,则此处为0',
    suppliers_id smallint,
    todolist longtext comment '记事本记录的数据',
    role_id smallint
);