
create table duplicate_key (
    s_id varchar(5) primary key,
    s_name varchar(5) not null
);
insert into duplicate_key values
('s001','王1'),
('s002','王2'),
('s003','王3'),
('s004','王4'),
('s005','王1'),
('s006','王2'),
('s007','王3'),
('s008','王4');

insert into duplicate_key values('s001','大王');
insert into duplicate_key values('s001','大王') on duplicate key update s_name='大王';
replace into duplicate_key values('s002','熊大');

create table old_worm(
  pinyin varchar(2)
);

insert into old_worm values('a'),('b'),('c'),('d'),('e'),('f');
create table new_worm like old_worm;
insert into new_worm select * from old_worm;

update duplicate_key set s_name = '小王' where s_id ='s002';

update duplicate_key set s_name = '旺旺' where s_name = '王1' limit 2;

delete from duplicate_key where s007 = '旺旺';

create table query_data1(
  pinyin varchar(2)
);

insert into query_data1 values('a'),
('b'),
('c'),
('a'),
('b'),
('c');

select distinct * from query_data1;

select distinct pinyin as name1, pinyin as name2 from query_data1;

create table stu_info(
    id int primary key auto_increment,
    name varchar(5) not null,
    age tinyint not null default 18,
    sex enum('男','女','保密') default '保密'
);

insert into stu_info(name,age,sex) values 
('小白龙',default, '男'),
('小黑龙',20, '男'),
('小紫龙',21, '女'),
('小红龙',17, '男'),
('小绿龙',19, '女'),
('小粉龙',16, '男'),
('小金龙',24, '女'),
('小龙龙',30, '男'),
('小云龙',43 ,'女');

create table stu_score(
     id int primary key auto_increment,
     name  varchar(6) not null,
     score tinyint not null
);

insert into stu_score(name,score) values
('小白龙',100),
('小黑龙',60),
('小紫龙',70),
('小红龙',90),
('小绿龙',99),
('小粉龙',80),
('小金龙',40),
('小龙龙',30),
('小云龙',20);
select name,sex as gender from stu_info;
select name as gende,sex as gender from stu_info;
select * from stu_info,stu_score;
select * from (select name,age from stu_info) as info;

select name from (select name,age from stu_info) as info;

select * from stu_info group by sex;  
select group_concat(name),sex from stu_info group by sex;
select group_concat(sex) from stu_info group by sex;
select count(*) from stu_score;
select count(*), sum(score) from stu_score;
select sum(score) from stu_score;
select max(score) from stu_score;
select min(score) from stu_score; 
select avg(score) from stu_score;

select group_concat(name),age,sex from stu_info group by sex asc,age asc;
select group_concat(name),age,sex from stu_info group by sex desc,age desc;

-- select count(*) from stu_info group by age desc with rollup;
select count(*) from stu_info group by age desc with rollup;
select id,count(*) from stu_info group by age desc,id desc with rollup;
select id,count(*) from stu_info group by age asc,id asc with rollup;


select * from stu_info group by sex order by age asc;
select * from stu_info order by age asc;
select name,id,age from stu_info order by age asc;


select * from stu_info limit 5; 

select * from stu_info limit 0,4;
select * from stu_info limit 4,4;
select * from stu_info limit 8,4;