-- having的本质和where一样，是用来进行数据条件筛选的

-- 1.having是再group by子句之后: 可以针对分组数据进行统计筛选，但是where不行 
-- 查询班级人数大于等于4个以上的班级
-- where 不能使用聚合函数:聚合函数是用group by分组的时候，where已经运行完毕

-- having在group by分组之后， 可以使用聚合函数或者字段别名(where 是从表中取出数据，别名是在数据进入到内存之后才有的)

-- 强调:having是在group by之后，group by是在where 之后: where 的时候表示将数据从磁盘拿到内存，where之后的所有操作都是内存操作

-- 创建数据库
create database mysql_4_16;

-- 创建数据表
create table having_data(
    id int primary key auto_increment,
    name varchar(6) not null,
    sex enum('男','女','保密') not null,
    age tinyint not null default 18
);

-- 插入数据
insert into having_data(name,sex,age) values
('妲己','女',18),
('黄忠','男',50),
('扁鹊','男',21), 
('张良','男',24),
('不知火舞','女',23),
('张飞','男',30),
('刘备','男',34),
('大乔','女',16),
('貂蝉','女',18),
('吕布','男',34);
-- 先找到having_data 的所有数据 按照 性别进行分组 显示有多少个数  
select count(*) from having_data group by sex;

-- 想让 数量大于=6的那一组 显示名字
select group_concat(name),count(*) from having_data group by sex; 

-- where 不能使用聚合函数
-- 使用having 能使用 聚合函数 要放在group by 后面

select group_concat(name),count(*) from having_data group by sex having count(*)>=6;




-- 基本算术运算：通常不在条件中使用，而是用于结果运算（select 字段中）
-- +、-、*、/、%
-- 创建表
create table ysf_ss(
    int_1 int,
    int_2 int,
    int_3 int,
    int_4 int,
    int_5 int
);
-- 插入数据
insert into ysf_ss values (1,2,3,4,5);

-- 使用算术运算符 进行结果运算
select int_1+int_5, int_2-int_1, int_3*int_2, int_4/int_2, int_5%int_2 from ysf_ss;

-- 1.mysql中除法运算结果用浮点数表示
-- 2.mysql中 如果除数为0 用null表示
-- 3.null进行任何算术运算结果都为null

-- 比较运算符
-- >、>=、<、<=、=、<>
-- 通常是用来在条件中进行限定结果
-- =：在mysql中，没有对应的 ==比较符号，就是使用=来进行相等判断
-- <=>：相等比较

-- 找出 having_data中年龄大于20的人
select * from having_data where age>=20;

-- -- 找出 having_data中 性别为女的人
select * from having_data where sex<=> '女';

-- -- 找出 having_data中 性别不是保密的人
select * from having_data where sex<> '保密';

-- 结果
-- +----+--------------+-----+-----+
-- | id | name         | sex | age |
-- +----+--------------+-----+-----+
-- |  1 | 妲己         | 女  |  18 |
-- |  2 | 黄忠         | 男  |  50 |
-- |  3 | 扁鹊         | 男  |  21 |
-- |  4 | 张良         | 男  |  24 |
-- |  5 | 不知火舞     | 女  |  23 |
-- |  6 | 张飞         | 男  |  30 |
-- |  7 | 刘备         | 男  |  34 |
-- |  8 | 大乔         | 女  |  16 |
-- |  9 | 貂蝉         | 女  |  18 |
-- | 10 | 吕布         | 男  |  34 |
-- +----+--------------+-----+-----+

-- mysql中 没有规定 select 后面必须跟 表名
select '1'<=>1, 0.002 <=>0,0.002<>0;

-- 结果
-- +---------+------------+----------+
-- | '1'<=>1 | 0.002 <=>0 | 0.002<>0 |
-- +---------+------------+----------+
-- |       1 |          0 |        1 |
-- +---------+------------+----------+


-- 逻辑运算符
-- and、or、not 逻辑运算符

-- and：逻辑与 &&
-- 找出 having_data表中 年龄在18到30岁的人
-- mysql中 条件的第一个值 一定是小于后面的值的
-- 第一个条件不能大于第二个条件(版本5.1)

-- 查询having-data 中 的age是否大于18 小于 30 
select * from having_data where age>=18 and age<= 30;

-- 结果
-- +----+--------------+-----+-----+
-- | id | name         | sex | age |
-- +----+--------------+-----+-----+
-- |  1 | 妲己         | 女  |  18 |
-- |  3 | 扁鹊         | 男  |  21 |
-- |  4 | 张良         | 男  |  24 |
-- |  5 | 不知火舞     | 女  |  23 |
-- |  6 | 张飞         | 男  |  30 |
-- |  9 | 貂蝉         | 女  |  18 |
-- +----+--------------+-----+-----+

select * from having_data where age>=30 and age<= 18;
-- (应该有版本之差 如果语句可以执行 那就用 但是还是建议 小的条件在前 大的条件在后);

-- or: 逻辑或 ||
-- 找出 女性 或 年龄小于18的人
select * from having_data where age<=18 or sex='女';

-- 结果
-- +----+--------------+-----+-----+
-- | id | name         | sex | age |
-- +----+--------------+-----+-----+
-- |  1 | 妲己         | 女  |  18 |
-- |  5 | 不知火舞     | 女  |  23 |
-- |  8 | 大乔         | 女  |  16 |
-- |  9 | 貂蝉         | 女  |  18 |
-- +----+--------------+-----+-----+

-- not: 逻辑非 !
select * from having_data where not sex='女';

-- 结果
-- +----+--------+-----+-----+
-- | id | name   | sex | age |
-- +----+--------+-----+-----+
-- |  2 | 黄忠   | 男  |  50 |
-- |  3 | 扁鹊   | 男  |  21 |
-- |  4 | 张良   | 男  |  24 |
-- |  6 | 张飞   | 男  |  30 |
-- |  7 | 刘备   | 男  |  34 |
-- | 10 | 吕布   | 男  |  34 |
-- +----+--------+-----+-----+



-- in运算符
-- In：在什么里面，是用来替代=，当结果不是一个值，而是一个结果集的时候
-- 基本语法： in (结果1,结果2,结果3…)，只要当前条件在结果集中出现过，那么就成立

-- 显示having_data id为1  3  5 6  8的用户的信息
select * from having_data where id in(1,3,5,6);

-- 结果
-- +----+--------------+-----+-----+
-- | id | name         | sex | age |
-- +----+--------------+-----+-----+
-- |  1 | 妲己         | 女  |  18 |
-- |  3 | 扁鹊         | 男  |  21 |
-- |  5 | 不知火舞     | 女  |  23 |
-- |  6 | 张飞         | 男  |  30 |
-- +----+--------------+-----+-----+

-- 删除having_data name为小乔  貂蝉 不知火舞这几个人的信息
delete from having_data where name in ('小乔','貂蝉','不知火舞');

-- 结果
-- +----+--------+-----+-----+
-- | id | name   | sex | age |
-- +----+--------+-----+-----+
-- |  1 | 妲己   | 女  |  18 |
-- |  2 | 黄忠   | 男  |  50 |
-- |  3 | 扁鹊   | 男  |  21 |
-- |  4 | 张良   | 男  |  24 |
-- |  6 | 张飞   | 男  |  30 |
-- |  7 | 刘备   | 男  |  34 |
-- |  8 | 大乔   | 女  |  16 |
-- | 10 | 吕布   | 男  |  34 |
-- +----+--------+-----+-----+



-- is运算符
-- Is是专门用来判断字段是否为NULL的运算符
-- 基本语法：is null / is not null

-- 1. 找出  having_data中 age不为null的数据
select * from having_data where age is not null;

-- 结果
-- +----+--------+-----+-----+
-- | id | name   | sex | age |
-- +----+--------+-----+-----+
-- |  1 | 妲己   | 女  |  18 |
-- |  2 | 黄忠   | 男  |  50 |
-- |  3 | 扁鹊   | 男  |  21 |
-- |  4 | 张良   | 男  |  24 |
-- |  6 | 张飞   | 男  |  30 |
-- |  7 | 刘备   | 男  |  34 |
-- |  8 | 大乔   | 女  |  16 |
-- | 10 | 吕布   | 男  |  34 |
-- +----+--------+-----+-----+

-- 创建表
create table is_data(
  int_1 int ,
  int_2 int
);
-- 添加数据
insert into is_data values
(null, 10),
(100,200);

-- 找到 is_data中int_1为null的数据
select * from is_data where int_1 is null;

-- 结果
-- +-------+-------+
-- | int_1 | int_2 |
-- +-------+-------+
-- |  NULL |    10 |
-- +-------+-------+


-- like运算符
-- Like运算符：是用来进行模糊匹配（匹配字符串）
-- 基本语法：like ‘匹配模式’;

-- 匹配模式中，有两种占位符：
-- _：匹配对应的单个字符
-- %：匹配多个字符

select * from  having_data where name like '张%';

-- 结果
-- +----+--------+-----+-----+
-- | id | name   | sex | age |
-- +----+--------+-----+-----+
-- |  4 | 张良   | 男  |  24 |
-- |  6 | 张飞   | 男  |  30 |
-- +----+--------+-----+-----+

select * from having_data where name like '妲_';

-- 结果
-- +----+--------+-----+-----+
-- | id | name   | sex | age |
-- +----+--------+-----+-----+
-- |  1 | 妲己   | 女  |  18 |
-- +----+--------+-----+-----+








-- 联合查询是可合并多个相似的选择查询的结果集，等同于将一个表追加到另一个表，从而实现将两个表的查询组合到一起
-- 使用谓词 union或union all;

-- 联合查询: 将多个查询的结果合并到一起(纵向合并) 字段数不变，多个查询的记录合并；

-- 应用场景
-- 1.将同一张表中的不同的结果(需要对应多条查询语句实现)，合并到一起展示数据 男是身高升序排列 女生升高降序排列

-- 2.最常见: 在数据量大的情况下，会对表进行分表操作，需要对每张表进行部分数据统计，使用联合查询来将数据存放到一起显示、


-- 基本语法:
-- select语句1
-- union [union]选项
-- select语句2 

-- 创建一张表
create table union_data(
  stu_id varchar(6) primary key,
  stu_name varchar(5) not null,
  stu_age tinyint not null default 18,
  stu_height varchar(4) not null,
  stu_set enum('男','女','保密') default '保密'
);

-- 添加数据
insert into union_data values
('stu001', '小红',default, '156','女'),
('stu002', '小绿',21, '156','男'),
('stu003', '小黑',20, '158','女'),
('stu004', '小白',17, '168','男'),
('stu005', '小婷',16, '175','女'),
('stu006', '小雅',25, '170','男'),
('stu007', '小石头',26, '180','女'),
('stu008', '小蒋',29, '170','男'),
('stu009', '小贾',20, '174','女');


-- 显示性别为男性的数据
select * from union_data where stu_set = '男';

-- 结果
-- +--------+----------+---------+------------+---------+
-- | stu_id | stu_name | stu_age | stu_height | stu_set |
-- +--------+----------+---------+------------+---------+
-- | stu002 | 小绿     |      21 | 156        | 男      |
-- | stu004 | 小白     |      17 | 168        | 男      |
-- | stu006 | 小雅     |      25 | 170        | 男      |
-- | stu008 | 小蒋     |      29 | 170        | 男      |
-- +--------+----------+---------+------------+---------+

-- 显示性别为女性的数据
select * from union_data where stu_set = '女';

-- 结果
-- +--------+-----------+---------+------------+---------+
-- | stu_id | stu_name  | stu_age | stu_height | stu_set |
-- +--------+-----------+---------+------------+---------+
-- | stu001 | 小红      |      18 | 156        | 女      |
-- | stu003 | 小黑      |      20 | 158        | 女      |
-- | stu005 | 小婷      |      16 | 175        | 女      |
-- | stu007 | 小石头    |      26 | 180        | 女      |
-- | stu009 | 小贾      |      20 | 174        | 女      |
-- +--------+-----------+---------+------------+---------+

-- 使用联合查询
-- 显示性别为男性的数据
select * from union_data where stu_set = '男'
union
-- 显示性别为女性的数据
select * from union_data where stu_set = '女';

-- 结果
-- +--------+-----------+---------+------------+---------+
-- | stu_id | stu_name  | stu_age | stu_height | stu_set |
-- +--------+-----------+---------+------------+---------+
-- | stu002 | 小绿      |      21 | 156        | 男      |
-- | stu004 | 小白      |      17 | 168        | 男      |
-- | stu006 | 小雅      |      25 | 170        | 男      |
-- | stu008 | 小蒋      |      29 | 170        | 男      |
-- | stu001 | 小红      |      18 | 156        | 女      |
-- | stu003 | 小黑      |      20 | 158        | 女      |
-- | stu005 | 小婷      |      16 | 175        | 女      |
-- | stu007 | 小石头    |      26 | 180        | 女      |
-- | stu009 | 小贾      |      20 | 174        | 女      |
-- +--------+-----------+---------+------------+---------+

-- 使用联合查询 显示男性数据升序 显示女性数据降序 要用排序 必须使用limit
(select * from union_data where stu_set = '男' order by stu_height asc limit 100)
union 
(select * from union_data where stu_set = '女' order by stu_height desc limit 100);

-- 结果
-- +--------+-----------+---------+------------+---------+
-- | stu_id | stu_name  | stu_age | stu_height | stu_set |
-- +--------+-----------+---------+------------+---------+
-- | stu002 | 小绿      |      21 | 156        | 男      |
-- | stu004 | 小白      |      17 | 168        | 男      |
-- | stu006 | 小雅      |      25 | 170        | 男      |
-- | stu008 | 小蒋      |      29 | 170        | 男      |
-- | stu007 | 小石头    |      26 | 180        | 女      |
-- | stu005 | 小婷      |      16 | 175        | 女      |
-- | stu009 | 小贾      |      20 | 174        | 女      |
-- | stu003 | 小黑      |      20 | 158        | 女      |
-- | stu001 | 小红      |      18 | 156        | 女      |
-- +--------+-----------+---------+------------+---------+