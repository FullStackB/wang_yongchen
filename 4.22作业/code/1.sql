-- 创建数据库
create database day07_b;
-- 选择数据库
use day07_b;
-- 创建数据表-> 存储班级信息的
create table stu_class(
  class_id int primary key auto_increment,
  class_name varchar(20) not null
);

-- 创建学生信息表 -> 存储学生信息的
create table stu_info(
  info_id int primary key auto_increment,
  info_name varchar(10) not null,
  info_age tinyint unsigned,
  class_id int
);

set names utf8;

-- 向 班级信息表 插入数据
insert into stu_class values
(null, 'Java应用方向'),
(null, '全栈应用方向'),
(null, 'python应用方向'),
(null, '大数据应用方向');
-- 向 学生信息表  插入数据
insert into stu_info values
(null, '小鱼儿', 30, 1),
(null, '段誉', 18, 2),
(null, '木婉清', 16, 3),
(null, '虚竹', 23, 4),
(null, '乔峰', 32, 1),
(null, '黄眉僧', 50, 2),
(null, '扫地僧', 60, 3),
(null, '钟万仇', 44, 4),
(null, '云中鹤', 30, 1),
(null, '萧远山', 56, 2),
(null, '萧炎', 24, 3),
(null, '西门庆', 26, 4);
-- 标量子查询：子查询得到结果是一个数据（一行一列）

-- 基本语法：select * from 数据源 where 条件判断 =/<> (select 字段名 from 数据源 where 条件判断); 

-- 需求: 我知道某一个学生的姓名 想要求出该学生的班级名称

-- 1.值到学生的姓名我可以求出学生的班级id
select class_id from stu_info where info_name ="西门庆";
-- 2.根据班级id 求出 班级的名称

select class_name from stu_class where class_id = (select class_id from stu_info where info_name="西门庆");


-- 列子查询
-- 需求: 获取已经有学生在班的所有班级名字

-- 思路:
-- 1.找出学生表中所有的班级ID
-- 2.找出班级对应的名字
select * from stu_class where class_id in (select class_id from stu_info);

-- 行子查询
-- 字段元素: 一个字段对应的值
-- 行元素: 就是多个字段， 多个字段合起来作为一个元素参与运算，把这种情况称之为行元素
-- 语法: 主查询 where 条件 [构造一个行元素] = (行子查询);

-- 需求: 找到用户表中年龄最大的

-- 总结: 常见的三个子查询
-- 标量子查询,列子查询和行子查询: 都属于where子查询

select max(info_age) from stu_info;

select * from stu_info where info_age = (select max(info_age) from stu_info);

-- 表子查询
-- 基本语法:
-- select 字段表 from (表子查询) as 别名 [where] [group by] [having] [order by] [limit];

-- 需求: 获取每个班年龄最大的学生

-- 思路: 
-- 1.将每个班最高的学生排在最前面: order by;
-- 2.再针对结果今天 group by 保留每组第一个
select * from stu_info order by info_age desc;
-- 表单查询有个问题 表子元素加limit 限制就好了 是mysql版本的问题 5.7版本以后的子查询要想生效必须要加限制
select * from (select * from stu_info order by info_age desc limit 1000) as temp group by class_id;

-- Exists子查询
-- 语法:
-- where exists(查询语句);   // exits就是根据查询得到的结果进行判断: 如果结果存在,那么返回1 否则返回0;

-- 需求: 求出,有学生在的所有班级

-- 查出所有学生的班级id
select * from stu_class where exists (select * from stu_info where stu_class.class_id = stu_info.class_id);





-- 第二
-- 标量子查询
select class_name from stu_class where class_id = (select class_id from stu_info where info_name="西门庆");
-- 列子查询
select * from stu_class where class_id in (select class_id from stu_info);
-- 行子查询
select * from stu_info where info_age = (select max(info_age) from stu_info);
-- 表子查询
select * from (select * from stu_info order by info_age desc limit 1000) as temp group by class_id;
-- Exists子查询
select * from stu_class where exists (select * from stu_info where stu_class.class_id = stu_info.class_id);