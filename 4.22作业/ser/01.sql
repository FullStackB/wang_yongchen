-- 创建数据库
create database day03;
-- 进入数据库
use day03;
-- 创建表
create table duplicate_key (
    s_id varchar(5) primary key,
    s_name varchar(5) not null
);
-- 插入数据
insert into duplicate_key values
('s001','王1'),
('s002','王2'),
('s003','王3'),
('s004','王4'),
('s005','王1'),
('s006','王2'),
('s007','王3'),
('s008','王4');

-- 查看是否主键冲突   ERROR 1062 (23000): Duplicate entry 's001' for key 'PRIMARY'
insert into duplicate_key values('s001','大王');

-- 解决主键冲突 
-- Insert into 表名 [(字段列表)] values(值列表) on duplicate key update 字段 = 新值;  相当于数据更新
insert into duplicate_key values('s001','大王') on duplicate key update s_name='大王';
-- Replace into 表名 [(字段列表)] values(值列表); (相当于先删除后添加)
replace into duplicate_key values('s002','熊大');

-- 1.创建一个旧数据表()
create table old_worm(
  pinyin varchar(2)
);

-- 2.在旧数据表中插入数据
insert into old_worm values('a'),('b'),('c'),('d'),('e'),('f');

-- 3.创建一个新的数据表 去复制 旧数据表的结构
create table new_worm like old_worm;
-- 4.用到了蠕虫复制 把旧数据表的数据复制到新的数据表中
-- 基本语法: Insert into 表名 [(字段列表)] select */字段列表 from 表;
insert into new_worm select * from old_worm;




-- 更新数据
-- 基本语法:
-- update table 表名 set  字段名=字段值 where 条件;
update duplicate_key set s_name = '小王' where s_id ='s002';


-- 我只想改变两个拼音为g的值 为a


-- 如果没有条件，是全表更新数据。但是可以使用limit 来限制更新的数量；
-- Update 表名 set 字段名 = 新值 [where 判断条件] limit 数量;

update duplicate_key set s_name = '旺旺' where s_name = '王1' limit 2;


-- 删除数据

-- Delete删除数据的时候无法重置auto_increment
-- 删除的基本语法: delete from 表名 where 条件;
delete from duplicate_key where s007 = '旺旺';


-- 完整的查询指令
-- Select select选项 字段列表 from 数据源 where条件 group by分组 having条件 order by排序 limit限制;

-- 1.创建一个含有重复数据的表
-- select 选项
-- 1.1 all 默认的
-- 1.2 Distinct

-- 创建表
create table qurey_data (
    id int primary key auto_increment,
    name varchar(5) not null
);
-- 添加数据
insert into qurey_data(name) values 
('张三'),
('蔡明'),
('李四'),
('王五'),
('赵六'),
('蔡徐坤'),
('姚明'),
('王五'),
('张三');

-- 查询所有数据(无论是否重复)
select * from query_data;
select all * from query_data;

-- 查询非重复数据(原因就是 要所有字段都必须一致才认为是重复数据)
select distinct * from query_data;

create table query_data1(
  pinyin varchar(2)
);

insert into query_data1 values('a'),
('b'),
('c'),
('a'),
('b'),
('c');

select distinct * from query_data1;

-- 基本语法：字段名 [as] 别名

select distinct pinyin as name1, pinyin as name2 from query_data1;

-- from子句
-- 1.创建 学生表
create table stu_info(
    id int primary key auto_increment,
    name varchar(5) not null,
    age tinyint not null default 18,
    sex enum('男','女','保密') default '保密'
);

-- 添加数据
insert into stu_info(name,age,sex) values 
('小白龙',default, '男'),
('小黑龙',20, '男'),
('小紫龙',21, '女'),
('小红龙',17, '男'),
('小绿龙',19, '女'),
('小粉龙',16, '男'),
('小金龙',24, '女'),
('小龙龙',30, '男'),
('小云龙',43 ,'女');

-- 2.创建 学生成绩表
create table stu_score(
     id int primary key auto_increment,
     name  varchar(6) not null,
     score tinyint not null
);

insert into stu_score(name,score) values
('小白龙',100),
('小黑龙',60),
('小紫龙',70),
('小红龙',90),
('小绿龙',99),
('小粉龙',80),
('小金龙',40),
('小龙龙',30),
('小云龙',20);

-- 起了别名的from
select name,sex as gender from stu_info;
select name as gende,sex as gender from stu_info;

-- 多表查询
select * from stu_info,stu_score;

-- 动态数据
-- 基本语法：from (select 字段列表 from 表) as 别名;
select * from (select name,age from stu_info) as info;

-- 含义: 从stu_info中选区了name,age两个字段 作为一个新的表名info全部显示出来
-- 相当于select * from info了

select name from (select name,age from stu_info) as info;

-- group by子句
-- 完整的查询指令
-- Select select选项 字段列表 from 数据源 where条件 group by分组 having条件 order by排序 limit限制;

-- 把stu_info中的龙 按照性别分 
-- 基本语法: group by 标准;

--下面的这个只能查看第一条信息
select * from stu_info group by sex;  
-- 以上语句的作用就是显示第一条符合标准的结果
-- 以下的语句的含义是: 从stu_info中 通过 性别(男 女)把所有数据都分组
-- 把分组后的结果(名字) 使用group_concat()连接成一条数据
-- 姓名 name sex把性别也同时也显示出来
select group_concat(name),sex from stu_info group by sex;
-- 表示了 多少个男 和女
select group_concat(sex) from stu_info group by sex;

-- 聚合函数
-- count 数量统计
-- select count(*) from stu_score;
select count(*) from stu_score;
-- sum() 求和
-- select count(*), sum(score) from stu_score;
select count(*), sum(score) from stu_score;
select sum(score) from stu_score;
-- max() 最大值
-- select max(score) from stu_score;
select max(score) from stu_score;
-- min() 最小值
-- select min(score) from stu_score;
select min(score) from stu_score; 
-- avg() 平均数
-- select avg(score) from stu_score;
select avg(score) from stu_score;

-- 多分组
-- 分组的依据有多个
-- 升序:自小而大 asc
-- 降序: 自大而小 desc
-- 含义: 先按照性别 分为男(1)女(2) 然后再按照年龄进行分组排序(升序)

-- 升序
select group_concat(name),age,sex from stu_info group by sex asc,age asc;
-- 降序
select group_concat(name),age,sex from stu_info group by sex desc,age desc;

-- 回溯统计
-- 基本语法: group by with rollup;
-- select count(*) from stu_info group by age desc with rollup;
select count(*) from stu_info group by age desc with rollup;

-- 多分组回溯统计
select id,count(*) from stu_info group by age desc,id desc with rollup;
select id,count(*) from stu_info group by age asc,id asc with rollup;

-- 09-order by子句
-- order by 排序的
-- 基本语法: order by asc 升序
-- 基本语法: order by desc 降序
-- group by 和 order by 连在一起写
select * from stu_info group by sex order by age asc;
-- 排序
select * from stu_info order by age asc;
-- 排序 那一个在前 那一个在后
select name,id,age from stu_info order by age asc;


-- 限制数量

-- 限制 找数据  只找 5条
select * from stu_info limit 5; 

-- 限制数量的排序
-- 分页: 每次取2条
-- 基本语法: limit 从第几个开始取,取几个
select * from stu_info limit 0,4;
select * from stu_info limit 4,4;
select * from stu_info limit 8,4;