-- 创建数据库 
create database mysql_4_15;
-- 选择
use mysql_4_15;
-- 创建学生表
create table student(
    no varchar(20) primary key comment'学号（主键）',
    name  varchar(20) not null comment '学生姓名',
    sex  varchar(20) not null comment '学生性别',
    birthday  datetime comment '学生出生年月',
    class  varchar(20) comment '学生所在班级'
);
-- 创建课程表
create table course (
    no varchar(20) primary key comment'课程号（主键）',
    name varchar(20) not null comment '课程名称',
    t_no varchar(20) not null comment '教工编号（外键）',
    foreign key (t_no) references teacher(no)
);
-- 创建成绩表
create table score (
    s_no varchar(20) not null comment '学号(外键)',
    c_no varchar(20) not null comment '课程号（外键）',
    degree Decimal(4,1) comment '成绩',
    foreign key (s_no) references student(no),
    foreign key (c_no) references course(no)
);
-- 创建教师表
create table teacher (
    no varchar(20) primary key comment'教工编号（主键）',
    name varchar(20) not null comment'教工姓名',
    sex varchar(20) not null comment'教工性别',
    birthday datetime comment'教工出生年月',
    prof varchar(20) comment'职称',
    depart varchar(20) not null comment'教工所在部门'
);

-- 在学生表上添加数据
insert into student values
('101','赵军','男','1987-03-20 00:00:00','95033'),
('103','毛军','男','1984-09-03 00:00:00','95031'),
('105','李明','男','1982-11-02 00:00:00','95031'),
('107','范丽','女','1987-01-23 00:00:00','95033'),
('108','王华','男','1981-09-01 00:00:00','95033'),
('109','张芳','女 ','1983-01-10 00:00:00','95031');

-- 在教师表上添加数据
insert into teacher values
('804','王诚','男','1957-12-02 00:00:00','副教授','计算机系'),
('825','张萍','女','1971-05-05 00:00:00','助教','计算机系'),
('831','毛冰','女','1975-08-14 00:00:00','助教','电子工程系'),
('856','李旭','男','1966-03-12 00:00:00','讲师','电子工程系');

-- 在成绩表上添加数据
insert into score values
('103','3-105','92'),
('103','3-245','86'),
('103','6-166','85'),
('105','3-105','88'),
('105','3-245','75'),
('105','6-166','79'),
('109','3-105','76'),
('109','3-245','68'),
('109','6-166','81');

-- 在课程表上添加数据
insert into course values
('3-105','计算机导论','825'),
('3-245','操作系统','804'),
('6-166','数学电路','856'),
('9-888','高等数学','831');

-- 查询
select * from student;
select * from teacher;
select * from course;
select * from score;

-- 查询student表中的所有记录的name、sex和class列信息
select name,sex,class from student;

-- 查询教师所有的单位(不重复)的depart列信息
select distinct depart from teacher;

-- 查询score表中成绩在70到90之间的所有记录
-- select s_no,c_no,degree from score where degree>=70 && degree<=90;
select s_no,c_no,degree from score where degree>=70 and degree<=90;

-- 查询score表中成绩为68，75或88的记录
select  s_no,c_no,degree from score where degree in(68,75,88);

-- 查询student表中“95031”班或性别为“女”的同学记录
-- select no,name,sex,birthday,class from student where class='95031' || sex = '女';
select no,name,sex,birthday,class from student where class='95031' or sex = '女';

-- 以c_no升序、degree降序查询score表的所有记录
-- select group_concat(s_no),c_no,degree from score group by c_no asc,degree desc;
-- 排序 之后名字拼接
-- select group_concat(s_no),c_no,degree from score order by c_no asc,degree desc;
-- select s_no,c_no,degree from score order by c_no asc,degree desc;
select * from score order by c_no asc,degree desc;

-- 查询“95031”班的学生人数
select count(*) from student where class = '95031';

-- 查询score表中的最高分的学生学号和课程号     错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误
-- select s_no,c_no from score where degree in(select max(degree) from score);  
select s_no,c_no from score having max(degree);

-- 查询每门课的平均成绩                       错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误 错误
select concat(c_no),avg(degree) from score group by c_no;

-- 查询分数大于60，小于80的s_no列
-- select s_no from score where degree>=60 && degree<=80;
select s_no from score where degree>=60 and degree<=80;

-- 查询student表中姓“王”的同学记录
select no,name,sex,birthday from student where name like '王%';

-- 查询student表中最大和最小的birthday日期值
select max(birthday),min(birthday) from student;

-- 以年龄从小到大的顺序查询student表中的全部记录 年龄需要降序
-- select * from student group by  birthday desc; 
select * from student order by birthday desc;

-- 查询最高分同学的成绩信息
select s_no,c_no,max(degree) from score;

-- 把学号为‘108’，名字为‘王华’学生的班级改为‘95034’
update student set class='95034' where no = '108' && name = '王华';

-- 查看
select * from student;
select * from score where degree <=70 and degree>=30;








-- 交叉连接: 将两张表的数据与另一张表彼此交叉

-- 原理:
-- 从第一张表依次取出每一条记录
-- 取出每一条记录之后，与另外一张表的全部记录挨个匹配
-- 没有任何匹配条件，所有的结果都会进行保留
-- 记录数=第一张表记录数*第二张表记录数;字段数=第一张表字段数*第二张表字段数(笛卡儿积)

-- 基本语法: 表1 cross join 表2； 
select * from having_data cross join union_data;


-- 交叉连接产生的结果是笛卡儿积，没有实际应用
-- 本质: from 表1，表2；






-- 内连接 inner join ,从一张表中取出所有的记录去另外一张表中匹配，利用匹配条件进行匹配，成功则保留，失败则放弃

-- 原理:
-- 1.从第一张表中取出一条记录，然后去另外一张表中进行匹配
-- 2.利用匹配条件进行匹配
-- 3.匹配到: 则保留，继续向下匹配
-- 4.匹配失败: 向下继续,如果全表匹配失败,结束

-- 语法: 表1 inner join 表2 on 匹配条件

-- 1.如果内连接没有条件(允许) 那么其实是交叉连接
select * from having_data inner join union_data;

-- 2.使用匹配条件进行匹配
select * from having_data inner join union_data on having_data.age = union_data.stu_age;

-- 内连接通常是在对数据有精确要求的地方使用: 必须保证两种表中都能进行数据匹配







-- 外联接查询

-- 基本语法：
-- 左连接：主表 left join 从表 on 连接条件;
-- 右连接：从表 right join 主表 on连接条件;
-- 外连接分为两种：左外连接（left join），右外连接（right join）
-- 左连接：左表是主表
-- 右连接：右表是主表

-- 特点
-- 1、	外连接中主表数据记录一定会保存：连接之后不会出现记录数少于主表（内连接可能）
-- 2、	左连接和右连接其实可以互相转换，但是数据对应的位置（表顺序）会改变

-- select * from 