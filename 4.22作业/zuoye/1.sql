-- 员工部分表
-- 1.部门表Dept
create table dept (
    deptno float(2) primary key comment '部门号',
    dname varchar(14) comment '部门名称',
    loc varchar(13) comment '部门地址'
);
-- 添加数据
insert into dept values
(10,'accounting','new york'),
(20,'research','dallas'),
(30,'sales','chicago'),
(40,'operations','boston');
-- 2.员工表EMP
create table emp (
    empno float(4) primary key comment '员工编号',
    ename varchar(10) comment '员工姓名',
    job varchar(9) comment '员工职位',
    mgr float(4) comment '员工上级工号',
    hiredate date comment '生日',
    sal float(7,2) comment '薪水',
    comm float(7,2) comment '年终奖',
    deptno float(2) references dept
);
-- 添加数据
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7369, 'SMITH', 'CLERK', 7902, '1980-12-17', 800, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7499, 'ALLEN', 'SALESMAN', 7698, '1981-02-20', 1600, 300, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7521, 'WARD', 'SALESMAN', 7698, '1981-02-22', 1250, 500, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7566, 'JONES', 'MANAGER', 7839, '1981-04-02', 2975, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7654, 'MARTIN', 'SALESMAN', 7698, '1981-09-28', 1250, 1400, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7698, 'BLAKE', 'MANAGER', 7839, '1981-05-01', 2850, null, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7782, 'CLARK', 'MANAGER', 7839, '1981-06-09', 2450, null, 10);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7788, 'SCOTT', 'ANALYST', 7566, '1987-04-19', 3000, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7839, 'KING', 'PRESIDENT', null, '1981-11-17', 5000, null, 10);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7844, 'TURNER', 'SALESMAN', 7698, '1981-09-08', 1500, 0, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7876, 'ADAMS', 'CLERK', 7788, '1987-05-23', 1100, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7900, 'JAMES', 'CLERK', 7698, '1981-12-03', 950, null, 30);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7902, 'FORD', 'ANALYST', 7566,'1981-12-02', 3000, null, 20);
insert into EMP (EMPNO, ENAME, JOB, MGR, HIREDATE, SAL, COMM, DEPTNO) values (7934, 'MILLER', 'CLERK', 7782, '1982-01-23', 1300, null, 10);


-- 单表基础查询
--  1) 查询没有上级的员工全部信息（mgr是空的）
select * from emp where mgr is null;
-- 2) 列出30号部门所有员工的姓名、薪资
select ename,sal from emp where deptno=30;
-- 3)  查询姓名包含 'A'的员工姓名、部门名称 
select ENAME,DEPTNO from emp where ENAME like '%A%';
-- 4) 查询员工“TURNER”的员工编号和薪资
select EMPNO,SAL from emp where ENAME='TURNER';
-- 5) -- 查询薪资最高的员工编号、姓名、薪资
select EMPNO,ENAME,max(SAL) from emp;
--  6) -- 查询10号部门的平均薪资、最高薪资、最低薪资
select avg(DEPTNO),max(DEPTNO),min(DEPTNO) from emp;

-- 子查询
-- 1) 列出薪金比员工“TURNER”多的所有员工姓名（ename）、员工薪资（sal）
select ENAME,SAL from emp where sal > (select sal from emp where ENAME='TURNER');
-- 2) 列出薪金高于公司平均薪金的所有员工姓名、薪金。
select ENAME,SAL from emp where SAL > (select avg(SAL) from emp);
-- 3) 列出与“SCOTT”从事相同工作的所有员工姓名、工作名称(不展示Scott的姓名、工作)
select ENAME,JOB from emp where JOB = (select JOB from emp where ENAME = 'SCOTT') and ENAME<> 'SCOTT';
-- 4) 列出薪金高于30部门最高薪金的其他部门员工姓名、薪金、部门号。
select ENAME,SAL,DEPTNO from emp where SAL > (select max(SAL) from emp where DEPTNO =30);
-- 5) -- 查询薪资最高的员工编号、姓名、薪资
select EMPNO,ENAME,SAL from emp where sal = (select max(SAL) from emp);
-- 6) 列出薪金高于本部门平均薪金的所有员工姓名、薪资、部门号、部门平均薪资。
select EMPNO,ENAME,SAL,emp.DEPTNO,t.avgsal from emp,(select DEPTNO ,avg(sal) avgsal from emp group by DEPTNO) as t where emp.DEPTNO=t.DEPTNO and emp.sal>t.avgsal;

-- 单表基础查询

-- 1) 查询没有上级的员工全部信息（mgr是空的）

-- 2) 列出30号部门所有员工的姓名、薪资

-- 3)  查询姓名包含 'A'的员工姓名、部门名称 

-- 4) 查询员工“TURNER”的员工编号和薪资

-- 5) -- 查询薪资最高的员工编号、姓名、薪资

-- 6) -- 查询10号部门的平均薪资、最高薪资、最低薪资

-- 子查询

-- 1) 列出薪金比员工“TURNER”多的所有员工姓名（ename）、员工薪资（sal）

-- 2) 列出薪金高于公司平均薪金的所有员工姓名、薪金。

-- 3) 列出与“SCOTT”从事相同工作的所有员工姓名、工作名称(不展示Scott的姓名、工作)

-- 4) 列出薪金高于30部门最高薪金的其他部门员工姓名、薪金、部门号。

-- 5) -- 查询薪资最高的员工编号、姓名、薪资

-- 6) 列出薪金高于本部门平均薪金的所有员工姓名、薪资、部门号、部门平均薪资。
