-- sailors 水手信息表
create table sailors(
    `sid` int primary key ,
    sname varchar(10),
    rating int,
    age int
);
-- 添加数据
insert into sailors values
(22,"dusin",7,45),
(29,"brustus",1,33),
(31,"lubber",8,56),
(32,"andy",11,26),
(58,"rusty",10,36),
(64,"horatio",7,35),
(71,"dusin",10,35),
(74,"zorba",9,35),
(85,"horatio",6,26),
(86,"john",4,17),
(95,"bob",3,64),
(96,"frodo",4,26),
(98,"tom",6,17);



-- boats 船信息表
create table boats(
    `bid`int primary key,
    bname varchar(4),
    color varchar(8)
);

-- 加数据
insert into boats values
(101,"A","red"),
(102,"B","green"),
(103,"C","blue"),
(104,"D","white"),
(105,"E","red"),
(106,"F","blue"),
(107,"G","green");



-- reserves 水手订船信息表
create table reserves(
    `sid` int primary key,
    `bid` int,
    `rdate date` varchar(13)
);
drop table reserves;
desc reserves;
-- 删除主键
alter table reserves drop primary key;
-- 加数据 Replace into 表名 [(字段列表)] values(值列表); (相当于先删除后添加)
insert into reserves values
(22,101,"2010-01-08"),
(22,102,"2010-01-09"),
(29,103,"2010-01-09"),
(22,104,"2010-01-08"),
(22,103,"2010-03-10"),
(32,105,"2010-03-11"),
(32,106,"2010-03-08"),
(32,102,"2010-03-09"),
(58,104,"2010-03-20"),
(64,105,"2010-03-20"),
(95,101,"2010-04-02"),
(85,102,"2010-04-05"),
(22,101,"2010-04-07"),
(22,105,"2010-05-01"),
(22,106,"2010-06-08"),
(22,107,"2010-07-09"),
(32,105,"2010-08-06"),
(29,104,"2010-08-07"),
(64,103,"2010-09-05"),
(58,102,"2010-09-09"),
(64,104,"2010-11-03"),
(64,105,"2010-11-04");

-- 然后再增加主键
alter table reserves add primary key(`sid`);
alter table reserves add sid int primary key;

-- 1.查找 定了 103号船的水手
select * from sailors where sailors.sid in (select reserves.sid from reserves where bid in (select bid from boats where bid = 103));
-- select * from sailors where sid in (select sid from reserves where bid = 103);
-- 2.查找 定了 红色船水手的姓名
-- select bname from boats where color= "red";
select sname from sailors where sailors.sid in (select reserves.sid from reserves where bid in (select bid from boats where color= "red"));
-- 3.将年龄小于30的水手级别+1
-- select rating+1 from sailors where age < 30;
update sailors set rating = rating + 1 where age<30;
-- 4.查找 定了 红色船而没有定绿色船的水手姓名

 -- 5.查找没有定过船的水手信息

-- 6.查找定过船而没有定过红色船的水手信息

-- 7.查找没有定过红色船的水手信息

-- 8.查找定过所有船的水手姓名和编号

-- 9.查找年龄最大的水手姓名和年龄

-- 10.统计水手表中每个级别组的平均年龄和级别组

-- 11.统计水手表中每个人数不少于2人的级别组中年满18岁水手的平均年龄和级别组

-- 12.统计水手表中每个级别组的人数

-- 13.统计水手表中人数最少的级别组及人数

-- 14.查找定过船而没有定过相同的船的水手姓名

-- 15.删除名字叫lubber的水手的定船信息.




-- 创建sailors水手表
CREATE table sailors(
    sid int(3) not null,
    sname VARCHAR(8) not null,
    rating int(3),
    age int(3) 
);
-- 插入数据
insert into sailors values
(22,'dustin',7,45),
(29,'brustus',1,33),
(31,'lubber',8,56),
(32,'andy',11,26),
(58,'rusty',10,36),
(64,'horatio',7,35),
(71,'zorba',10,35),
(74,'horatio',9,35),
(85,'art',6,26),
(86,'john',4,17),
(95,'bob',3,64),
(96,'frodo',6,26),
(98,'tom',6,17);
-- 创建船信息表
create table boats(
    bid int(10) primary key auto_increment,
    bname varchar(10) not null,
    color varchar(15) not null
);
-- 插入数据
insert into boats values 
(101,"A",'red'),
(102,"B",'green'),
(103,"C",'blue'),
(104,"D",'white'),
(105,"E",'red'),
(106,"F",'blue'),
(107,"G",'green');
-- 创建rweserves水手订船信息表
create table reserves(
    sid int(10),
    bid int(10) not null,
    rdatedate int(20) not null 
);
-- 插入数据
insert into reserves values 
(22,101,2010-01-08),
(22,102,2010-01-09),
(29,103,2010-01-09),
(22,104,2010-03-08),
(22,103,2010-03-10),
(32,105,2010-03-11),
(32,106,2010-03-18),
(32,102,2010-03-19),
(58,104,2010-03-20),
(64,105,2010-03-20),
(95,101,2010-04-02),
(85,102,2010-04-05),
(22,101,2010-04-07),
(22,105,2010-05-01),
(22,106,2010-06-18),
(22,107,2010-07-09),
(32,105,2010-08-06),
(29,104,2010-08-07),
(64,103,2010-09-05),
(58,102,2010-09-09),
(64,104,2010-11-03),
(64,105,2010-11-04);