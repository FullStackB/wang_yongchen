1.单表查询

-- 1) 查询每门课程被选修的学生数
select cid,count(sid) from sc group by cid;


-- 2) 分别查询男生、女生的数量
select sSex,count(*) from student group by sSex;


-- 3) 查询姓“张”的学生名单
select sname from student where sname like '张%';


-- 4) 查询同名同姓学生名单，并统计同名人数
select sname,count(*) from student group by sname having count(*) > 1;


-- 5) 查询1981年出生的学生名单
select sid, sname  from student  where Year(sage) = 1981;


-- 6) 查询每门课程的平均成绩，结果按平均成绩升序排列，平均成绩相同时，按课程号降序排列
select cid , avg(score)  avgs  from sc group by cid order by avgs asc, cid desc;


-- 7) 选了课程的学生人数
select count(distinct sid) as 选课人数 from sc;


-- 8) 查询各个课程及相应的选修人数
select cid, count(cid) from sc group by cid;


-- 9) 查询至少选修了2门课程的学生学号
select sid from sc group by sid having count(*) > 2;


-- 10) 统计每门课程的学生选修人数(至少有2人选修的课程才统计)。要求输出课程号和选修人数
select cid ,count(*)  from sc group by cid having count(*) > 1 order by count(*) desc,cid asc;


-- 11) 删除“1002”同学的“001”课程的成绩
delete from sc where sid ='1002' and cid = '001';


-- 12) 查找“004”课程分数小于60，按分数降序排列的同学学号
select sid, score from sc where cid = '004' and score > 60 order by score desc;


-- 2.子查询

-- 1) 查询出只选修了一门课程的全部学生的学号和姓名
select sid,sname from student where sid=(select sid from sc group by sid having count(sid)=1);


-- 3.自连接

-- 1) 查询不同课程成绩相同的学生的学号、课程号、学生成绩；
select distinct sca.sid, sca.cid,scb.score from sc as sca ,sc as scb where sca.score=scb.score and sca.cid <>scb.cid;


-- 4.多表查询

-- 1) 查询平均成绩大于70 的所有学生的学号、姓名和平均成绩
select s.sid,s.sname,sas.avgs from student as s,( select sid,avg(score) avgs from sc group by sid having avgs>70) sas where s.sid=sas.sid;


-- 2) 查询课程名称为“数据库”，且分数低于60的学生姓名和分数
select s.sid,s.snamefrom sc,student as s,course as c where sc.sid=s.sid and c.cid=sc.cid  and c.cname='数据库' and sc.score<60;


-- 3) 查询所有学生的选课情况
select s.sid,s.sname,c.cid,c.cname from sc,student as s,course as c where sc.sid=s.sid and c.cid=sc.cid;


-- 4) 查询任何一门课程成绩在70分以上的姓名、课程名称和分数;
select  s.sname,c.cname,sc.score from sc,student s,course c where sc.sid=s.sid and c.cid=sc.cid and sc.score>70;


-- 5) 查询有不及格的课程的课程编号，课程名称，成绩，并按课程号从大到小排列
select sc.cid,c.cname,sc.score from sc ,course as c where sc.score<60 and c.cid=sc.cid order by sc.cid desc;


-- 6) 查询选了课程编号为003且课程成绩在70分以上的学生的学号和姓名
select s.sid,s.sname  from sc,student s where sc.sid=s.sid and sc.score>70 and sc.cid="003";


-- 7) 查询选修“叶平”老师所授课程的学生中，成绩最高的学生姓名及其成绩
select s.sname,c.cname, sc.score from student as s,sc,course as c,taacher as t where s.sid=sc.sid and sc.cid=c.cid and c.tid=t.tid and t.tname ='叶平' and sc.score=(select max(score) from sc where cid = c.cid);


-- 8) 查询两门以上不及格课程的同学的学号及其平均成绩
select sid , avg(score), count(*) from sc where score < 60 group by sid having count(*) > 2;