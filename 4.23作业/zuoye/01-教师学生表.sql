-- 创建数据库
CREATE database m_wyc;
-- 选择数据库
use m_wyc;

-- 学生表Student
-- 建表语句
CREATE TABLE student (
  	sid varchar(10) NOT NULL comment "学生编号",
  	sName varchar(20) DEFAULT NULL comment "学生姓名",
  	sAge datetime DEFAULT '1980-10-12 23:12:36' comment "学生出生日期",
  	sSex varchar(10) DEFAULT NULL comment "学生性别",
  	PRIMARY KEY (sid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据
insert  into student(sid,sName,sAge,sSex) values 
('1001','张三丰','1980-10-12 23:12:36','男'),
('1002','张无极','1995-10-12 23:12:36','男'),
('1003','李奎','1992-10-12 23:12:36','女'),
('1004','李元宝','1980-10-12 23:12:36','女'),
('1005','李世明','1981-10-12 23:12:36','男'),
('1006','赵六','1986-10-12 23:12:36','男'),
('1007','田七','1981-10-12 23:12:36','女');


-- 课程表 Course
-- 建表语句
CREATE TABLE course (
  cid varchar(10) NOT NULL comment "课程id编号",
  cName varchar(10) DEFAULT NULL comment "课程名称",
  tid int(20) DEFAULT NULL comment "课程节数",
  PRIMARY KEY (cid)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据
insert  into course(cid,cName,tid) values 
('001','企业管理',3),
('002','马克思',3),
('003','UML',2),
('004','数据库',1),
('005','英语',1);

-- 成绩表  SC
-- 建表语句
CREATE TABLE sc (
  sid varchar(10) DEFAULT NULL,
  cid varchar(10) DEFAULT NULL,
  score int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- 数据
insert  into sc(sid,cid,score) values 
('1001','001',80),
('1001','002',60),
('1001','003',75),
('1002','001',85),
('1002','002',70),
('1003','004',100),
('1003','001',90),
('1003','002',55),
('1004','002',65),
('1004','003',60);

-- 教师表 Teacher
-- 建表语句
CREATE TABLE teacher (
  tid int(10) DEFAULT NULL,
  tName varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 数据
insert  into teacher(tid,tName) values 
(1,'李老师'),
(2,'何以琛'),
(3,'叶平');



-- 1.单表查询

-- 1) 查询每门课程被选修的学生数
-- select cid,count(sid) num from sc group by cid;
select cName,count(*) num from course c join sc s on c.cid=s.cid group by c.cName;
-- select count(cid) from sc where cid in (select cid from course) group by cid;
  
-- 2) 分别查询男生、女生的数量
select sSex,count(*) from student group by sSex;
 
-- 3) 查询姓“张”的学生名单
select * from student where sName like "张%"; 

-- 4) 查询同名同姓学生名单，并统计同名人数
select sName,count(*) from student group by sName having count(*) > 1;

-- 5) 查询1981年出生的学生名单
-- select sid, sname  from student  where Year(sage) = 1981;
select sid, sname from student where sAge like "1981%";

-- 6) 查询每门课程的平均成绩，结果按平均成绩升序排列，平均成绩相同时，按课程号降序排列
select  cid,avg(score) avgs from sc group by cid order by avgs asc, cid desc;

-- 7) 选了课程的学生人数


-- 8) 查询各个课程及相应的选修人数


-- 9) 查询至少选修了2门课程的学生学号


-- 10) 统计每门课程的学生选修人数(至少有2人选修的课程才统计)。要求输出课程号和选修人数


-- 11) 删除“1002”同学的“001”课程的成绩


-- 12) 查找“004”课程分数小于60，按分数降序排列的同学学号


-- 2.子查询

-- 1) 查询出只选修了一门课程的全部学生的学号和姓名


-- 3.自连接

-- 1) 查询不同课程成绩相同的学生的学号、课程号、学生成绩；


-- 4.多表查询

-- 1) 查询平均成绩大于70 的所有学生的学号、姓名和平均成绩


-- 2) 查询课程名称为“数据库”，且分数低于60的学生姓名和分数


-- 3) 查询所有学生的选课情况


-- 4) 查询任何一门课程成绩在70分以上的姓名、课程名称和分数;


-- 5) 查询有不及格的课程的课程编号，课程名称，成绩，并按课程号从大到小排列


-- 6) 查询选了课程编号为003且课程成绩在70分以上的学生的学号和姓名


-- 7) 查询选修“叶平”老师所授课程的学生中，成绩最高的学生姓名及其成绩


-- 8) 查询两门以上不及格课程的同学的学号及其平均成绩