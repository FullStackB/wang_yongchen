const express = require('express');
const app = express();

// 静态资源
app.use(express.static('public'));
// 路由
const route = require('./route');
app.use(route);
// 模板引擎
const ejs = require('ejs');
app.set('view engine' , 'ejs');
app.set('views','./views'); 

// 启动服务器
app.listen(80, () => {
    console.log("sever is running at http://localhost:80");
})