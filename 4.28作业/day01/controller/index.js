// 4.1 配置登录页显示
module.exports.beLogin = (req, res) => {
    res.render('login');
}
// 4.2 配置首页显示
module.exports.beIndex = (req, res) => {
    res.render('index');
}
// 4.3 配置文章管理页面
module.exports.bePosts = (req, res) => {
    res.render('posts');
}
// 4.4 配置增加(写)文章页面
module.exports.bePostAdd = (req, res) => {
    res.render('post-add');
}
// 4.5 配置分类目录页面
module.exports.beCategories = (req, res) => {
    res.render('categories');
}
// 4.6 配置评论管理页面
module.exports.beComments = (req, res) => {
    res.render('comments');
}
// 4.7 配置用户管理页面
module.exports.beUsers = (req, res) => {
    res.render('users');
}
// 4.8 配置导航设置页面
module.exports.beNavMenus = (req, res) => {
    res.render('nav-menus');
}
// 4.9 配置图片轮播管理页面
module.exports.beSlides = (req, res) => {
    res.render('slides');
}
// 4.10 网站设置页面
module.exports.beSettings = (req, res) => {
    res.render('settings');
}
// 4.11 个人中心
module.exports.beProfile = (req, res) => {
    res.render('profile');
}

// 4.11 前台 首页显示

// // 4.12 前台 列表页面显示

// // 4.13 前台  详细文章显示