// 引入
const connection = require('../data');

// 4.1 配置登录页显示
module.exports.beLogin = (req, res) => {
    res.render('login');
}

// 4.2 配置首页显示
module.exports.beIndex = (req, res) => {
    res.render('index');
}

// 4.3 配置文章管理页面
module.exports.bePosts = (req, res) => {
    res.render('posts');
}

// 4.4 配置增加(写)文章页面
module.exports.bePostAdd = (req, res) => {
    res.render('post-add');
}

// 4.5 配置分类目录页面
module.exports.beCategories = (req, res) => {
    res.render('categories');
}

// 4.6 配置评论管理页面
module.exports.beComments = (req, res) => {
    res.render('comments');
}

// 4.7 配置用户管理页面  (添加 , 修改 , 删除 , 查询)
module.exports.beUsers = (req, res) => {
    res.render('users');
}
// 4.7.1  查询
module.exports.usersShow = (req, res) => {
    connection.query('select * from users', (err, results) => {
        if (err) return console.log(err);
        res.json(results);
    })
}
// 4.7.1  添加
module.exports.usersAdd = (req, res) => {
    let addDate = ['./uploads/default.png', req.body.email, req.body.slug, req.body.nickname, req.body.password, 'activated'];
    let addStr = 'insert into users values(null,?,?,?,?,?,?);';
    connection.query(addStr, addDate, (err, results) => {
        if (err) return console.log(err);
        if (results.affectedRows) {
            res.json({
                code: '1'
            })
        }
    })
}

module.exports.usersDelete = (req, res) => {
 // console.log(req.query.id);
 connection.query('delete from users where id=?', req.query.id, (error, results) => {
    if (error) {
      return console.log(error);
    }

    if (results.affectedRows) {
        
      res.json({
        code: '0',
        message: '用户删除成功'
      })
    }
  })
}
// 4.8 配置导航设置页面
module.exports.beNavMenus = (req, res) => {
    res.render('nav-menus');
}

// 4.9 配置图片轮播管理页面
module.exports.beSlides = (req, res) => {
    res.render('slides');
}

// 4.10 网站设置页面
module.exports.beSettings = (req, res) => {
    res.render('settings');
}

// 4.11 个人中心
module.exports.beProfile = (req, res) => {
    res.render('profile');
}


// 4.11 前台 首页显示

// // 4.12 前台 列表页面显示

// // 4.13 前台  详细文章显示