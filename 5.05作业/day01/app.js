const express = require('express');
const app = express();

// 配置body-parser 
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({extended:false}));

// 静态资源
app.use(express.static('public'));
// 路由
const route = require('./route');
app.use(route);
// 模板引擎
const ejs = require('ejs');
app.set('view engine' , 'ejs');
app.set('views','./views'); 

// 启动服务器
app.listen(3366, () => {
    console.log("sever is running at http://127.0.0.1:3366/beusers");
})