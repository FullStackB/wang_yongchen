// 导入 lodash
const _ = require('lodash');
// 导入 faker 生成假数据
const faker = require('faker');
// 设置本地化
faker.locale = 'zh_CN';

// 使用 CommonJS 规范把生成的数据导出；
// 其中，module.exports 必须指向 函数，且函数中，必须 return 一个数据对象
module.exports = () => {
    // json-server 要求最终 return 的数据，必须是一个对象；
    const data = {
        // 数组形式
        movieList: []
    };
    // 调用 _.times 随机 10 次生成假数据
    data.movieList = _.times(10, n => {
        return {
            id: n + 1,
            name: faker.random.word(),
            type: faker.random.words(),
            ctime: faker.date.recent()
        }
    })
    // 把生成的数据返回
    return data;
}