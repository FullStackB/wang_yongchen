import Vue from 'vue';

// 引入 axios
import axios from 'axios';
Vue.prototype.$http = axios;



import router from '../../routes/index.js'

// 引入APP组件
import App from './App.vue'

new Vue({
    el: '#app',
    render: h => h(App),
    router
})