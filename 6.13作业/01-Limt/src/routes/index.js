import Vue from 'vue';

import VueRouter from 'vue-router';
Vue.use(VueRouter);

// 引入组件
import movieList from '../components/01-list/movieList.vue';
import movieDetail from '../components/01-list/movieDetail.vue';

const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/movie/list'
        },
        {
            path: '/movie/list',
            component: movieList
        },
        {
            path: '/movie/detail/:id',
            component: movieDetail,
            props: true
        }
    ]
})
export default router;