import Vue from 'vue'
// 引入 axios
import axios from 'axios'
// 重命名
Vue.prototype.$http = axios;

// 引入主组件 APP
import app from './App.vue'
// 引入路由
import router from '../routes'
// 实例化vue
new Vue({
    el: '#app',
    render: h => h(app),
    // 挂载
    router
})