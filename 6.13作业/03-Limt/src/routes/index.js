// 导入 vue
import Vue from 'vue';
// 导入 路由
import VueRouter from 'vue-router';
// 安装 路由
Vue.use(VueRouter);

// 引入 组件
import MovieList from '../components/movieList.vue';
import MovieDetail from '../components/movieDetail.vue';
// 创建路由对象 
const router = new VueRouter({
    routes: [{
            path: '/',
            redirect: '/movie/list'
        },
        {
            path: '/movie/list',
            component: MovieList
        },
        {
            path: '/movie/detail/:id',
            component: MovieDetail,
            props: true
        }
    ]
})
export default router;