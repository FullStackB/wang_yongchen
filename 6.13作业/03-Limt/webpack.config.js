const path = require('path');

const HtmlWebpackPlugin = require('./node_modules/html-webpack-plugin');
const CleanWebpackPlugin = require('./node_modules/clean-webpack-plugin/dist/clean-webpack-plugin');
// 导入插件
const VueLoaderPlugin = require('./node_modules/vue-loader/lib/plugin');
// new 一个插件的实例对象
const vuePlugin = new VueLoaderPlugin();
module.exports = {
    module: 'development',
    entry: './src/js/index.js',
    output: {
        filename: 'index.js',
        path: path.join(__dirname, 'dist')
    },
    plugins: [
        new HtmlWebpackPlugin({
            // 模板文件在哪里
            template: './src/index.html',
            filename: 'index.html'
        }),
        new CleanWebpackPlugin(),
        vuePlugin
    ],
    // 服务器去哪里找
    devServer: {
        contentBase: './dist'
    },
    module: {
        rules: [
            { test: /\.css$/, use: ['style-loader', 'css-loader'] },
            { test: /\.jpg|png|gif|bmp$/, use: 'url-loader' },
            // 字体 
            { test: /\.(woff|woff2|eot|ttf|otf|svg)$/, use: ['file-loader'] },
            { test: /\.vue$/, use: 'vue-loader' }
        ]
    }
}