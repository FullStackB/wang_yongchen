import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";
// 导入首页子组件
import Welcome from './views/Welcome.vue'

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      redirect: "/home"
    },
    {
      path: "/home",
      component: Home,
      redirect: '/welcome',
      children: [{
        path: '/welcome',
        component: Welcome
      }]
    },
    {
      path: "/login",
      component: Login
    }
  ]
});

// 路由导航守卫
router.beforeEach((to, form, next) => {
  // 2、 进入login网页 但是没给login放行
  if (to.path === "/login") return next();
  // 1、 访问其他页面的时候 没有token的话就无法访问其他网页
  const tokenStr = window.sessionStorage.getItem("token");
  if (!tokenStr) return next("/login");
  // 3、访问其他网页时，也必须放行
  next();
});
export default router;
