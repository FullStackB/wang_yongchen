export default {
    data() {
        return {
            // 角色的数据
            rolesList: [],
            // 添加角色弹出框 默认为false
            addDialogVisible: false,
            // 添加角色数据名
            addForm: {
                // 角色名称
                roleName: '',
                // 角色描述
                roleDesc: ''
            },
            // 添加角色数据判验证规则
            addFormRules: {
                // 角色名称
                roleName: [{
                        required: true,
                        message: '请输入角色名称',
                        trigger: 'blur'
                    },
                    {
                        min: 3,
                        max: 8,
                        message: '长度在 3 到 8 个字符',
                        trigger: 'blur'
                    }
                ],
                // 角色描述
                roleDesc: [{
                        required: true,
                        message: '请输入角色描述',
                        trigger: 'blur'
                    },
                    {
                        min: 3,
                        max: 30,
                        message: '长度在 3 到 30 个字符',
                        trigger: 'blur'
                    }
                ]
            },
            // 修改角色数据弹出框 默认为false
            echoDialogVisible: false,
            // 修改角色数据名
            echoForm: {},
            //  分配权限弹出框 默认为false
            jurisdictionDialogVisible: false,
            // 树节点的数据
            rightTree: [],
            // 定义树节点的数据,每一个节点的数据和它子节点的数据  直接的关系
            treeProp: {
                // 首节点显示,要显示的内容  把那个内容显示出来，就把该名字给赋值出来 赋值到另一个变量 个标签有关
                label: 'authName',
                // 子节点
                children: 'children'
            },
            // 树节点默认勾选 三角函数
            defaultKeys: []
        }
    },
    // 生命周期
    created() {
        // 刷新页面
        this.getRolesList();
    },
    methods: {
        // 查询所有角色数据
        async getRolesList() {
            const {
                data: res
            } = await this.$http.get("roles");
            // console.log(res);
            // 判断是否等于200
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 把数据放到数组中
            this.rolesList = res.data;
        },
        // 添加角色数据的弹出框 隐藏时 清除表单
        addClose() {
            this.$refs.addFormRef.resetFields();
        },
        // 添加角色数据
        addRoles() {
            // 判断表单验证规则
            this.$refs.addFormRef.validate(async valid => {
                if (!valid) return;
                // 添加角色数据
                const {
                    data: res
                } = await this.$http.post("roles", this.addForm);
                // console.log(res);
                // 判断是否添加成功
                if (res.meta.status !== 201) return this.$message.error(res.meta.msg);
                // 把添加角色弹出框隐藏
                this.addDialogVisible = !this.addDialogVisible;
                // 提示框
                this.$message.success("添加角色成功");
                // 刷新页面
                this.getRolesList();
            })

        },
        // 删除角色数据
        async delRoles(id) {
            // console.log(id);
            // 弹出框
            const confirm = await this.$confirm('此操作将永久删除该角色, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(error => error);
            // console.log(confirm);
            // 判断是否点击的取消
            if (confirm === "cancel") return this.$message.info("已取消删除");

            // 删除角色数据
            const {
                data: res
            } = await this.$http.delete(`roles/${id}`);
            // console.log(res);
            // 判断是否删除成功
            if (res.meta.status !== 200) return this.$message.error('删除角色失败！');
            // 提示框
            this.$message.success(res.meta.msg);
            // 刷新页面
            this.getRolesList();
        },
        // 修改角色数据的弹出框 隐藏时 清除表单
        echoClose() {
            this.$refs.echoFormRef.resetFields();
        },
        // 回显角色数据
        async echoRoles(id) {
            // console.log(id);
            // 修改角色弹出框显示
            this.echoDialogVisible = !this.echoDialogVisible;
            // 把角色数据查询
            const {
                data: res
            } = await this.$http.get(`roles/${id}`);
            // console.log(res);
            // 判断是否查询角色数据正确
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 把resd.data 里的数据全部给 一个对象
            this.echoForm = res.data;
        },
        // 修改角色数据
        modifyRoles() {
            // console.log(this.echoForm);
            // 判断表单验证规则
            this.$refs.echoFormRef.validate(async valid => {
                if (!valid) return;
                // 修改数据
                const {
                    data: res
                } = await this.$http.put(`roles/${this.echoForm.roleId}`, this.echoForm);
                // console.log(res);
                // 判断是否修改成功
                if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
                // 修改角色弹出框隐藏
                this.echoDialogVisible = !this.echoDialogVisible;
                // 提示框
                this.$message.success("修改角色成功");
                // 刷新页面
                this.getRolesList();
            });
        },
        // 根据权限ID 删除角色指定下的权限
        async removeRightRoles(role, id) {
            // console.log(id);
            // 弹出框
            const confirm = await this.$confirm('此操作将永久删除该角色, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(error => error);
            // console.log(confirm);
            // 判断是否点击的取消
            if (confirm === "cancel") return this.$message.info("已取消删除");

            // 删除角色数据
            const {
                data: res
            } = await this.$http.delete(`roles/${role.id}/rights/${id}`);
            // console.log(res);
            // 判断是否删除成功
            if (res.meta.status !== 200) return this.$message.error('删除权限失败！');
            // 提示框
            this.$message.success(res.meta.msg);
            // 把服务器返回的最新的权限数组，赋值给 当前角色的 children 属性
            role.children = res.data;
            // // 刷新页面
            // this.getRolesList();
        },
        // 回显分配权限数据
        async jurisdictionRoles(role) {
            // console.log(role);
            // 分配权限弹出框
            this.jurisdictionDialogVisible = !this.jurisdictionDialogVisible;
            // 查询分配权限数据
            const {
                data: res
            } = await this.$http.get("rights/tree");
            // console.log(res);
            // 判断是否查询数据正确
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 把数据放到树节点数组中
            this.rightTree = res.data;
            // 定义一个key数组 目的是为了 存储回显时所选中的数据
            var keys = [];
            // 要调用函数 把数据给key中
            this.getLeafId(role, keys)
            // 把数据给 默认勾选的数组中
            this.defaultKeys = keys;
        },
        getLeafId(role, keys) {
            // console.log(role);
            // console.log(keys);
            // 判断有没有中权限，没有选中 就把ID 传过去
            if (!role.children) return keys.push(role.id);
            // 如果有数据选中 就循环
            role.children.forEach(item => this.getLeafId(item, keys));
        }
    },
}