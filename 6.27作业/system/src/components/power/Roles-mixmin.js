export default {
    data() {
        return {
            // 渲染数据
            rolesDate: [],
            // 添加角色 弹出框
            addDialogVisible: false,
            // 添加角色 数据名
            addForm: {
                // 角色名称
                roleName: '',
                // 角色描述
                roleDesc: ''
            },
            // 添加角色 验证
            addFormRules: {
                // 角色名称
                roleName: [{
                        required: true,
                        message: '请输入角色名称',
                        trigger: 'blur'
                    },
                    {
                        min: 3,
                        max: 15,
                        message: '长度在 3 到 15 个字符',
                        trigger: 'blur'
                    }
                ],
                // 角色描述
                roleDesc: [{
                        required: true,
                        message: '请输入角色描述',
                        trigger: 'blur'
                    },
                    {
                        min: 3,
                        max: 20,
                        message: '长度在 3 到 20 个字符',
                        trigger: 'blur'
                    }
                ]
            },
            // 修改角色 弹出框
            echoDialogVisible: false,
            // 修改角色 数据名
            echoForm: {}
        }
    },
    methods: {
        // 查询渲染数据
        async renderRoles() {
            // 查询数据
            const {
                data: res
            } = await this.$http.get("roles");
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("查询渲染数据失败");
            // 把数据放到数组中
            this.rolesDate = res.data;
        },
        // 关闭 添加角色 重置表单
        addClose() {
            this.$refs.addFormRef.resetFields();
        },
        // 添加角色
        addRoles() {
            // 判断表单验证规则
            this.$refs.addFormRef.validate(async valid => {
                if (!valid) return;
                // 添加角色
                const {
                    data: res
                } = await this.$http.post("roles", this.addForm);
                // console.log(res);
                // 判断
                if (res.meta.status !== 201) return this.$message.error("添加角色失败");
                // 提示框
                this.$message.success("添加角色成功");
                // 添加角色 对话框 关闭
                this.addDialogVisible = !this.addDialogVisible;
                // 渲染页面
                this.renderRoles();
            });
        },
        // 删除 角色
        async delRoles(id) {
            const confirm = await this.$confirm('此操作将永久删除该角色, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(err => err);
            // console.log(confirm);
            // 判断是否点击取消按钮  
            if (confirm === "cancel") return this.$message.info("已取消删除");
            // 删除角色
            const {
                data: res
            } = await this.$http.delete(`roles/${id}`);
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("删除角色失败");
            // 提示框
            this.$message.success("删除角色成功");
            // 渲染页面
            this.renderRoles();
        },
        // 关闭 修改角色 重置表单
        echoClose() {
            this.$refs.echoFormRef.resetFields();
        },
        // 回显 修改角色
        async echoRoles(id) {
            // 修改角色 弹出框
            this.echoDialogVisible = !this.echoDialogVisible;
            // 回显数据
            const {
                data: res
            } = await this.$http.get(`roles/${id}`);
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("获取数据失败");
            // 把数据放到 修改角色 对象中
            this.echoForm = res.data;
        },
        // 修改角色
        async editRoles() {
            // 判断表单验证规则
            this.$refs.echoFormRef.validate(async valid => {
                if (!valid) return;
                const {
                    data: res
                } = await this.$http.put(`roles/${this.echoForm.roleId}`, this.echoForm);
                // console.log(res);
                // 判断
                if (res.meta.status !== 200) return this.$message.error("修改角色失败");
                // 修改角色 弹出框
                this.echoDialogVisible = !this.echoDialogVisible;
                // 提示框
                this.$message.success("修改角色成功");
                // 渲染页面
                this.renderRoles();
            })
        },
        // 删除
        async jurisTagId(row, id) {
            const confirm = await this.$confirm('此操作将永久删除该角色, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(err => err);
            // console.log(confirm);
            // 判断是否点击取消按钮  
            if (confirm === "cancel") return this.$message.info("已取消删除");
            // 删除角色
            const {
                data: res
            } = await this.$http.delete(`roles/${row.id}/rights/${id}`);
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("删除角色失败");
            // 提示框
            this.$message.success("删除角色成功");
            // 为了实现 不刷新页面
            row.children = res.data;
            // 渲染页面
            // this.renderRoles();
        }
    },
    created() {
        // 渲染页面
        this.renderRoles();
    },
}