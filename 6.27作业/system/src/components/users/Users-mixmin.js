export default {
    data() {
        // 添加用户判断手机号
        var addMobile = (rule, value, callback) => {
            // 判断是否输入内容
            if (!value) {
                return callback(new Error('请输入手机号'));
            }
            // 判断是否正确，默认是正确 然后取反正确时不进入判断,所以加一个!
            if (!(/^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/.test(value))) return callback(new Error('请输入正确的11位手机号码'));
            // 正确时，就显示该样式
            callback();
        };
        return {
            // 用户渲染数据
            userLists: [],
            // 查询用户参数
            usersInfo: {
                // 查询参数
                query: "",
                // 当前页码
                pagenum: 1,
                // 每页显示条数
                pagesize: 2
            },
            // 总数据的个数  在查询数据中查询  默认为 0 
            total: 0,
            // 添加用户弹出框
            addDialogVisible: false,
            // 添加用户数据名
            addForm: {
                // 用户名
                username: "",
                // 密码
                password: "",
                // 邮箱
                email: "123@126.cn",
                // 手机号
                mobile: "17553041262"
            },
            // 添加用户验证
            addFormRules: {
                // 用户名
                username: [{
                        required: true,
                        message: '请输入用户名',
                        trigger: 'blur'
                    },
                    {
                        min: 2,
                        max: 8,
                        message: '长度在 2 到 8 个字符',
                        trigger: 'blur'
                    }
                ],
                // 密码
                password: [{
                        required: true,
                        message: '请输入密码',
                        trigger: 'blur'
                    },
                    {
                        min: 6,
                        max: 18,
                        message: '长度在 6 到 18 个字符',
                        trigger: 'blur'
                    }
                ],
                // 邮箱
                email: [{
                        required: true,
                        message: '请输入邮箱地址',
                        trigger: 'blur'
                    },
                    {
                        type: 'email',
                        message: '请输入正确的邮箱地址',
                        trigger: 'blur'
                    }
                ],
                // 手机号
                mobile: [{
                    required: true,
                    validator: addMobile,
                    trigger: 'blur'
                }]
            },
            // 修改用户弹出框
            echoDialogVisible: false,
            // 修改用户数据名
            echoForm: {},
            // 分配权限 弹出框
            jurisDialogVisible: false,
            // 分配权限 数据
            jurisInfo: {},
            // 下拉菜单 选择第几个
            jurisId: '',
            // 下拉菜单 数组 遍历
            jurisList: []
        }
    },
    methods: {
        async usersList() {
            // 用户渲染数据查询
            const {
                data: res
            } = await this.$http.get("users", {
                params: this.usersInfo
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 赋值数据
            this.userLists = res.data.users;
            // 总数据个数
            this.total = res.data.total;
        },
        // 页面显示多少 数据
        handleSizeChange(val) {
            // 把改变的值给 每页显示条
            this.usersInfo.pagesize = val;
            // 重新渲染数据
            this.usersList();
        },
        // 显示当前页数
        handleCurrentChange(val) {
            // 把改变的值给 、当前页数
            this.usersInfo.pagenum = val;
            // 重新渲染数据
            this.usersList();
        },
        // 添加用户
        addUsers() {
            this.$refs.addFormRef.validate(async valid => {
                if (!valid) return;
                const {
                    data: res
                } = await this.$http.post("users", this.addForm);
                // console.log(res);
                // 判断
                if (res.meta.status !== 201) return this.$message.error(res.meta.msg);
                // 提示框
                this.$message.success("添加用户成功");
                // 添加用户弹出框隐藏
                this.addDialogVisible = !this.addDialogVisible
                // 重新渲染数据
                this.usersList();
            })
        },
        // 修改用户状态
        async stateUsers(id, state, row) {
            // 修改
            const {
                data: res
            } = await this.$http.put(`users/${id}/state/${state}`);
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) {
                row.mg_state = !row.mg_state;
                return this.$message.error("修改用户状态失败");
            }
        },
        // 删除
        async delUser(id) {
            // 手写弹框
            const confirm = await this.$confirm('此操作将永久删除该用户, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(err => err);
            // console.log(confirm);
            // 判断
            if (confirm == "cancel") return this.$message.info("已取消删除");
            // 删除
            const {
                data: res
            } = await this.$http.delete(`users/${id}`);
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 提示框
            this.$message.success("用户删除成功");
            // 重新渲染数据
            this.usersList();
        },
        // 修改回显
        async echoUser(id) {
            // 显示修改用户弹出框
            this.echoDialogVisible = !this.echoDialogVisible;
            // 回显
            const {
                data: res
            } = await this.$http.get(`users/${id}`);
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 把数据给 修改用户数据名  对象
            this.echoForm = res.data;
        },
        // 修改数据
        async editUsers() {
            // 修改
            const {
                data: res
            } = await this.$http.put(`users/${this.echoForm.id}`, {
                // 用户名回显数据
                username: this.echoForm.username,
                // 邮箱回显数据
                email: this.echoForm.email,
                // 手机号回显数据
                mobile: this.echoForm.mobile
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 提示框
            this.$message.success("修改用户成功");
            // 隐藏修改用户弹出框
            this.echoDialogVisible = !this.echoDialogVisible;
            // 重新渲染数据
            this.usersList();
        },
        // 分配权限 回显
        async jurisUser(row) {
            // 弹出框
            this.jurisDialogVisible = !this.jurisDialogVisible;
            // console.log(row);
            // 把数据放到 自定义的数组中
            this.jurisInfo = row;
            // 请求  获取角色列表数据
            const {
                data: res
            } = await this.$http.get("roles");
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 把数据放到 数组中 遍历
            this.jurisList = res.data;
        },
        // 分配权限 修改
        async jurisUsers() {
            // 什么没选择  就 提示
            if (!this.jurisId) return this.$message.warning("请选择要分配的新角色！");
            // 修改
            const {
                data: res
            } = await this.$http.put(`users/${this.jurisInfo.id}/role`, {
                rid: this.jurisId
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 提示框
            this.$message.success("分配的角色成功！");
            // 弹出框
            this.jurisDialogVisible = !this.jurisDialogVisible;
            // 重新渲染数据
            this.usersList();
        }
    },
    // 生命周期
    created() {
        // 重新渲染数据
        this.usersList();
    },
};