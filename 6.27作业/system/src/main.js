import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
// axios
import axios from "axios";
// 设置路由路径
axios.defaults.baseURL = "https://www.escook.cn:8888/api/private/v1/";
// 设置路由的 http
Vue.prototype.$http = axios;

// 设置拦截器
axios.interceptors.request.use(config => {
  // console.log(config);

  config.headers.Authorization = window.sessionStorage.getItem("token");
  return config;
});

Vue.use(ElementUI);
Vue.config.productionTip = false;
// 引入矢量图图标
import "./assets/fonts/iconfont.css";

// 引入全局自定义css
import "./assets/css/base.css";
new Vue({
  router,
  render: h => h(App)
}).$mount("#app");