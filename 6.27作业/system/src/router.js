import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [{
      path: "/",
      redirect: "/home"
    },
    {
      path: "/home",
      component: Home,
      redirect: '/welcome',
      children: [{
        path: '/welcome',
        component: () => import("./components/Welcome.vue")
      }, {
        path: '/users',
        component: () => import("./components/users/Users.vue")
      }, {
        path: '/roles',
        component: () => import("./components/power/Roles.vue")
      }]
    },
    {
      path: "/login",
      component: () => import("./views/Login.vue")
    }
  ]
});
// 路由导航守卫
router.beforeEach((to, from, next) => {
  //  访问login
  if (to.path === "/login") return next();
  // 访问其他页面
  const tokenStr = window.sessionStorage.getItem("token");
  if (!tokenStr) return next("/login");
  // 放行
  next();
})

export default router;