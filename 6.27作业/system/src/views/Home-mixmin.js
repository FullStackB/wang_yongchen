export default {
    data() {
        return {
            // 定义数组左侧
            homeList: [],
            // 定义图标数组
            iconList: [
                "icon-users",
                "icon-tijikongjian",
                "icon-shangpin",
                "icon-danju",
                "icon-baobiao"
            ],
            // 折叠
            collapse: false
        }
    },
    methods: {
        // 退出
        outHome() {
            // 清楚token
            window.sessionStorage.removeItem("token");
            // 提示框
            this.$message.success("退出成功");
            // 退出路由
            this.$router.push("/login");
        },
        // 左侧菜单栏渲染数据
        async menuHome() {
            // 获取数据   无效的token 设置拦截器
            const {
                data: res
            } = await this.$http.get("menus");
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 赋值
            this.homeList = res.data;
        }
    },
    // 生命周期
    created() {
        // 刷新页面 渲染数据
        this.menuHome();
    },
}