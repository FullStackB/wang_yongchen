export default {
    data() {
        return {
            // 渲染数据
            categoDate: [],
            // 渲染参数
            categoDateInfo: {
                // 表示显示一层二层三层分类列表  那么就是 数组 
                type: [],
                // 当前页码值
                pagenum: 1,
                // 每页显示多少条数据
                pagesize: 5
            },
            // 总数据
            total: 0,
            // 定义表格中，都有哪些列
            columns: [
                {
                    label: "分类名称",
                    prop: "cat_name"
                },
                {
                    label: "是否有效",
                    prop: "cat_deleted",
                    // 先指定使用自定义模板渲染
                    type: "template",
                    // 模板名叫什么
                    template: "isok"
                },
                {
                    label: "排序",
                    prop: "cat_level",
                    // 先指定使用自定义模板渲染
                    type: "template",
                    // 模板名叫什么
                    template: "level"
                },
                {
                    label: "操作",
                    // 先指定使用自定义模板渲染
                    type: "template",
                    // 模板名叫什么
                    template: "opt"
                }
            ],
            // 添加分类 弹出框
            addDialogVisible: false,
            // 添加分类 数据名
            addForm: {
                //分类ID
                cat_pid: 0,
                // 分类名称
                cat_name: '',
                // 分类层级
                cat_level: 0
            },
            // 添加分类 判断验证规则对象
            addFormrules: {
                cat_name: [{
                        required: true,
                        message: '请输入分类名称',
                        trigger: 'blur'
                    },
                    {
                        min: 3,
                        max: 100,
                        message: '长度在 3 到 15 个字符',
                        trigger: 'blur'
                    }
                ]
            },
            // 下拉菜单的 数据
            addCateList: [],
            // 根据ID 去选中哪一条信息
            cascaderId: [],
            // 级联选项框
            addCateListProps: {
                // 选中的Id
                value: "cat_id",
                // 名字
                label: "cat_name",
                // 二级
                children: "children"
            },
            // 修改分类 弹出框
            eachDialogVisible: false,
            // 修改分类 数据名
            eachForm: {
                // 分类名称
                cat_name: ''
            },
        }
    },
    methods: {
        async categoLists() {
            const {
                data: res
            } = await this.$http.get("categories", {
                params: this.categoDateInfo
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("获取商品分类列表失败!");
            // 把数据方到渲染数组中
            this.categoDate = res.data.result;
            // 因分页 需要总数据的个数
            this.total = res.data.total;
        },
        // 页面的页数
        handleCurrentChange(val) {
            this.categoDateInfo.pagenum = val;
            // 渲染数据
            this.categoLists();
        },
        // 对话框隐藏后 对话框中 的表单清除
        addCateClose() {
            this.$refs.addFormRef.resetFields();
        },
        // 添加分类  弹出框  级联下拉菜单数据
        async addDialog() {
            // 添加分类  弹出框
            this.addDialogVisible = !this.addDialogVisible;
            // 先获取 第一级 和第二级
            const {
                data: res
            } = await this.$http.get("categories", {
                params: {
                    type: 2
                }
            });
            // console.log(res);
            // 判断 是否查询到了数据 
            if (res.meta.status !== 200) return this.$message.error('获取父级分类失败！');
            // 把数据给下拉菜单的 数组
            this.addCateList = res.data;
        },
        // 触发事件
        addCateChange() {
            // console.log(this.cascaderId.length);
            // 判断 ID 去选中哪一条信息 是否长度 为 0  如果为0 就说明 没有选择
            if (this.cascaderId.length === 0) {
                // 一级分类的父Id为0
                this.addForm.cat_pid = 0;
                // 要添加一级分类
                this.addForm.cat_level = 0;
            } else {
                // 如果数组不为空，则把数组最后一项的值，当作 父级分类的Id
                this.addForm.cat_pid = this.cascaderId[this.cascaderId.length - 1];
                // 设置 分类的等级
                this.addForm.cat_level = this.cascaderId.length;
                // console.log(this.addForm.cat_pid);
            }

        },
        // 点击 添加分类 中的 确定按钮
        addCate() {
            // 判断校验 规则
            this.$refs.addFormRef.validate(async valid => {
                // 判断是否正确
                if (!valid) return;
                // 添加数据
                const {
                    data: res
                } = await this.$http.post("categories", this.addForm);
                // console.log(res);
                // 判断
                if (res.meta.status !== 201) return this.$message.error("添加分类失败！");
                // 提示框
                this.$message.success('添加分类成功！');
                // 添加分类  弹出框
                this.addDialogVisible = !this.addDialogVisible;
                // 渲染数据
                this.categoLists();
            });

        },
        // 删除事件
        async delCate(id) {
            // 删除弹出框
            const confirmResult = await this.$confirm('此操作将永久删除该分类, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(error => error);
            // console.log(confirmResult);
            if (confirmResult === "cancel") return this.$message.info("已取消删除！");
            // 删除
            const {
                data: res
            } = await this.$http.delete(`categories/${id}`);
            // console.log(res);
            if (res.meta.status !== 200) return this.$message.error("删除分类失败！");
            // 没有删除  
            // 提示框
            this.$message.success("删除分类成功！");
            // 删除成功后  判断当前页面上是否 为唯一一条数据
            if (this.categoDate.length == 1 && this.categoDateInfo.pagenum > 1) {

                this.categoDateInfo.pagenum--;
            }
            // 渲染数据
            this.categoLists();
        },
        // 对话框隐藏后 对话框中 的表单清除
        eachCateClose() {
            this.$refs.eachFormRef.resetFields();
        },
        // 回显数据 分类
        async eachCateData(id) {
            // console.log(id);
            // 修改分类  弹出框
            this.eachDialogVisible = !this.eachDialogVisible;
            // 查询数据
            const {
                data: res
            } = await this.$http.get(`categories/${id}`);
            // console.log(res);
            if (res.meta.status !== 200) return this.$message.error('获取分类数据失败！');
            // 把回显的数据回显
            this.eachForm = res.data;
        },
        // 修改回显数据
        eachCate() {
            // 判断校验 规则
            this.$refs.eachFormRef.validate(async valid => {
                // 判断是否正确
                if (!valid) return;
                // 修改数据
                const {
                    data: res
                } = await this.$http.put(`categories/${this.eachForm.cat_id}`, {
                    cat_name: this.eachForm.cat_name
                });
                // 判断
                if (res.meta.status !== 200) return this.$message.error("更新分类失败！");
                // 提示框
                this.$message.success('更新分类成功！');
                // 修改分类  弹出框
                this.eachDialogVisible = !this.eachDialogVisible;
                // 渲染数据
                this.categoLists();
            })
        }
    },
    created() {
        // 渲染数据
        this.categoLists();
    },
};