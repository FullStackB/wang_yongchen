export default {
    data() {
        return {
            // 权限列表数据
            rightsList: []
        }
    },
    // 生命周期
    created() {
        this.getRightList();
    },
    methods: {
        // 查询数据
        async getRightList() {
            // 查询数据 发起 请求
            const {
                data: res
            } = await this.$http.get("rights/list");
            // console.log(res);
            // 把数据放到数组中
            this.rightsList = res.data;
        }
    },
}