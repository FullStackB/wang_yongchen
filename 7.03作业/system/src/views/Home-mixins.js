export default {
    data() {
        return {
            // 定义数据数组遍历
            getMenuLists: [],
            // 定义图标数组
            iconList: [
                "icon-users",
                "icon-tijikongjian",
                "icon-shangpin",
                "icon-danju",
                "icon-baobiao"
            ],
            // 折叠
            colFold: false
        };
    },
    methods: {
        // 退出 返回登陆界面
        loginOut() {
            // 清除token
            window.sessionStorage.removeItem("token");
            // 推出后访问的路由 登录界面
            this.$router.push("/login");
            // 退出成功  弹出提示框
            this.$message.success("退出成功");
        },
        // 获取左侧栏的数据
        async getMenuList() {
            // 获取数据 如果数据没有获取到出现  无效token 那就说名没有设置头 也就是拦截器 去main.js里去设置
            const {
                data: res
            } = await this.$http.get("menus");
            // console.log(res);
            // 判断如果没有获取到数据就报错
            if (res.meta.status != 200)
                return this.$message.error("获取左侧菜单失败!");
            // 把数据添加到自定义数组中
            this.getMenuLists = res.data;
        }
    },
    // 生命周期
    created() {
        this.getMenuList();
    }
};