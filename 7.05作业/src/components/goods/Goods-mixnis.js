export default {
    data() {
        return {
            // 数据的渲染 数组
            goodsList: [],
            // 渲染的参数
            goodsListInfo: {
                // 查询参数
                query: '',
                // 当前页码
                pagenum: 1,
                // 每页显示条数
                pagesize: 10
            },
            // 数据总数据 个数
            total: 0
        }
    },
    methods: {
        // 查询数据 
        async getGoodsList() {
            // 数据查询
            const {
                data: res
            } = await this.$http.get("goods", {
                params: this.goodsListInfo
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("获取商品数据失败");
            // 把数据放到 数组中
            this.goodsList = res.data.goods;
            // 把总数据放到 定义总数据的变量中
            this.total = res.data.total;
        },
        // 当前页数 的个数 别忘记渲染数据 每一次执行 都要 渲染数据
        handleSizeChange(val) {
            this.goodsListInfo.pagesize = val;
            // 渲染数据 刷新页面
            this.getGoodsList();
        },
        // 当前页数 的页数 别忘记渲染数据 每一次执行 都要 渲染数据
        handleCurrentChange(val) {
            this.goodsListInfo.pagenum = val;
            // 渲染数据 刷新页面
            this.getGoodsList();
        },
        // 删除商品数据
        async delGoods(id) {
            // console.log(id);
            // 弹出框
            const confirm = await this.$confirm('此操作将永久删除该文件, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(error => error);
            // console.log(confirm);
            // 判断
            if (confirm == "cancel") return this.$message.info("已取消删除数据成功");
            // 删除
            const {
                data: res
            } = await this.$http.delete(`goods/${id}`);
            console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("删除商品数据失败");
            // 提示框
            this.$message.success("删除商品数据失败");
            // 渲染数据 刷新页面
            this.getGoodsList();
        }
    },
    created() {
        // 渲染数据 刷新页面
        this.getGoodsList();
    },
};