export default {
    data() {
        return {
            // 级联下拉菜单 渲染数据
            paramsData: [],
            // 渲染参数
            paramsDataInfo: {
                // 表示显示一层二层三层分类列表  那么就是 数组 
                type: [],
                // 当前页码值
                pagenum: 1,
                // 每页显示多少条数据
                pagesize: 5
            },
            // 显示的数据
            paramsDataProps: {
                // 显示的名字
                label: "cat_name",
                // 区分 vulue 的值
                value: "cat_id",
                // 是否有子数组
                children: "children"
            },
            // 选中的ID 级联的下拉菜单
            paramsDataId: [],
            // tab 默认显示的
            activeName: "many",
            // 动态参数 数据
            manyParamsData: [],
            // 静态属性 数据
            onlyParamsData: [],
            // 添加 添加动态参数 和 添加静态属性 弹出框
            addDialogVisible: false,
            // 添加 数据名
            addForm: {
                attr_name: ''
            },
            // 判断校验规则
            addFormRules: {
                attr_name: [{
                        required: true,
                        message: '请输入名称',
                        trigger: 'blur'
                    },
                    {
                        min: 2,
                        max: 30,
                        message: '长度在 3 到 5 个字符',
                        trigger: 'blur'
                    }
                ]
            },
            // 修改 弹出框
            editDialogVisible: false,
            // 修改 数据名
            editForm: {
                attr_name: ''
            },
            // Tag 里的 输入框
            inputVisibleExpand: false,
            inputValueExpand: ''
        }
    },
    methods: {
        // 渲染数据
        async paramsList() {
            // 查询数据 如果给参数 给值 就是相应值，没有传参就是获取所有的 这里指的是 type
            const {
                data: res
            } = await this.$http.get("categories");
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("获取分类列表失败");
            // 成功提示框
            // this.$message.success("获取分类列表成功");
            // 把数据放到 数组中 进行遍历
            this.paramsData = res.data;
        },
        // 级联选择和未选择
        paramsDataChange() {
            // console.log(this.paramsDataId);
            // 判断长度 不是三级分类 就不显示 
            if (this.paramsDataId.length !== 3) {
                // 选中 级联的下拉菜单
                this.paramsDataId = [];
                // 动态参数 数据
                this.manyParamsData = [];
                // 静态属性 数据
                this.onlyParamsData = [];
            } else {
                // 查询三级分类列表
                this.paramsDatas();
            }

        },
        // 查询数据
        async paramsDatas() {
            // 查询 三级分类 id值是  三级分类的值 参数值是 查询的那个 tab页
            const {
                data: res
            } = await this.$http.get(`categories/${this.cateId}/attributes`, {
                params: {
                    sel: this.activeName
                }
            });
            // console.log(res);
            // 判断
            // if (res.meta.status !== 200) return this.$message.error("获取商品分类失败");
            // 判断是 动态参数 还是 静态属性
            if (this.activeName == "many") {
                this.manyParamsData = res.data;
            } else {
                this.onlyParamsData = res.data;
            }
            if (this.cateId !== null) {
                // 切割  在展开行的时候 需要
                res.data.forEach(item => {
                    // console.log(item);
                    // 判断是否 字符串是否正确 正确的话  进行 以空格 进行切割 如何没有 放到数组里
                    const arr = item.attr_vals ? item.attr_vals.split(' ') : [];
                    // 进行传值 
                    item.attr_vals = arr;
                });
            }
        },
        // 点击tab 调用查询函数 每一次点击都查询
        parHandleClick() {
            // 查询三级分类列表
            this.paramsDatas();
        },
        // 添加 对话框隐藏 里面的表单清除  变成默认
        addParamsClose() {
            this.$refs.addFormRef.resetFields();
        },
        // 添加数据
        addParams() {
            // 检验规则判断
            this.$refs.addFormRef.validate(async valid => {
                // 校验规则是否错误
                if (!valid) return;
                // 添加数据
                const {
                    data: res
                } = await this.$http.post(`categories/${this.cateId}/attributes`, {
                    attr_name: this.addForm.attr_name,
                    attr_sel: this.activeName
                });
                // console.log(res);
                // 判断
                if (res.meta.status !== 201) return this.$message.error("添加失败");
                this.$message.success("添加成功");
                // 弹出框 隐藏
                this.addDialogVisible = !this.addDialogVisible;
                // 渲染数据 刷新页面  调用查询函数 每一次点击都查询
                this.parHandleClick();
            })
        },
        // 删除数据
        async delParams(id) {
            // console.log(id);
            const confirm = await this.$confirm('此操作将永久删除该文件, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(error => error);
            // console.log(confirm);
            // 判断
            if (confirm == "cancel") return this.$message.info("已取消成功")
            // 删除
            const {
                data: res
            } = await this.$http.delete(`categories/${this.cateId}/attributes/${id}`);
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("删除商品分类失败");
            // 提示框
            this.$message.success("删除商品分类成功");
            // 渲染数据 刷新页面  调用查询函数 每一次点击都查询
            this.parHandleClick();
        },
        // 添加 对话框隐藏 里面的表单清除  变成默认
        editParamsClose() {
            this.$refs.editFormRef.resetFields();
        },
        // 回显
        async editParams(id) {
            // console.log(id);
            // 修改 弹出框
            this.editDialogVisible = !this.editDialogVisible;
            // 回显数据 
            const {
                data: res
            } = await this.$http.get(`categories/${this.cateId}/attributes/${id}`);
            console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("回显商品分类失败");
            // 把数据给 修改数据 数组对象
            this.editForm = res.data;
        },
        // 修改
        async editsParams() {
            // 检验规则判断
            this.$refs.editFormRef.validate(async valid => {
                // 校验规则是否错误
                if (!valid) return;
                // 修改数据
                const {
                    data: res
                } = await this.$http.put(`categories/${this.cateId}/attributes/${this.editForm.attr_id}`, this.editForm);
                // console.log(res);
                // 判断
                if (res.meta.status !== 200) return this.$message.error("修改商品分类失败");
                // 提示框
                this.$message.success("修改商品分类成功");
                // 修改 弹出框
                this.editDialogVisible = !this.editDialogVisible;
                // 渲染数据 刷新页面  调用查询函数 每一次点击都查询
                this.parHandleClick();
            })
        },
        // 当焦点失去时 和 按回车时  触发函数
        async handleInputConfirmExpand(row) {
            // 判断输入的值 是否为空  为空就不创建 把input 恢复默认 按钮状态
            if (this.inputValueExpand.length === 0) {
                // 让 Tag 里的输入隐藏
                this.inputVisibleExpand = false;
                // 让input 清除
                return this.inputValueExpand = '';
            };
            // 把数据放到 数据库里
            row.attr_vals.push(this.inputValueExpand.trim());
            // console.log(row);
            // 让 Tag 里的输入隐藏
            this.inputVisibleExpand = false;
            // 让input 清除
            this.inputValueExpand = '';
            // 更新数据  就是在原来的数据上 添加数据  也就是修改数据
            const {
                data: res
            } = await this.$http.put(`categories/${this.cateId}/attributes/${row.attr_id}`, {
                attr_name: row.attr_name,
                attr_sel: this.activeName,
                // 因为数据库里的数据 是字符串  不接收 是数组形式,所以要变成 字符串
                attr_vals: row.attr_vals.join(' ')
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("更新失败");
        },
        // 点击Tag 里的 按钮, 让输入显示
        showInputExpand() {
            // 让 Tag 里的输入显示
            this.inputVisibleExpand = true;
            // 在显示的时候 直接 获取 文本框的焦点
            // 等到所以循环 完毕后 才会执行
            this.$nextTick(_ => {
                this.$refs.saveTagInputExpand.$refs.input.focus();
            });
        },
        // 删除指定的数据
        async delTagParams(row, index) {
            // console.log(row, index);
            // 在 row.attr_vals 中  删除哪一个 index 表示第几个 
            row.attr_vals.splice(index, 1);
            // console.log(row);
            // 把 这个 更新到 数据库里  原本是删除 也相当于 更新数据
            const {
                data: res
            } = await this.$http.put(`categories/${this.cateId}/attributes/${row.attr_id}`, {
                attr_name: row.attr_name,
                attr_sel: this.activeName,
                // 因为数据库里的数据 是字符串  不接收 是数组形式,所以要变成 字符串
                attr_vals: row.attr_vals.join(' ')
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("更新失败");
        }
    },
    // 计算数值的
    computed: {
        // 计算属性，在定义的时候，一定要被定义为一个方法
        // 但是，在页面上使用计算属性的时候，直接当作属性来用就行
        // 而且，在计算属性中，必须 return 一个 计算的结果
        isDisabled() {
            // 计算属性的特性：计算属性中，所以来的 数据，只要发生了变化，那么，就会立即 对计算属性重新求值
            if (this.paramsDataId.length === 3) return false;
            return true;
        },
        // 如果选中了 三级分类 那么值为数字 没有选中 那么值为 null  三级分类的ID 它要当作 请求数据的ID 使用
        cateId() {
            if (this.paramsDataId.length === 3) return this.paramsDataId[2];
            return null;
        }
    },
    created() {
        // 渲染数据 刷新页面
        this.paramsList();
    },
};