export default {
    data() {
        return {
            //  数据值
            ordersList: [],
            // 数据值 的参数
            ordersListInfo: {
                // 查询参数
                query: '',
                // 当前页码
                pagenum: 1,
                // 每页显示条数
                pagesize: 10
            },
            // 总页数
            total: 0,
            // 物流信息 弹出框
            logisticDialogVisible: false,
            // 物流信息 数组
            logisticData: []
        }
    },
    methods: {
        // 查询数据
        async getOrdersList() {
            // 数据查询
            const {
                data: res
            } = await this.$http.get("orders", {
                params: this.ordersListInfo
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("获取订单列表失败");
            // 把数据放到数组中
            this.ordersList = res.data.goods;
            // 总数据
            this.total = res.data.total;
        },
        // 每页的数据条数
        handleSizeChange(val) {
            this.ordersListInfo.pagesize = val;
            // 渲染数据 刷新页面
            this.getOrdersList();
        },
        // 每页的页数
        handleCurrentChange(val) {
            this.ordersListInfo.pagenum = val;
            // 渲染数据 刷新页面
            this.getOrdersList();
        },
        // 物流信息 回显
        async logisticOrders() {
            // 物流信息 弹出框
            this.logisticDialogVisible = true;
            // 回显
            const {
                data: res
            } = await this.$http.get("/kuaidi/619915933338");
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("获取物流信息失败");
            // 把数据放到 物流信息数组中
            this.logisticData = res.data;
        }
    },
    created() {
        // 渲染数据 刷新页面
        this.getOrdersList();
    },
}