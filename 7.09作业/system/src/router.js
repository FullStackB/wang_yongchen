import Vue from "vue";
import Router from "vue-router";
import Home from "./views/Home.vue";
import Login from "./views/Login.vue";


Vue.use(Router);

const router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [{
      path: "/",
      redirect: "/home"
    },
    {
      path: "/home",
      component: Home,
      redirect: '/welcome',
      children: [{
          path: '/welcome',
          component: () => import("./components/Welcome.vue")
        }, {
          // 用户首页
          path: '/users',
          component: () => import("./components/users/Users.vue")
        },
        {
          // 权限列表
          path: '/rights',
          component: () => import("./components/power/Rights.vue")
        },
        {
          // 角色列表
          path: '/roles',
          component: () => import("./components/power/Roles.vue")
        },
        {
          // 商品分类
          path: '/categories',
          component: () => import("./components/goods/Categories.vue")
        },
        {
          // 分类参数
          path: '/params',
          component: () => import("./components/goods/Params.vue")
        },
        {
          // 订单列表
          path: '/orders',
          component: () => import("./components/orders/Orders.vue")
        },
        {
          // 商品列表
          path: '/goods',
          component: () => import("./components/goods/Goods.vue")
        },
        {
          // 商品列表  添加
          path: '/goods/add',
          component: () => import("./components/goods/Goods_add.vue"),
        },
        {
          // 数据报表
          path: '/reports',
          component: () => import("./components/reports/Reports.vue"),
        }
      ]
    },
    {
      path: "/login",
      component: Login
    }
  ]
});

// 路由导航守卫
router.beforeEach((to, form, next) => {
  // 2、 进入login网页 但是没给login放行
  if (to.path === "/login") return next();
  // 1、 访问其他页面的时候 没有token的话就无法访问其他网页
  const tokenStr = window.sessionStorage.getItem("token");
  if (!tokenStr) return next("/login");
  // 3、访问其他网页时，也必须放行
  next();
});
export default router;