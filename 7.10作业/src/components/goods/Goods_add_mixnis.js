// 导入路由
import config from "../../config.json";
// 克隆
import _ from "lodash";
export default {
    data() {
        return {
            activeName: "0",
            // 级联数据源
            cascaderData: [],
            // 级联选择的 数据源的ID
            cascaderDataId: [],
            // 数据所显示的参数 比如名称 子节点 value值
            cascaderDataProps: {
                // 要显示的名称
                label: "cat_name",
                // 要选中的值 
                value: "cat_id",
                // 子节点
                children: "children"
            },
            // 表单的数据名
            addForm: {
                // 商品名称 
                goods_name: "",
                // 分类列表
                goods_cat: "",
                // 价格
                goods_price: "",
                // 数量
                goods_number: "",
                // 重量
                goods_weight: "",
                // 介绍
                goods_introduce: "",
                // 上传的图片临时路径（对象
                pics: [],
                // 商品的参数（数组），包含 `动态参数` 和 `静态属性`
                attrs: []
            },
            // 添加数据的 数据名验证
            addFormRules: {
                // 商品名称 
                goods_name: [{
                    required: true,
                    message: '请输入商品名称',
                    trigger: 'blur'
                }],
                // 价格
                goods_price: [{
                    required: true,
                    message: '请输入价格',
                    trigger: 'blur'
                }],
                // 重量
                goods_weight: [{
                    required: true,
                    message: '请输入重量',
                    trigger: 'blur'
                }],
                // 数量
                goods_number: [{
                    required: true,
                    message: '请输入数量',
                    trigger: 'blur'
                }],
                // 分类列表
                goods_cat: [{
                    required: true,
                    message: '请选择分类列表',
                    trigger: 'blur'
                }]

            },
            // 动态参数 数据
            manyParamsData: [],
            // 静态属性 数据
            onlyParamsData: [],
            // 图片路径  存储图片的
            uploadURL: config.baseURL + "upload",
            // 手动设置头部 图片上传时候手动指定token
            headerObj: {
                Authorization: window.sessionStorage.getItem('token')
            },
            // 图片预览 弹出框 
            picDialogVisible: false,
            // 图片存储的路径 显示
            picPreviewPath: ""
        }
    },
    methods: {
        // 查询 商品分类数据
        async getCascaderData() {
            // 查询数据 参数根据 文档来
            const {
                data: res
            } = await this.$http.get("categories", {
                params: {
                    type: 3
                }
            });
            // console.log(res);
            // 判断
            if (res.meta.status !== 200) return this.$message.error("获取商品分类失败");
            // 把数据给 数据源 数组中
            this.cascaderData = res.data;
        },
        // 级联下拉菜单  触发后 会执行函数
        cascaderDataChange() {
            // 判断有没有选择 三级菜单
            if (this.addForm.goods_cat.length !== 3) {
                this.addForm.goods_cat = [];
            }
        },
        // 在离开 tabs 前触发该函数
        beforeTabHandleClick(activeName, oldActiveName) {
            // console.log(oldActiveName);
            // 判断 之前 tabs name的值 是否为第一个 还有 判断级联 v-model  数组里的值
            if (oldActiveName == 0 && this.cateId == null) {
                this.$message.error("请选择商品分类");
                return false;
            }
        },
        // 切换 tabs 执行所要 数据
        async tabHandleClick() {
            // 判断是否  name的值 为 1 
            if (this.activeName === "1") {
                // 获取 动态参数
                const {
                    data: res
                } = await this.$http.get(`categories/${this.cateId}/attributes`, {
                    params: {
                        sel: "many"
                    }
                });
                // console.log(res);
                // 判断
                if (res.meta.status !== 200) return this.$message.error("获取动态参数失败");
                // 默认就是数组 把每一个动态参数的 attr_vals 的属性 都分割输出来 用 ' ' 空格 分割 防止出现空格
                res.data.forEach(item => {
                    item.attr_vals = item.attr_vals ? item.attr_vals.split(' ') : [];
                })
                // 把数据放到 动态参数 数组中
                this.manyParamsData = res.data;
            } else if (this.activeName === "2") {
                // 获取 静态参数
                const {
                    data: res
                } = await this.$http.get(`categories/${this.cateId}/attributes`, {
                    params: {
                        sel: "only"
                    }
                });
                // console.log(res);
                // 判断
                if (res.meta.status !== 200) return this.$message.error("获取动态参数失败");
                // 把数据放到 动态参数 数组中 默认就是字符串
                this.onlyParamsData = res.data;
            }
        },
        // 点击 预览图片
        handlePreview(result) {
            // console.log(result);
            // 图片预览 弹出框 
            this.picDialogVisible = !this.picDialogVisible;
            // 存储 放到 图片存储的路径
            this.picPreviewPath = result.response.data.url;
        },
        // 删除上传图片
        handleRemove(result) {
            // console.log(result);
            // 先把 该路径 存储起来
            const picPath = result.response.data.tmp_path;
            // console.log(picPath);
            // 进行查询，查询存储图片数组  利用findIndex 进行查 返回的是 数组索引值值
            const i = this.addForm.pics.findIndex(i => i.pic === picPath);
            // console.log(i);
            // 进行切割
            this.addForm.pics.splice(i, 1);

        },
        // 上传成功时 
        handleSuccess(result) {
            console.log(result);
            // 判断 如果成功 把路径给 存储图片路径 的页面数组中
            if (result.meta.status === 200) {
                // 把图片路径 放到定义对象中  数据库里需要这样的样子 按照数据库给的 这样写好 到最后就不用处理了
                const o = {
                    pic: result.data.tmp_path
                };
                // console.log(o);
                // 把该路径放到 定义 图片路径 数组中
                this.addForm.pics.push(o);
            }
            // console.log(this.addForm);

        },
        // 图片预览 隐藏框
        picPreviewClose() {
            this.picPreviewPath = "";
        },
        // 添加商品
        addGoods() {
            // 判断
            this.$refs.addFormRef.validate(async valid => {
                // 判断表单验证
                if (!valid) return this.$message.error("请完善表单数据然后在添加!");
                // console.log(this.addForm);
                // console.log(this.onlyParamsData);

                // 单独 把 goods_cat 数据 转换成字符串  是不成立的  克隆时后 就可以 实现该效果 那么就 下载lodash  this.addform.goods_cat.join 不是函数
                // this.addForm.goods_cat = this.addForm.goods_cat.join(",");

                // 把整个表单 克隆一下 在克隆的 变量上 执行操作 需要接收一下
                const form = _.cloneDeep(this.addForm);
                // 把 goods_cat 数据 利用 join 拼接
                form.goods_cat = form.goods_cat.join(",");
                // console.log(form.goods_cat);
                // 把 动态参数 和 静态属性 的值放到 attrs 中, attrs只需要两个值一个attr_id ,一个attr_value (attr_vals);
                // 1.循环 动态参数 把每一个元素都放到一起 
                this.manyParamsData.forEach(i => {
                    //  把数据放到 自定义的变量中 为什么折磨写 因为 数据库要以这样的方式接收 方便
                    const manyInfo = {
                        attr_id: i.attr_id,
                        attr_value: i.attr_vals.join(" ")
                    };
                    // 把数据存放起来  存到上面定义的表单 到时候提交的时候 直接用了  不用在做处理
                    form.attrs.push(manyInfo);
                });
                // 2.循环 静态属性 把每一个元素都放到一起 
                this.onlyParamsData.forEach(i => {
                    //  把数据放到 自定义的变量中 为什么折磨写 因为 数据库要以这样的方式接收 方便
                    const onlyInfo = {
                        attr_id: i.attr_id,
                        attr_value: i.attr_vals
                    };
                    // 把数据存放起来  存到上面定义的表单 到时候提交的时候 直接用了  不用在做处理
                    form.attrs.push(onlyInfo);
                });
                // console.log(form);
                // 添加数据
                const {
                    data: res
                } = await this.$http.post("goods", form).catch(err => err);
                // console.log(res);
                // 判断
                if (res.meta.status !== 201) return this.$message.error(res.meta.msg);
                // 添加提示框
                this.$message.success("添加商品成功");
                // 通过编程式导航，跳转到 商品列表页面
                this.$router.push("/goods");
            })
        }
    },
    created() {
        // 渲染数据  刷新页面
        this.getCascaderData();
    },
    // 计算属性
    computed: {
        // 选中三级 分类后 我要获取里面的 值 在这里定义 是为了 统一使用 方便 俭省代码 
        cateId() {
            if (this.addForm.goods_cat.length === 3) {
                return this.addForm.goods_cat[this.addForm.goods_cat.length - 1];
            }
            return null;
        }
    },
}