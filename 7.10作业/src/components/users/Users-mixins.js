export default {
    data() {
        // 添加用户判断手机号
        var addMobile = (rule, value, callback) => {
            // 判断是否输入内容
            if (!value) {
                return callback(new Error('请输入手机号'));
            }
            // 判断是否正确，默认是正确 然后取反正确时不进入判断,所以加一个!
            if (!(/^1([358][0-9]|4[579]|66|7[0135678]|9[89])[0-9]{8}$/.test(value))) return callback(new Error('请输入正确的11位手机号码'));
            // 正确时，就显示该样式
            callback();
        };
        // 数据定义返回出来
        return {
            // 用户列表
            userDatas: [],
            // 状态
            // 查询数据参数
            queryInfo: {
                // 查询的参数
                query: "",
                // 当前的页码
                pagenum: 1,
                // 每页显示多少条数据
                pagesize: 2
            },
            // 显示总数据
            total: 0,
            // 添加用户数据弹出框
            addDialogVisible: false,
            // 添加用户数据名
            addForm: {
                // 用户名
                username: "",
                // 密码
                password: "",
                // 邮箱
                email: "123@126.cn",
                // 手机号
                mobile: "17553041262"
            },
            // 添加用户数据判断
            addFormRules: {
                // 用户名
                username: [{
                        required: true,
                        message: '请输入用户名',
                        trigger: 'blur'
                    },
                    {
                        min: 2,
                        max: 8,
                        message: '长度在 2 到 8 个字符',
                        trigger: 'blur'
                    }
                ],
                // 密码
                password: [{
                        required: true,
                        message: '请输入密码',
                        trigger: 'blur'
                    },
                    {
                        min: 6,
                        max: 18,
                        message: '长度在 6 到 18 个字符',
                        trigger: 'blur'
                    }
                ],
                // 邮箱
                email: [{
                        required: true,
                        message: '请输入邮箱地址',
                        trigger: 'blur'
                    },
                    {
                        type: 'email',
                        message: '请输入正确的邮箱地址',
                        trigger: 'blur'
                    }
                ],
                // 手机号
                mobile: [{
                    required: true,
                    validator: addMobile,
                    trigger: 'blur'
                }]
            },
            // 回显用户数据弹出框
            echoDialogVisible: false,
            // 修改用户数据名
            echoForm: {
                // 用户名
                username: "",
                // 邮箱
                email: "",
                // 手机号
                mobile: ""
            },
            // 修改用户数据判断
            echoFormRules: {
                // 邮箱
                email: [{
                        required: true,
                        message: '请输入邮箱地址',
                        trigger: 'blur'
                    },
                    {
                        type: 'email',
                        message: '请输入正确的邮箱地址',
                        trigger: 'blur'
                    }
                ],
                // 手机号
                mobile: [{
                    required: true,
                    validator: addMobile,
                    trigger: 'blur'
                }]
            },
            // 存一下修改回显时的ID
            echoId: '',
            // 分配角色弹出框
            roleDialogVisible: false,
            // 分配角色用户数据名
            roleForm: {},
            // 所有角色列表
            rolesList: [],
            // 被选中的角色Id
            selectedRoleId: ''
        };
    },
    methods: {
        //  查询用户列表数据
        async userData() {
            const {
                data: res
            } = await this.$http.get("users", {
                params: this.queryInfo
            });
            //   console.log(res);
            //   把数据传给查询数组里
            this.userDatas = res.data.users;
            //  因为页数 我要查询一个多少条数据  所以自定义一个变量初始化
            this.total = res.data.total;
        },
        // 每当 pageSize 改变，都会触发这个函数  多少条数据
        handleSizeChange(val) {
            // 把该值传给 它  显示多少条数据
            this.queryInfo.pagesize = val;
            // 刷新数据
            this.userData();
        },
        // 每当 页码值 改变，都会触发这个函数
        handleCurrentChange(val) {
            // 把该值传给 它 显示多少页
            this.queryInfo.pagenum = val;
            // 刷新数据
            this.userData();
        },
        // 添加用户对话框隐藏后校验清除
        addUserClose() {
            // 清除表单内的内容
            this.$refs.addFormRef.resetFields();
        },
        // 添加用户
        addUser() {
            // for (var i = 10; i <= 20; i++) {
            //     // 添加用户数据
            //     const {
            //         data: res
            //     } = await this.$http.post("users", {
            //         username: this.addForm.username + i,
            //         password: this.addForm.password,
            //         email: this.addForm.email,
            //         mobile: this.addForm.mobile
            //     });
            //     // 提示框
            //     this.$message.success("添加用户成功");
            //     // 弹出框隐藏
            //     this.addDialogVisible = !this.addDialogVisible;
            //     // 刷新页面
            //     this.userData();
            //     // 清除表单内的内容
            //     this.$refs.addFormRef.resetFields();
            // }
            this.$refs.addFormRef.validate(async valid => {
                // 判断 表单是否正确
                if (!valid) return;
                // 添加用户数据
                const {
                    data: res
                } = await this.$http.post("users", this.addForm);
                // console.log(res);
                // 判断 是否为201 如果如果不是就执行判断里的代码 
                if (res.meta.status !== 201) return this.$message.warning(res.meta.msg);
                // 提示框
                this.$message.success("添加用户成功");
                // 弹出框隐藏
                this.addDialogVisible = !this.addDialogVisible;
                // 刷新页面
                this.userData();
                // 清除表单内的内容
                this.$refs.addFormRef.resetFields();
            });
        },
        // 删除用户数据
        async delUser(id) {
            // for (var i = id;; i++) {
            //     const {
            //         data: res
            //     } = await this.$http.delete("/users/" + i);
            //     this.userData();
            //     this.$message.success("删除成功")
            // };
            // console.log(id);
            // console.log(res);
            // 判断是否是成功删除 confirm  取消删除cancel
            const result = await this.$confirm('此操作将永久删除该用户, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(error => error);
            // console.log(result);
            // 判断返回出来的结果是 cancel 如果是cancel,就代表取消删除 否则就删除数据
            if (result === "cancel") return this.$message.info("已取消删除")
            // 删除数据 根据ID  删除
            const {
                data: res
            } = await this.$http.delete("/users/" + id);
            // 判断 是否为200 如果如果不是就执行判断里的代码 
            if (res.meta.status !== 200) return this.$message.warning(res.meta.msg);
            // 删除提示框
            this.$message.success(res.meta.msg);

            // 此时，只是把当前页面上，唯一的一条数据，从数据库中删除了，但是，userDatas 的长度还为 1
            if (this.userDatas.length === 1 && this.queryInfo.pagenum > 1) this.queryInfo.pagenum--;
            // 刷新页面
            this.userData();
        },
        // 修改用户状态
        async stateUser(id, state, user) {
            // 更改数据库里的状态 
            const {
                data: res
            } = await this.$http.put(`users/${id}/state/${state}`);
            // 如果更新失败 就让按钮不动  所以 user.mg_state = !user.mg_state
            if (res.meta.status !== 200) {
                user.mg_state = !user.mg_state
                return this.$message.error('修改用户状态失败！')
            }
        },
        // 修改用户对话框隐藏后校验清除
        echoUserClose() {
            this.$refs.echoFormRef.resetFields();
        },
        // 修改用户回显数据
        async echoUser(id) {
            // 回显弹出框显示
            this.echoDialogVisible = !this.echoDialogVisible;
            // 存一下回显时的ID,修改时用到
            this.echoId = id;
            // 查询数据 一条数据
            const {
                data: res
            } = await this.$http.get("/users/" + id);
            // console.log(res);
            // 判断是否查询正确
            if (res.meta.status !== 200) return this.$message.error("查询数据失败！");
            // 用户名回显数据
            this.echoForm.username = res.data.username;
            // 邮箱回显数据
            this.echoForm.email = res.data.email;
            // 手机号回显数据
            this.echoForm.mobile = res.data.mobile;
        },
        // 修改用户数据
        modify() {
            this.$refs.echoFormRef.validate(async valid => {
                // 判断
                if (!valid) return;
                // 修改数据
                const {
                    data: res
                } = await this.$http.put("/users/" + this.echoId, {
                    // 用户名回显数据
                    username: this.echoForm.username,
                    // 邮箱回显数据
                    email: this.echoForm.email,
                    // 手机号回显数据
                    mobile: this.echoForm.mobile
                });
                // 判断 是否为200 如果如果不是就执行判断里的代码 
                if (res.meta.status !== 200) return this.$message.warning("更新失败");

                // 回显弹出框隐藏
                this.echoDialogVisible = !this.echoDialogVisible;
                // 提示框
                this.$message.success(res.meta.msg);
                // 刷新页面
                this.userData();
            })
        },
        // 分配角色对话框隐藏后校验清除
        roleUserClose() {
            this.selectedRoleId = ''
        },
        // 分配角色回显数据
        async roleUser(row) {
            // console.log(row);
            // 把数据放到分配角色 对象中
            this.roleForm = row;
            // 分配角色弹出框显示
            this.roleDialogVisible = !this.roleDialogVisible;
            // 分配角色查询数据 一条数据
            const {
                data: res
            } = await this.$http.get("roles");
            // console.log(this.roleForm);

            // 判断是否等于200 如果不等于就获取角色列表失败
            if (res.meta.status !== 200) return this.$message.error('获取角色列表失败！');
            // 把所有的分配角色列表 放到数组里
            this.rolesList = res.data;
            // console.log(this.rolesList);

        },
        // 分配角色修改数据
        async assignment() {
            // console.log(this.selectedRoleId);
            // console.log(this.rolesList);
            if (!this.selectedRoleId) return this.$message.warning('请选择要分配的新角色！');
            // 修改角色
            const {
                data: res
            } = await this.$http.put(`users/${this.roleForm.id}/role`, {
                rid: this.selectedRoleId
            });
            // console.log(res);
            // 判断是否正确
            if (res.meta.status !== 200) return this.$message.error("修改角色失败");
            // 修改提示框
            this.$message.success(res.meta.msg);
            // 分配角色弹出框隐藏
            this.roleDialogVisible = !this.roleDialogVisible;
            // 刷新页面
            this.userData();
        }
    },
    // 生命周期
    created() {
        // 刷新页面
        this.userData();
    }
};