export default {
    data() {
        return {
            // 角色的数据
            rolesList: [],
            // 添加角色弹出框 默认为false
            addDialogVisible: false,
            // 添加角色数据名
            addForm: {
                // 角色名称
                roleName: '',
                // 角色描述
                roleDesc: ''
            },
            // 添加角色数据判验证规则
            addFormRules: {
                // 角色名称
                roleName: [{
                        required: true,
                        message: '请输入角色名称',
                        trigger: 'blur'
                    },
                    {
                        min: 3,
                        max: 8,
                        message: '长度在 3 到 8 个字符',
                        trigger: 'blur'
                    }
                ],
                // 角色描述
                roleDesc: [{
                        required: true,
                        message: '请输入角色描述',
                        trigger: 'blur'
                    },
                    {
                        min: 3,
                        max: 30,
                        message: '长度在 3 到 30 个字符',
                        trigger: 'blur'
                    }
                ]
            },
            // 修改角色数据弹出框 默认为false
            echoDialogVisible: false,
            // 修改角色数据名
            echoForm: {},
            // 分配角色 弹出框 默认为false
            jurisDialogVisible: false,
            // 数据渲染数据
            jurisList: [],
            // 要显示什么  
            jurisProps: {
                // 显示的名字
                label: "authName",
                // 在哪里
                children: "children"
            },
            // 默认显示的数据
            jurisKeys: [],
            // 分配权限  修改ID
            jurisId: ""
        }
    },
    // 生命周期
    created() {
        // 刷新页面
        this.getRolesList();
    },
    methods: {
        // 查询所有角色数据
        async getRolesList() {
            const {
                data: res
            } = await this.$http.get("roles");
            // console.log(res);
            // 判断是否等于200
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 把数据放到数组中
            this.rolesList = res.data;
        },
        // 添加角色数据的弹出框 隐藏时 清除表单
        addClose() {
            this.$refs.addFormRef.resetFields();
        },
        // 添加角色数据
        addRoles() {
            // 判断表单验证规则
            this.$refs.addFormRef.validate(async valid => {
                if (!valid) return;
                // 添加角色数据
                const {
                    data: res
                } = await this.$http.post("roles", this.addForm);
                // console.log(res);
                // 判断是否添加成功
                if (res.meta.status !== 201) return this.$message.error(res.meta.msg);
                // 把添加角色弹出框隐藏
                this.addDialogVisible = !this.addDialogVisible;
                // 提示框
                this.$message.success("添加角色成功");
                // 刷新页面
                this.getRolesList();
            })

        },
        // 删除角色数据
        async delRoles(id) {
            // console.log(id);
            // 弹出框
            const confirm = await this.$confirm('此操作将永久删除该角色, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(error => error);
            // console.log(confirm);
            // 判断是否点击的取消
            if (confirm === "cancel") return this.$message.info("已取消删除");

            // 删除角色数据
            const {
                data: res
            } = await this.$http.delete(`roles/${id}`);
            // console.log(res);
            // 判断是否删除成功
            if (res.meta.status !== 200) return this.$message.error('删除角色失败！');
            // 提示框
            this.$message.success(res.meta.msg);
            // 刷新页面
            this.getRolesList();
        },
        // 修改角色数据的弹出框 隐藏时 清除表单
        echoClose() {
            this.$refs.echoFormRef.resetFields();
        },
        // 回显角色数据
        async echoRoles(id) {
            // console.log(id);
            // 修改角色弹出框显示
            this.echoDialogVisible = !this.echoDialogVisible;
            // 把角色数据查询
            const {
                data: res
            } = await this.$http.get(`roles/${id}`);
            // console.log(res);
            // 判断是否查询角色数据正确
            if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
            // 把resd.data 里的数据全部给 一个对象
            this.echoForm = res.data;
        },
        // 修改角色数据
        modifyRoles() {
            // console.log(this.echoForm);
            // 判断表单验证规则
            this.$refs.echoFormRef.validate(async valid => {
                if (!valid) return;
                // 修改数据
                const {
                    data: res
                } = await this.$http.put(`roles/${this.echoForm.roleId}`, this.echoForm);
                // console.log(res);
                // 判断是否修改成功
                if (res.meta.status !== 200) return this.$message.error(res.meta.msg);
                // 修改角色弹出框隐藏
                this.echoDialogVisible = !this.echoDialogVisible;
                // 提示框
                this.$message.success("修改角色成功");
                // 刷新页面
                this.getRolesList();
            });
        },
        // 根据权限ID 删除角色指定下的权限
        async removeRightRoles(role, id) {
            // console.log(id);
            // 弹出框
            const confirm = await this.$confirm('此操作将永久删除该角色, 是否继续?', '提示', {
                confirmButtonText: '确定',
                cancelButtonText: '取消',
                type: 'warning'
            }).catch(error => error);
            // console.log(confirm);
            // 判断是否点击的取消
            if (confirm === "cancel") return this.$message.info("已取消删除");

            // 删除角色数据
            const {
                data: res
            } = await this.$http.delete(`roles/${role.id}/rights/${id}`);
            // console.log(res);
            // 判断是否删除成功
            if (res.meta.status !== 200) return this.$message.error('删除权限失败！');
            // 提示框
            this.$message.success(res.meta.msg);
            // 把服务器返回的最新的权限数组，赋值给 当前角色的 children 属性
            role.children = res.data;
            // // 刷新页面
            // this.getRolesList();
        },
        // 回显
        async jurisRoles(row) {
            // ID
            this.jurisId = row.id;
            // 分配角色 弹出框 
            this.jurisDialogVisible = !this.jurisDialogVisible;
            // 查询数据
            const {
                data: res
            } = await this.$http.get("rights/tree");
            // console.log(res);
            // 把数据渲染出来 给定义好的数组
            this.jurisList = res.data;
            // 为了 实现回显 需要渲染数据 看那些是 叶子节点 那么就需要一个递归函数  先定义一个保留数据的数组
            const keys = [];
            // 调用函数 递归函数
            this.getrRcursion(row, keys);
            // 把查询的数据  放到回显数据中 让页面回显
            this.jurisKeys = keys;
        },
        // 递归
        getrRcursion(node, keysArr) {
            if (!node.children) return keysArr.push(node.id);
            node.children.forEach(item => this.getrRcursion(item, keysArr));
        },
        // 修改
        async jurisEditRoles() {
            // 修改数据 就是获取该 按钮是否被选中 选中的区别有两种 一个半选 一个全选
            // 全选按钮
            const key1 = this.$refs.tree.getCheckedKeys();
            // 半选按钮
            const key2 = this.$refs.tree.getHalfCheckedKeys();
            // 进行拼接
            const keyNum = [...key1, ...key2].join(",");
            // 修改
            const {
                data: res
            } = await this.$http.post(`roles/${this.jurisId}/rights`, {
                rids: keyNum
            });
            // 判断
            if (res.meta.status !== 200) return this.$message.error("分配权限失败");
            // 提示框
            this.$message.success("分配权限成功");
            // 分配角色 弹出框 
            this.jurisDialogVisible = !this.jurisDialogVisible;
            // 刷新页面
            this.getRolesList();
        }
    },
}